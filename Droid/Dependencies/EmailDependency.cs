﻿using System;
using Yuvish.Droid;
using Android.Content;

[assembly: Xamarin.Forms.Dependency (typeof (EmailDependency))]

namespace Yuvish.Droid
{
	public class EmailDependency : IEmail
	{
		#region IEmail implementation

		public void SendEmail ()
		{
			var emailIntent = new Intent (Android.Content.Intent.ActionSend);

			emailIntent.PutExtra (Android.Content.Intent.ExtraEmail, new string[]{"hola@yuvish.com"});

			emailIntent.PutExtra (Android.Content.Intent.ExtraSubject, "Solicitud de Soporte");

			emailIntent.SetType ("message/rfc822");

			Xamarin.Forms.Forms.Context.StartActivity (emailIntent);
		}

		#endregion

	}
}

