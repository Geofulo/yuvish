﻿using System;
using Yuvish.Droid;
using Xamarin.Forms.Maps;
using Android.Locations;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency (typeof (LocationDependency))]

namespace Yuvish.Droid
{
	public class LocationDependency : ILocation, ILocationListener
	{
		LocationManager LocMgr;
		Location UserLocation;

		public LocationDependency () {
			LocMgr = Forms.Context.GetSystemService (Forms.Context.LocationService) as LocationManager;
		}

		#region ILocation implementation

		public bool AskWhenInUseAuthorization ()
		{
			LocationManager LocMgr = Forms.Context.GetSystemService (Forms.Context.LocationService) as LocationManager;

			return true;
		}

		public Position GetCurrentLocation ()
		{
			return new Position (UserLocation.Latitude, UserLocation.Longitude);
		}

		#endregion


		#region ILocationListener implementation
		public void OnLocationChanged (Location location)
		{
			UserLocation = location;
		}
		public void OnProviderDisabled (string provider)
		{
			
		}
		public void OnProviderEnabled (string provider)
		{
			LocMgr.RequestLocationUpdates (provider, 2000, 1, this);
		}
		public void OnStatusChanged (string provider, Availability status, Android.OS.Bundle extras)
		{
			
		}
		#endregion

		#region IDisposable implementation
		public void Dispose ()
		{
			
		}
		#endregion

		#region IJavaObject implementation
		public IntPtr Handle {
			get {
				throw new NotImplementedException ();
			}
		}
		#endregion
	}
}

