﻿using System;
using Yuvish.Droid;
using Android.Content;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency (typeof (PhoneDependency))]

namespace Yuvish.Droid
{
	public class PhoneDependency : IPhone
	{
		#region IPhone implementation

		public void Call ()
		{
			var uri = Android.Net.Uri.Parse ("tel:44556677");
			var intent = new Intent (Intent.ActionDial, uri);
			Forms.Context.StartActivity (intent);
		}

		#endregion
	}
}

