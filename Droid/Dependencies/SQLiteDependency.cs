﻿using System;
using Yuvish.Droid;
using System.IO;

[assembly: Xamarin.Forms.Dependency (typeof (SQLiteDependency))]

namespace Yuvish.Droid
{
	public class SQLiteDependency : ISQLite
	{
		#region ISQLite implementation

		public SQLite.SQLiteConnection GetConnection ()
		{
			const string sqliteFilename = "yuvish.db3";
			string documentsPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal); // Documents folder
			var path = Path.Combine(documentsPath, sqliteFilename);
			var conn = new SQLite.SQLiteConnection(path);

			return conn;
		}

		#endregion


	}
}

