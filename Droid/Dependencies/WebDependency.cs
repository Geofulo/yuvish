﻿using System;
using Yuvish.Droid;
using Android.Content;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency (typeof (WebDependency))]

namespace Yuvish.Droid
{
	public class WebDependency : IWeb
	{
		#region IWeb implementation

		public void OpenUrl (string url)
		{
			var uri = Android.Net.Uri.Parse (url);			
			var intent = new Intent (Intent.ActionView, uri);
			Forms.Context.StartActivity (intent);
		}

		#endregion

	}
}

