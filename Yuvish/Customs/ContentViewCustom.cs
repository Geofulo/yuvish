﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class ContentViewCustom : ContentView
	{
		public static readonly BindableProperty IsDarkBlurProperty =
			BindableProperty.Create<ContentViewCustom, Boolean>(
				p => p.IsDarkBlur, false);

		public Boolean IsDarkBlur {
			get { return (Boolean)GetValue(IsDarkBlurProperty); }
			set { SetValue(IsDarkBlurProperty, value); }
		}

		public static readonly BindableProperty IsLightBlurProperty =
			BindableProperty.Create<ContentViewCustom, Boolean>(
				p => p.IsLightBlur, false);

		public Boolean IsLightBlur {
			get { return (Boolean)GetValue(IsLightBlurProperty); }
			set { SetValue(IsLightBlurProperty, value); }
		}

		public static readonly BindableProperty BlurNumberProperty =
			BindableProperty.Create<ContentViewCustom, int>(
				p => p.BlurNumber, 1);

		public int BlurNumber {
			get { return (int)GetValue(BlurNumberProperty); }
			set { SetValue(BlurNumberProperty, value); }
		}

		public static readonly BindableProperty RadiusProperty =
			BindableProperty.Create<ContentViewCustom, int>(
				p => p.Radius, 0);

		public int Radius {
			get { return (int)GetValue(RadiusProperty); }
			set { SetValue(RadiusProperty, value); }
		}

		public static readonly BindableProperty ColorBorderProperty =
			BindableProperty.Create<ContentViewCustom, Color>(
				p => p.ColorBorder, Color.Transparent);

		public Color ColorBorder {
			get { return (Color)GetValue(ColorBorderProperty); }
			set { SetValue(ColorBorderProperty, value); }
		}
	}
}

