﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class EditorCustom : Editor
	{
		public static readonly BindableProperty WidthUnderlineProperty = 
			BindableProperty.Create<EditorCustom, int>(
				p=>p.WidthUnderline, 0);

		public int WidthUnderline {
			get { return (int)GetValue (WidthUnderlineProperty); }
			set { SetValue (WidthUnderlineProperty, value); }
		}	
	}
}

