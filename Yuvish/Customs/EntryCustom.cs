﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class EntryCustom : Entry
	{
		public static readonly BindableProperty BorderRadiusProperty = 
			BindableProperty.Create<EntryCustom, int>(
				p=>p.BorderRadius, 0);

		public int BorderRadius {
			get { return (int)GetValue (BorderRadiusProperty); }
			set { SetValue (BorderRadiusProperty, value); }
		}

		public static readonly BindableProperty IsUnderlineProperty = 
			BindableProperty.Create<EntryCustom, bool>(
				p=>p.IsUnderline, false);

		public bool IsUnderline {
			get { return (bool)GetValue (IsUnderlineProperty); }
			set { SetValue (IsUnderlineProperty, value); }
		}

		public static readonly BindableProperty WidthUnderlineProperty = 
			BindableProperty.Create<EntryCustom, int>(
				p=>p.WidthUnderline, 0);

		public int WidthUnderline {
			get { return (int)GetValue (WidthUnderlineProperty); }
			set { SetValue (WidthUnderlineProperty, value); }
		}

		public static readonly BindableProperty IsNoBorderProperty = 
			BindableProperty.Create<EntryCustom, bool>(
				p=>p.IsNoBorder, false);

		public bool IsNoBorder {
			get { return (bool)GetValue (IsNoBorderProperty); }
			set { SetValue (IsNoBorderProperty, value); }
		}
	}
}

