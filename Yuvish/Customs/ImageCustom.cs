﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class ImageCustom : Image
	{
		public static readonly BindableProperty IsCircleProperty = 
			BindableProperty.Create<ImageCustom, bool>(
				p=>p.IsCircle, false);

		public bool IsCircle {
			get { return (bool)GetValue (IsCircleProperty); }
			set { SetValue (IsCircleProperty, value); }
		}
	}
}

