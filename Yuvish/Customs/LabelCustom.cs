﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class LabelCustom : Label
	{
		public static readonly BindableProperty BorderRadiusProperty = 
			BindableProperty.Create<LabelCustom, int>(
				p=>p.BorderRadius, 0);

		public int BorderRadius {
			get { return (int)GetValue (BorderRadiusProperty); }
			set { SetValue (BorderRadiusProperty, value); }
		}
	}
}

