﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class PickerCustom : Picker
	{
		public static readonly BindableProperty WidthUnderlineProperty = 
			BindableProperty.Create<PickerCustom, int>(
				p=>p.WidthUnderline, 0);

		public int WidthUnderline {
			get { return (int)GetValue (WidthUnderlineProperty); }
			set { SetValue (WidthUnderlineProperty, value); }
		}	

		public static readonly BindableProperty IsTextAligmentCenterProperty = 
			BindableProperty.Create<PickerCustom, bool>(
				p=>p.IsTextAligmentCenter, false);

		public bool IsTextAligmentCenter {
			get { return (bool)GetValue (IsTextAligmentCenterProperty); }
			set { SetValue (IsTextAligmentCenterProperty, value); }
		}	

		public static readonly BindableProperty TextColorProperty = 
			BindableProperty.Create<PickerCustom, Color>(
				p=>p.TextColor, Color.Transparent);

		public Color TextColor {
			get { return (Color)GetValue (TextColorProperty); }
			set { SetValue (TextColorProperty, value); }
		}	

		public static readonly BindableProperty FontProperty = 
			BindableProperty.Create<PickerCustom, Font>(
				p=>p.Font, Font.Default);

		public Font Font {
			get { return (Font)GetValue (FontProperty); }
			set { SetValue (FontProperty, value); }
		}	
	}
}

