﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class StepperCustom : Stepper
	{
		public static readonly BindableProperty TintColorProperty = 
			BindableProperty.Create<StepperCustom, Color>(
				p=>p.TintColor, Color.Black);

		public Color TintColor {
			get { return (Color)GetValue (TintColorProperty); }
			set { SetValue (TintColorProperty, value); }
		}
	}
}

