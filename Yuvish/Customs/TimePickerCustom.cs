﻿using System;
using Xamarin.Forms;


namespace Yuvish
{
	public class TimePickerCustom : TimePicker
	{
		public static readonly BindableProperty WidthUnderlineProperty = 
			BindableProperty.Create<TimePickerCustom, int>(
				p=>p.WidthUnderline, 0);

		public int WidthUnderline {
			get { return (int)GetValue (WidthUnderlineProperty); }
			set { SetValue (WidthUnderlineProperty, value); }
		}
	}
}

