﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class ViewCellCustom : ViewCell
	{
		public static readonly BindableProperty SelectedBackgroundColorProperty = 
			BindableProperty.Create<ViewCellCustom, Color>(
				p=>p.SelectedBackgroundColor, Color.Gray);

		public Color SelectedBackgroundColor {
			get { return (Color)GetValue (SelectedBackgroundColorProperty); }
			set { SetValue (SelectedBackgroundColorProperty, value); }
		}
	}
}

