﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class LocalDataManager
	{
		SQLiteConnection Database;

		public LocalDataManager ()
		{
			createLocalDataBase ();
		}

		void createLocalDataBase ()
		{
			Database = DependencyService.Get<ISQLite> ().GetConnection ();

			Database.CreateTable<CarLocalModel> ();
			Database.CreateTable<CreditCardModel> ();
			Database.CreateTable<PlaceModel> ();
			Database.CreateTable<FiscalModel> ();
			Database.CreateTable<CreditCardModel> ();
		}

		public void deleteLocalDataBase ()
		{
			Database.DeleteAll<CarLocalModel> ();
			Database.DeleteAll<CreditCardModel> ();
			Database.DeleteAll<PlaceModel> ();
			Database.DeleteAll<FiscalModel> ();
			Database.DeleteAll<CreditCardModel> ();
		}

		// :::::::::::::::::::: CREDIT CARDS ::::::::::::::::::::

		public List<CreditCardModel> GetCreditCards ()
		{
			Print ("LocalDB GetCreditCards...");
			var result = new List<CreditCardModel> ();
			try {									
				var cards = from ps in Database.Table<CreditCardModel>() select ps;

				foreach(var item in cards)
				{
					result.Add (item);
				}
				Print (result.Count + " tarjetas");
				return result; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool CreateCreditCard (CreditCardModel card)
		{
			Print ("LocalDB CreateCreditCard...");
			try {
				Database.Insert (card);
				return true; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool UpdateCreditCard (CreditCardModel card)
		{
			Print ("LocalDB UpdateCreditCard...");
			try {
				Database.Update (card);
				return true; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool DeleteCreditCard (CreditCardModel card)
		{
			Print ("LocalDB DeleteCreditCard...");
			try {
				Database.Delete (card);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool ExistCreditCard (string cardNumber)
		{
			Print ("LocalDB ExistCreditCard...");
			var cards = GetCreditCards ();
			foreach (var card in cards) {
				if (card.Number.Equals (cardNumber))
					return true;
			}
			return false;
		}

		// :::::::::::::::::::: PLACES ::::::::::::::::::::

		public List<PlaceModel> GetPlaces ()
		{
			Print ("LocalDB GetPlaces...");
			var result = new List<PlaceModel> ();
			try {									
				var places = from ps in Database.Table<PlaceModel>() select ps;

				foreach(var place in places)
				{
					result.Add (place);
				}
				Print (result.Count + " lugares");
				return result; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool CreatePlace (PlaceModel place)
		{
			Print ("LocalDB CreatePlace...");
			try {
				Database.Insert (place);
				return true; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool UpdatePlace (PlaceModel place)
		{
			Print ("LocalDB UpdatePlace...");
			try {
				Database.Update (place);
				return true; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool DeletePlace (PlaceModel place)
		{
			Print ("LocalDB DeletePlace...");
			try {
				Database.Delete (place);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool ExistPlace (double lat, double lon)
		{
			Print ("LocalDB ExistPlace...");
			var places = GetPlaces ();
			foreach (var place in places) {
				if (place.Latitud.Equals(lat) && place.Longitud.Equals(lon))
					return true;
			}
			return false;
		}

		public bool ExistPlace (string nombre)
		{
			var places = GetPlaces ();
			foreach (var place in places) {
				if (place.Nombre.Equals(nombre))
					return true;
			}
			return false;
		}

		// :::::::::::::::::::: CARS ::::::::::::::::::::

		public List<CarLocalModel> GetCars ()
		{
			Print ("LocalDB GetCars...");
			var result = new List<CarLocalModel> ();
			try {									
				var cars = from ps in Database.Table<CarLocalModel>() select ps;

				foreach(var car in cars)
				{
					result.Add (car);
				}
				Print (result.Count + " autos");
				return result; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool CreateCar (CarLocalModel car)
		{
			Print ("LocalDB CreateCar...");
			try {
				Database.Insert (car);
				return true; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool UpdateCar (CarLocalModel car)
		{
			Print ("LocalDB UpdateCar...");
			try {
				Database.Update (car);
				return true; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool DeleteCar (CarLocalModel car)
		{
			Print ("LocalDB DeleteCar...");
			try {
				Database.Delete (car);
				return true;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool ExistCar (string placas)
		{
			Print ("LocalDB ExistCar...");
			var cars = GetCars ();
			foreach (var car in cars) {
				if (car.Placas.Equals (placas))
					return true;
			}
			return false;
		}

		public CarLocalModel GetCar (string placas)
		{
			Print ("LocalDB GetCar...");
			var cars = GetCars ();
			foreach (var car in cars) {
				if (car.Placas.Equals (placas))
					return car;
			}
			return null;
		}

		// :::::::::::::::::::: FISCAL INFORMATION ::::::::::::::::::::

		public FiscalModel GetFiscal ()
		{
			Print ("LocalDB GetFiscal...");
			try {									
				var fiscals = from ps in Database.Table<FiscalModel>() select ps;
				var fiscal = fiscals.ElementAt (0);
				Print (fiscals.Count() + " fiscal");
				return fiscal;
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return null;
			}
		}

		public bool CreateFiscal (FiscalModel fiscal)
		{
			Print ("LocalDB CreateFiscal...");
			try {
				Database.Insert (fiscal);
				return true; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		public bool UpdateFiscal (FiscalModel fiscal)
		{
			Print ("LocalDB UpdateFiscal...");
			try {
				Database.Update (fiscal);
				return true; 
			} catch(Exception e) {
				System.Diagnostics.Debug.WriteLine (e.Message);
				return false;
			}
		}

		// :::::::::::::::::::: OTHERS ::::::::::::::::::::

		void Print(string msg)
		{
			System.Diagnostics.Debug.WriteLine (msg);
		}
	}
}

