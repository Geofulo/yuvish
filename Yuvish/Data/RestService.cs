﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Yuvish
{
	public class RestService 
	{
		HttpClient Client;

		//static string _uriBase = "http://yuvishtestws-env.elasticbeanstalk.com/yuvish/";
		//static string _uriBase = "http://yuvish.com:8080/Yuvish/yuvish/";
		//static string _uriBase = "http://qa.yuvish.com/yuvish/";
		static string _uriBase = "https://d3023j0rgcjadw.cloudfront.net/yuvish/";
		static string _typeContent = "application/json";

		public RestService ()
		{
			Client = new HttpClient ();
		}

		public bool Signup (UserRegisterModel user)
		{
			System.Diagnostics.Debug.WriteLine ("REST Signup...");

			var tcs = new TaskCompletionSource<bool> ();

			new Task (async () => {				
				try {					
					var json = JsonConvert.SerializeObject (user);

					System.Diagnostics.Debug.WriteLine ("JSON Object: " + json);

					var jsonContent = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "registrar", jsonContent);

					var res = await response.Content.ReadAsStringAsync ();

					System.Diagnostics.Debug.WriteLine ("Response Message: " + response.RequestMessage);

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode.ToString() + " " + res);
						var jsonObject = JsonConvert.DeserializeObject<IDictionary<string, string>> (res);

						var token = (string) jsonObject ["token"];
						System.Diagnostics.Debug.WriteLine ("Token: " + token);

						Yuvish.Helpers.Settings.TokenLogin = token;
						App.UserToken = token;

						App.CurrentUser = GetMyInfo();

						tcs.SetResult (true);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode.ToString() + " " + response.Content);
						tcs.SetResult (false);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (false);
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public string Login (UserLoginModel user)
		{
			System.Diagnostics.Debug.WriteLine ("REST Login...");

			var tcs = new TaskCompletionSource<string> ();

			new Task (async () => {
				try {
					var json = JsonConvert.SerializeObject (user);

					var jsonContent = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "login", jsonContent);

					var res = await response.Content.ReadAsStringAsync ();

					System.Diagnostics.Debug.WriteLine ("Response Message: " + response.RequestMessage);
					System.Diagnostics.Debug.WriteLine ("Response Content: " + res);

					if (response.IsSuccessStatusCode) {					
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode.ToString() + " " + res);

						var jsonObject = JsonConvert.DeserializeObject<IDictionary<string, string>> (res);

						var token = (string) jsonObject ["token"];
						System.Diagnostics.Debug.WriteLine ("Token: " + token);

						Yuvish.Helpers.Settings.TokenLogin = token;
						App.UserToken = token;

						App.CurrentUser = GetMyInfo();

						tcs.SetResult ("OK");
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode.ToString() + " " + res);
						tcs.SetResult (response.StatusCode.ToString());
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult ("Exception");
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public bool LoginFacebook (string tokenFacebook)
		{
			System.Diagnostics.Debug.WriteLine ("REST LoginFacebook...");

			var tcs = new TaskCompletionSource<bool> ();

			new Task (async () => {

				try {
					var obj = new { facebookAccessToken = tokenFacebook };

					var json = JsonConvert.SerializeObject (obj);

					var content = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "login", content);

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {					
						System.Diagnostics.Debug.WriteLine ("Success!");
						System.Diagnostics.Debug.WriteLine ("Response Message: " + res);

						var jsonObject = JsonConvert.DeserializeObject<IDictionary<string, string>> (res);

						var token = (string) jsonObject ["token"];
						System.Diagnostics.Debug.WriteLine ("Token: " + token);

						Yuvish.Helpers.Settings.TokenLogin = token;
						App.UserToken = token;

						App.CurrentUser = GetMyInfo ();

						tcs.SetResult (true);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + " " + res);
						tcs.SetResult (false);
					}
				} catch (Exception e) {
					tcs.SetResult (false);
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public bool Logout (UserLogoutModel user)
		{
			System.Diagnostics.Debug.WriteLine ("REST Logout...");

			var tcs = new TaskCompletionSource<bool> ();

			new Task(async () => {
				try {
					var json = JsonConvert.SerializeObject (user);

					var content = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "logout", content);

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode.ToString() + " " + res);

						tcs.SetResult(true);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + " " + res);
						tcs.SetResult(false);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult(false);
				}
		   	}).Start();

			return tcs.Task.Result;
		}

		public UserModel GetMyInfo ()
		{
			System.Diagnostics.Debug.WriteLine ("REST GetMyInfo...");

			var tcs = new TaskCompletionSource<UserModel> ();

			new Task (async () => {
				try {
					var obj = new { token = App.UserToken };
					
					var json = JsonConvert.SerializeObject (obj);
					
					var content = new StringContent (json, Encoding.UTF8, _typeContent);
					
					HttpResponseMessage response = null;
					
					response = await Client.PostAsync(_uriBase + "obtenerMiInfo", content);
					
					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode + " " + res);
						
						var user = JsonConvert.DeserializeObject <UserModel> (res);

						tcs.SetResult (user);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + " " + res);
						tcs.SetResult (null);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (null);
				}
			}).Start();
				
			return tcs.Task.Result;
		}

		public bool EditMyInfo (UserModel user, string pswd)
		{
			System.Diagnostics.Debug.WriteLine ("REST EditMyInfo...");

			var tcs = new TaskCompletionSource<bool> ();

			new Task (async () => {
				try {
					var obj = new { 
						firstName = user.firstName, 
						lastName = user.lastName,
						mobile = user.mobile,
						token = App.UserToken, 
						password = pswd
					};

					var json = JsonConvert.SerializeObject (obj);

					var content = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "editarInfo", content);

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode + " " + res);

						var status = JsonConvert.DeserializeObject <bool> (res);

						tcs.SetResult (status);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + " " + res);
						tcs.SetResult (false);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public List<BrandModel> GetMarcasModelos()
		{
			System.Diagnostics.Debug.WriteLine ("REST GetMarcasModelos...");

			var tcs = new TaskCompletionSource<List<BrandModel>> ();
			new Task (async () => {
				try {
					var response = await Client.GetAsync (_uriBase + "obtenMarcasyModelos");

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode + " " + res);

						var brands = JsonConvert.DeserializeObject <List<BrandModel>> (res);

						tcs.SetResult (brands);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + " " + res);
						tcs.SetResult (null);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (null);
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public List<ServiceModel> GetServices (string size)
		{
			System.Diagnostics.Debug.WriteLine ("REST GetServices...");

			var tcs = new TaskCompletionSource<List<ServiceModel>> ();
			new Task (async () => {
				try {					
					var obj = new { 
						size = size
					};

					System.Diagnostics.Debug.WriteLine ("SIZE: " + size);

					var json = JsonConvert.SerializeObject (obj);

					var content = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync (_uriBase + "obtenServicios", content);

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode + " " + res);

						var services = JsonConvert.DeserializeObject <List<ServiceModel>> (res);

						tcs.SetResult (services);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + " " + res);
						tcs.SetResult (null);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (null);
				}
			}).Start ();

			return tcs.Task.Result;
		}

		public List<OrderRecModel> GetMyOrders ()
		{
			System.Diagnostics.Debug.WriteLine ("REST GetMyOrders...");

			var tcs = new TaskCompletionSource<List<OrderRecModel>> ();

			new Task (async () => {
				try {
					var obj = new { 
						token = App.UserToken
					};

					var json = JsonConvert.SerializeObject (obj);

					var content = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "obtenerMisServicios", content);

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode + " " + res);

						var orders = JsonConvert.DeserializeObject <List<OrderRecModel>> (res);

						tcs.SetResult (orders);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + " " + res);
						tcs.SetResult (null);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool SendOrder (OrderModel2 order)
		{
			System.Diagnostics.Debug.WriteLine ("REST SendOrder...");

			var tcs = new TaskCompletionSource<bool> ();

			new Task (async () => {
				try {
					var json = JsonConvert.SerializeObject (order);

					System.Diagnostics.Debug.WriteLine ("JSON SOLICITAR SERVICIO: " + json);

					var content = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "solicitarServicio", content);

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode + ", " + res);

						var orderStatus = JsonConvert.DeserializeObject <OrderStatusModel> (res);

						tcs.SetResult (true);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + ", " + res);
						tcs.SetResult (false);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (false);
				}
			}).Start();
				
			return tcs.Task.Result;
		}

		public bool ValidateCoverageArea (double latitude, double longitude)
		{
			System.Diagnostics.Debug.WriteLine ("REST ValidateCoverageArea...");

			var tcs = new TaskCompletionSource<bool> ();

			new Task (async () => {
				try {
					var area = new { token = App.UserToken, lat = latitude, lon = longitude };
					var json = JsonConvert.SerializeObject (area);

					var content = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "validatedLocation", content);

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode + ", " + res);

						var orderStatus = JsonConvert.DeserializeObject <bool> (res);

						tcs.SetResult (orderStatus);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + ", " + res);
						tcs.SetResult (false);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public TicketModel RestartPassword (string correo)
		{
			System.Diagnostics.Debug.WriteLine ("REST RestartPassword...");

			var tcs = new TaskCompletionSource<TicketModel> ();

			new Task (async () => {
				try {
					var obj = new { email = correo };
					var json = JsonConvert.SerializeObject (obj);

					var content = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "generarCambioPassTicket", content);

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode + ", " + res);

						var ticket = JsonConvert.DeserializeObject <TicketModel> (res);

						tcs.SetResult (ticket);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + ", " + res);
						tcs.SetResult (null);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool ValidateToken ()
		{
			System.Diagnostics.Debug.WriteLine ("REST ValidateToken...");

			var tcs = new TaskCompletionSource<bool> ();

			new Task (async () => {
				try {
					var obj = new { token = App.UserToken };
					var json = JsonConvert.SerializeObject (obj);

					var content = new StringContent (json, Encoding.UTF8, _typeContent);

					HttpResponseMessage response = null;

					response = await Client.PostAsync(_uriBase + "validateToken", content);

					var res = await response.Content.ReadAsStringAsync ();

					if (response.IsSuccessStatusCode) {
						System.Diagnostics.Debug.WriteLine ("Success: " + response.StatusCode + ", " + res);

						tcs.SetResult (true);
					} else {
						System.Diagnostics.Debug.WriteLine ("Error: " + response.StatusCode + ", " + res);
						tcs.SetResult (false);
					}
				} catch (Exception e) {
					System.Diagnostics.Debug.WriteLine ("Exception: " + e.Message);
					tcs.SetResult (false);
				}
			}).Start();

			return tcs.Task.Result;
		}
	}
}

