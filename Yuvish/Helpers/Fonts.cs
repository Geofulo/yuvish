﻿using System;
namespace Yuvish
{
	public static class Fonts
	{
		public static string Caslon_B = "ACaslonPro-Bold";
		public static string Caslon_B_I = "ACaslonPro-BoldItalic";
		public static string Caslon_I = "ACaslonPro-Italic";
		public static string Caslon = "ACaslonPro-Regular";
		public static string Caslon_SB = "ACaslonPro-Semibold";
		public static string Caslon_SB_I = "ACaslonPro-SemiboldItalic";

		public static string AvenirNext_B = "AvenirNextRoundedStd-Bold";
		public static string AvenirNext_B_I = "AvenirNextRoundedStd-BoldIt";
		public static string AvenirNext_D = "AvenirNextRoundedStd-Demi";
		public static string AvenirNext_D_I = "AvenirNextRoundedStd-DemiIt";
		public static string AvenirNext_I = "AvenirNextRoundedStd-Italic";
		public static string AvenirNext_M = "AvenirNextRoundedStd-Med";
		public static string AvenirNext_M_I = "AvenirNextRoundedStd-MedIt";
		public static string AvenirNext = "AvenirNextRoundedStd-Reg";

	}
}

