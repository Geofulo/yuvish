﻿using System;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Yuvish
{
	public class Help
	{
		Geocoder GeoCoder;

		public Help () {
			GeoCoder = new Geocoder ();
		}

		public string LocationToAddress (Position position) {			
			var tcs = new TaskCompletionSource<string> ();

			new Task (async () => {
				try {
					var possibleAddresses = await GeoCoder.GetAddressesForPositionAsync (position);	
					string res = "Dirección de prueba";
					foreach (var address in possibleAddresses) {
						System.Diagnostics.Debug.WriteLine ("Position to Address: " + address);
						res = address;
					}
					tcs.SetResult (res);
				} catch (Exception e) {
					tcs.SetResult (null);
				}
			}).Start ();

			return tcs.Task.Result;
		}
	}
}

