﻿using System;
using Xamarin.Forms.Maps;

namespace Yuvish
{
	public interface ILocation
	{
		bool AskWhenInUseAuthorization ();

		Position GetCurrentLocation ();
	}
}

