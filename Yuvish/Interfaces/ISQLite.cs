﻿using System;

namespace Yuvish
{
	public interface ISQLite
	{
		SQLite.SQLiteConnection GetConnection();
	}
}

