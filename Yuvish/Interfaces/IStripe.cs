﻿using System;
using System.Threading.Tasks;

namespace Yuvish
{
	public interface IStripe
	{
		Task<bool> GetToken (string creditCard, string cvc, string expYear, string expMonth);
	}
}

