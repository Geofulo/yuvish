﻿using System;

namespace Yuvish
{
	public class CarModel
	{
		public string idColor { get; set; }

		public string licensePlate { get; set; }

		public string details { get; set; }

		public int idBrand { get; set; }

		public int idModel { get; set; }
	}
}

