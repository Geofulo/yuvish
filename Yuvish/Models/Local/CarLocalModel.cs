﻿using System;
using SQLite;

namespace Yuvish
{
	public class CarLocalModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdCar { get; set; }

		public string UserEmail { get; set; }

		public string Nombre { get; set; }

		public string Marca { get; set; }

		public string Modelo { get; set; }

		public string ModeloId { get; set; }

		public string MarcaId { get; set; }

		public string Placas { get; set; }

		public string Color { get; set; }

		public string Detalles { get; set; }

		public string Size { get; set; }
	}
}

