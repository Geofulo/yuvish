﻿using System;
using SQLite;

namespace Yuvish
{
	public class CreditCardModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdCreditCard { get; set; }

		public string UserEmail { get; set; }

		public string BankName { get; set; }

		public string Number { get; set; }

		public string CVC { get; set; }

		public string ExpYear { get; set; }

		public string ExpMonth { get; set; }

		public string Card { 
			get {
				var last4 = Number.Substring (Number.Length - 4, 4);
				return "**** **** **** " + last4;
			}
		}

		public string CreditCardImage { 
			get {				
				if (Number.StartsWith("4"))
					return "tarjeta_visa.png";
				else if (Number.StartsWith("51") || Number.StartsWith("52") || Number.StartsWith("53") || Number.StartsWith("54") || Number.StartsWith("55"))
					return "tarjeta_master.png";
				else if (Number.StartsWith("34") || Number.StartsWith("37"))
					return "tarjeta_americanr.png";
				else
					return "";
			}
		}
	}
}

