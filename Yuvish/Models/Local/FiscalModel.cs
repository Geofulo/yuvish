﻿using System;
using SQLite;

namespace Yuvish
{
	public class FiscalModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdFiscal { get; set; }

		public string Nombre { get; set; }

		public string RFC { get; set; }

		public string Calle { get; set; }

		public string NumeroExterior { get; set; }

		public string NumeroInterior { get; set; }

		public string Colonia { get; set; }

		public string Municipio { get; set; }

		public string Estado { get; set; }

		public string CodigoPostal { get; set; }
	}
}

