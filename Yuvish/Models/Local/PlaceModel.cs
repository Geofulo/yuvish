﻿using System;
using SQLite;

namespace Yuvish
{
	public class PlaceModel
	{
		[PrimaryKey, AutoIncrement]
		public int IdPlace { get; set; }

		public string UserEmail { get; set; }

		public string Nombre { get; set; }

		public string Direccion { get; set; }

		public string Observaciones { get; set; }

		public double Latitud { get; set; }

		public double Longitud { get; set; }

		/*
		public string Direccion { 
			get { 
				var dir = Calle + " " + NumeroExterior + " Col. " + Colonia + " C.P. " + CodigoPostal;
				return dir;
			} 
		}*/
	}
}

