﻿using System;

namespace Yuvish
{
	public class ModeloModel
	{
		public string idModel { get; set; }

		public string name { get; set; }

		public string type { get; set; }

		public string size { get; set; }
	}
}

