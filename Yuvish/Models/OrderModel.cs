﻿using System;

namespace Yuvish
{
	public class OrderModel
	{
		public string YuvisherName { get; set; }

		public string ClientName { get; set; }

		public string TypeWash { get; set; }

		public string DateOrder { get; set; }

		public string StartTimeOrder { get; set; }

		public string EndTimeOrder { get; set; }

		public string AddressOrder { get; set; }

		public double Price { get; set; }
	}
}

