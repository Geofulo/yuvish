﻿using System;

namespace Yuvish
{
	public class OrderModel2
	{
		public string token { get; set; }

		public int[] services { get; set; }

		public CarModel clientCar { get; set; }

		public double lat { get; set; }

		public double lon { get; set; }

		public string datetime { get; set; }

		public string address { get; set; }

		public PaymentModel clientPayment { get; set; }
	}
}

