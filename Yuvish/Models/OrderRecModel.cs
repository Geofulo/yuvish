﻿using System;

namespace Yuvish
{
	public class OrderRecModel
	{
		public int idWash { get; set; }

		public string brand { get; set; }

		public string model { get; set; }

		public string color { get; set; }

		public string licensePlate { get; set; }

		public string address { get; set; }

		public double lat { get; set; }

		public double lon { get; set; }

		public string comments { get; set; }

		public string schedule { get; set; }

		public double amount { get; set; }

		public int idStatus { get; set; }

		public ServiceModel2[] servicios { get; set; }

		public string Car { 
			get {
				return brand + " " + model;
			}
		}

		public string Status {
			get {
				if (idStatus == 0)
					return "Pendiente";
				else if (idStatus == 1)
					return "En curso";				
				else if (idStatus == 2)
					return "Realizado";		
				else return "";
			}
		}

	}
}

