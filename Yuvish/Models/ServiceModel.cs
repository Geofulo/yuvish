﻿using System;

namespace Yuvish
{
	public class ServiceModel
	{
		public string idServicio { get; set; }

		public string nombre { get; set; }

		public string detalles { get; set; }

		public string precio { get; set; }
	}
}

