﻿using System;

namespace Yuvish
{
	public class UserLoginModel
	{
		public string email { get; set; }

		public string password { get; set; }

		public string appVersion { get; set; }

		public string deviceName { get; set; }
	}
}

