﻿using System;

namespace Yuvish
{
	public class UserModel
	{
		public string firstName { get; set; }

		public string lastName { get; set; }

		public string mobile { get; set; }

		public string email { get; set; }

	}
}

