﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public static class ColorResource
	{
		public static Color VerdeAgua = Color.FromHex("1BCFC9");

		public static Color VerdeObscuro = Color.FromHex("4E5758");

		public static Color GrisClaro = Color.FromHex("F1F1F2");

		public static Color Pantone3252c = Color.FromHex("2ad2c9");

		public static Color Pantone445c = Color.FromHex("505759");

		public static Color Pantone446c = Color.FromHex("3f4444");

		public static Color PantoneBlackC = Color.FromHex("2d2926");

		public static Color PantoneBlackC5 = Color.FromHex("f2f2f3");

		public static Color FacebookLoginButton = Color.FromHex("3d5a98");

		public static Color TwitterLoginButton = Color.FromHex("4099ff");
	}
}

