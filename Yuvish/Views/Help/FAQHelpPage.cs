﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class FAQHelpPage : ContentPage
	{
		Button BackMenuImage;

		StackLayout BarStack;
		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		public FAQHelpPage ()
		{
			Title = "FAQ";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void createElements ()
		{
			BackMenuImage = new Button ()
			{
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5, 20, 0),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Padding = new Thickness(0, 0, 0, 10),
				Spacing = 0,
				WidthRequest = App.ScreenWidth,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - 70,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Preguntas Frecuentes",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new Label {
								Text = "¿Qué es lavado en seco?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.StartAndExpand,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_01.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}							
						}	
					},
					new Label {
						Text = "Se trata de una modalidad de lavado que no utiliza agua, es un método alternativo que nos ayuda obtener incluso mejores resultados que en el lavado tradicional, ya que proporciona una capa protectora que además dar un brillo excepcional a la pintura de tu auto contribuye a ahorrar agua y a conservar el medio ambiente.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new Label {
								Text = "¿Se maltrata la pintura de mi auto?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_02.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}							
						}	
					},
					new Label {
						Text = "No, la razón es porque utilizamos una serie de productos específicamente diseñados para evitar que esto ocurra en cada tipo de superficies que tratamos, el acabado de tu auto no se verá afectado de ninguna manera, porque incluso nuestros técnicos están capacitados en el cuidado de tu auto.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							new Label {
								Text = "Mi auto está muy sucio, ¿Se puede lavar en seco?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.Fill,
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_03.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}							
						}	
					},
					new Label {
						Text = "Cuando la capa de suciedad es muy gruesa, lodo por ejemplo, no es recomendable aplicar tu auto con nuestro sistema, te recomendamos hacer un lavado tradicional y buscarnos con posterioridad. El lavado en seco se recomienda para el cuidado y mantenimiento de la belleza de tu auto.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new Label {
								Text = "¿Qué pasa con los residuos de chapopote en la parte baja?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_04.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}		
						}	
					},
					new Label {
						Text = "Hay contaminantes que se adhieren al acabado de tu auto, como chapopote, insectos de la carretera, residuos de los árboles y hasta excremento de aves y hacemos nuestro mejor esfuerzo para eliminarlos con nuestro sistema, pero en algunas ocasiones no es suficiente, para eso es recomendable aplicar el descontaminado de pintura también.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new Label {
								Text = "¿Y si quiero que lo limpien por dentro?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_05.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}		
						}	
					},
					new Label {
						Text = "El servicio básico sólo implica la limpieza exterior, si deseas que aspiremos los interiores selecciona el paquete que lo incluye cuando agendes tu lavado, cuando lleguemos a tu auto te avisaremos para que nos des acceso al interior, pero toma en cuenta que dispones sólo de 15 minutos a partir del momento en que lleguemos.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new Label {
								Text = "¿Qué hay de los rines, llantas y vidrios?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_06.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}		
						}	
					},
					new Label {
						Text = "Nuestro servicio básico incluye también la limpieza de estas superficies de tu auto, utilizando para ello los productos y las microfibras apropiados para cada una de ellas aplicando el mismo esmero con el que limpiamos el resto de la superficie.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new Label {
								Text = "Acabo de encerar mi auto, ¿este método de lavado quitará la cera?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_07.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}		
						}	
					},
					new Label {
						Text = "Definitivamente no, los productos que utilizamos sirven para quitar la suciedad del auto, su composición química no remueve o diluye la cera ni cualquier otra protección que previamente hayas aplicado a tu auto.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new Label {
								Text = "Estaciono mi auto en un lugar privado, ¿me pueden dar el servicio ahí?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_08.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}		
						}	
					},
					new Label {
						Text = "Por supuesto, pero es importante que no olvides hacer los arreglos necesarios para que nos permitan entrar a donde está tu auto, nuestro personal está perfectamente identificado para tu seguridad.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new Label {
								Text = "¿Cuánto se tardan en lavar mi auto?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_09.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}		
						}	
					},
					new Label {
						Text = "Depende del tamaño de tu auto y del servicio que hayas solicitado, nos puede tomar entre 20 y 30 minutos si se trata de los servicios básicos, si solicitas servicio como el descontaminado y abrillantado de pintura el tiempo podría ser de hasta 2 horas.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15, 20),
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new Label {
								Text = "¿Le tengo que dar propina al que lava el auto?",
								FontFamily = Fonts.AvenirNext_M,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand
							},
							new ContentView {
								WidthRequest = 60,
								HorizontalOptions = LayoutOptions.End,
								Content = new Image {
									Source = "ic_faq_10.png",
									HorizontalOptions = LayoutOptions.Center,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								}
							}		
						}	
					},
					new Label {
						Text = "En Yuvish no manejamos efectivo, y nuestro personal no está autorizado para recibir o solicitar propinas. Nuestra mejor recompensa es que califiques y recomiendes nuestro servicio, no sólo nos generas la retribución apropiada sino que con eso  ayudas a cuidar el planeta evitando que tus amigos y familiares desperdicien agua.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand
					},
				}
			});
			Stack.Children.Add (new BoxView {				
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			Scroll.Content = Stack;

			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Scroll,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar - 10)
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync();
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page FAQHelpPage");
		}
	}
}

