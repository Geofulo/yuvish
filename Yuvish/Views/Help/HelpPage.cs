﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class HelpPage : ContentPage
	{
		Button BackMenuImage;

		StackLayout BarStack;
		StackLayout Stack;
		RelativeLayout Relative;

		TapGestureRecognizer TapGestureQuestions = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureWeb = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureSocio = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureCall = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureMail = new TapGestureRecognizer();

		public HelpPage ()
		{
			Title = "Ayuda";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void createElements ()
		{
			BackMenuImage = new Button ()
			{
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5, 20, 0),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Padding = new Thickness(0, 0, 0, 10),
				Spacing = 20,
				WidthRequest = App.ScreenWidth,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Ayuda",
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});


			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				HeightRequest = 100,
				Children = {
					new StackLayout {
						BackgroundColor = ColorResource.Pantone3252c,
						WidthRequest = App.ScreenWidth / 2,
						Children = {
							new StackLayout {
								BackgroundColor = ColorResource.Pantone3252c,
								WidthRequest = App.ScreenWidth / 2,
								VerticalOptions = LayoutOptions.CenterAndExpand,
								Children = {
									new Image {
										Source = "ic_phone_perfil.png",
										HorizontalOptions = LayoutOptions.CenterAndExpand,
									},
									new Label {
										Text = "4455 6677",
										FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
										TextColor = ColorResource.Pantone446c,
										HorizontalOptions = LayoutOptions.CenterAndExpand,
									}
								}
							},
						},
						GestureRecognizers = {
							TapGestureCall
						}
					},
					new StackLayout {
						BackgroundColor = Color.FromHex("88e2df"),
						WidthRequest = App.ScreenWidth / 2,
						Children = {
							new StackLayout {								
								WidthRequest = App.ScreenWidth / 2,
								VerticalOptions = LayoutOptions.CenterAndExpand,
								Children = {
									new Image {
										Source = "ic_at.png",
										HorizontalOptions = LayoutOptions.CenterAndExpand,
									},
									new Label {
										Text = "ayuda@yuvish.com",
										FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
										TextColor = ColorResource.Pantone446c,
										HorizontalOptions = LayoutOptions.CenterAndExpand,
									}
								}
							}
						},
						GestureRecognizers = {
							TapGestureMail
						}
					},
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 20,
				Padding = new Thickness(20, 0),
				HeightRequest = 60,
				Children = {
					new Image {
						Source = "ic_question.png",
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
					new Label {
						Text = "Preguntas frecuentes",
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
					new Image {
						Source = "ic_list_view.png",
						VerticalOptions = LayoutOptions.CenterAndExpand,
						HorizontalOptions = LayoutOptions.EndAndExpand,
					}
				},
				GestureRecognizers = {
					TapGestureQuestions
				}
			});

			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 20,
				Padding = new Thickness(20, 0),
				HeightRequest = 60,
				Children = {
					new Image {
						Source = "ic_web.png",
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
					new Label {
						Text = "www.yuvish.com",
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
				},
				GestureRecognizers = {
					TapGestureWeb
				}
			});
			/*
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 20,
				Padding = new Thickness(20, 0),
				HeightRequest = 60,
				Children = {
					new Image {
						Source = "ic_person.png",
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
					new Label {
						Text = "¿Quieres ser socio?",
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
					new Image {
						Source = "ic_list_view.png",
						VerticalOptions = LayoutOptions.CenterAndExpand,
						HorizontalOptions = LayoutOptions.EndAndExpand,
					}
				}
			});
			*/


			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Stack,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar - 10)
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync();

			TapGestureQuestions.Tapped += (sender, e) => {
				Navigation.PushAsync(new FAQHelpPage());
			};

			TapGestureWeb.Tapped += (sender, e) => {
				Navigation.PushAsync(new WebHelpPage());
			};

			TapGestureCall.Tapped += (sender, e) => {
				DependencyService.Get<IPhone>().Call();
			};

			TapGestureMail.Tapped += (sender, e) => {
				DependencyService.Get<IEmail>().SendEmail();
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page HelpPage");
		}
	}
}

