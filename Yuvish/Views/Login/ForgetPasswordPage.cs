﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class ForgetPasswordPage : ContentPage
	{
		Image BackImage;
		Image LogoImage;

		EntryCustom EmailEntry;
		Button OKButton;

		StackLayout Stack;

		TapGestureRecognizer TapGestureBack = new TapGestureRecognizer();

		public ForgetPasswordPage ()
		{
			Title = "Olvidé la contraseña";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);
			BackgroundImage = "login_bg.jpg";
			Content = Stack;
		}

		void createElements ()
		{
			LogoImage = new Image {
				Source = ImageSource.FromResource ("registro_logo.png"),	
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			BackImage = new Image {
				Source = ImageSource.FromResource ("back_white.png"),	
				Scale = 0.75f,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				GestureRecognizers = {
					TapGestureBack
				}
			};

			EmailEntry = new EntryCustom {
				Placeholder = "Correo",
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Entry)),
				BackgroundColor = Color.White,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			OKButton = new Button {
				Text = "Aceptar",
				TextColor = Color.White,
				BackgroundColor = ColorResource.PantoneBlackC,
				BorderRadius = 20,
				HeightRequest = 40,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 15,
				Padding = new Thickness (30, 20),
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (BackImage);
			Stack.Children.Add (new StackLayout {
				Padding = new Thickness (0, 20),
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = {
					LogoImage,
					new Label {
						Text = "Recuperar contraseña",
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Entry)),
						TextColor = Color.White,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
					},
				},
			});
			Stack.Children.Add (new Label {
				Text = "Ingresa tu correo para recuperar tu contraseña",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				TextColor = Color.White,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			});
			Stack.Children.Add (new ContentViewCustom {
				Radius = 20,
				Padding = new Thickness (15, 0),
				BackgroundColor = Color.White,
				Content = EmailEntry,
			});
			Stack.Children.Add (OKButton);
		}

		void setListeners ()
		{
			TapGestureBack.Tapped += (sender, e) => Navigation.PopAsync ();

			OKButton.Clicked += (sender, e) => {
				var email = EmailEntry.Text;

				if (!string.IsNullOrEmpty (email)) {
					var ticket = App.WSRest.RestartPassword (email);
					if (ticket != null) {
						DisplayAlert ("Correcto", "Se le ha enviado un correo para continuar con el proceso de restauración de contraseña", "OK");
					} else {
						DisplayAlert ("", "Ocurrió un error al intentar enviar el correo", "OK");
					}
				} else {
					DisplayAlert ("", "Debe ingresar el correo", "OK");
				}
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page ForgetPasswordPage");
		}
	}
}

