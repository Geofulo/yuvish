﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class LoginPage : ContentPage
	{
		Image LogoImage;

		Button FacebookLoginButton;

		EntryCustom EmailEntry;
		EntryCustom PasswordEntry;
		Button LoginButton;
		Button ForgetPasswordButton;
		Button SignupButton;

		StackLayout Stack;
		ScrollView Scroll;

		public LoginPage ()
		{
			Title = "Login";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);
			//BackgroundColor = ColorResource.Pantone3252c;
			BackgroundImage = "login_bg.jpg";

			Content = Scroll;
		}

		void createElements ()
		{
			LogoImage = new Image {
				Source = "login_logo2.png",	
				Scale = 1.5f,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};

			FacebookLoginButton = new Button
			{
				Text = "Ingresar con Facebook",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				TextColor = Color.White,
				BackgroundColor = ColorResource.FacebookLoginButton,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			EmailEntry = new EntryCustom {
				Placeholder = "Correo",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				Keyboard = Keyboard.Email,
				BackgroundColor = Color.White,
				IsNoBorder = true,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			PasswordEntry = new EntryCustom {
				Placeholder = "Contraseña",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				IsPassword = true,
				BackgroundColor = Color.White,
				IsNoBorder = true,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			LoginButton = new Button {
				Text = "Iniciar sesión",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Button)),
				TextColor = Color.White,
				BackgroundColor = ColorResource.PantoneBlackC,
				BorderRadius = 20,
				HeightRequest = 40,	
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			ForgetPasswordButton = new Button {
				Text = "Olvidé mi contraseña",
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Entry)),
				TextColor = Color.White,
				BackgroundColor = Color.Transparent,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			SignupButton = new Button {
				Text = "Registrarme",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				TextColor = ColorResource.PantoneBlackC,
				BackgroundColor = ColorResource.PantoneBlackC5,
				BorderRadius = 20,
				HeightRequest = 40,	
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Padding = new Thickness (30),
				Spacing = 30,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			Scroll = new ScrollView ();
		}

		void setLayouts ()
		{			
			Stack.Children.Add (new ContentView {
				Padding = new Thickness (0, 20, 0, 40),
				Content = LogoImage,
			});
			Stack.Children.Add (FacebookLoginButton);
			Stack.Children.Add (new Label {
				Text = "o ingresa usando tu cuenta",
				FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
				TextColor = Color.White,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Children = {
					new ContentViewCustom {
						Radius = 20,
						Padding = new Thickness (15, 0),
						BackgroundColor = Color.White,
						Content = EmailEntry,
					},
					new ContentViewCustom {
						Radius = 20,
						Padding = new Thickness (15, 0),
						BackgroundColor = Color.White,
						Content = PasswordEntry,
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 5,
				Children = {
					LoginButton,
					ForgetPasswordButton
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				VerticalOptions = LayoutOptions.EndAndExpand,
				Spacing = 10,
				Children = {
					new Label {
						Text = "¿Aún no tienes una cuenta?",
						FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
						TextColor = Color.White,
						VerticalOptions = LayoutOptions.CenterAndExpand,
						HorizontalOptions = LayoutOptions.FillAndExpand,
					},
					SignupButton
				}
			});

			Scroll.Content = Stack;
		}

		void setListeners ()
		{
			LoginButton.Clicked += (sender, e) => {
				if (!string.IsNullOrEmpty(EmailEntry.Text) && !string.IsNullOrEmpty(PasswordEntry.Text)) {
					var userCreated = new UserLoginModel {
						email = EmailEntry.Text,
						password = PasswordEntry.Text,
						appVersion = "",
						deviceName = "",
					};						

					App.UserToken = App.WSRest.Login (userCreated);

					if (App.WSRest.Login (userCreated).Equals("OK")) {				
						Yuvish.Helpers.Settings.TokenLogin = App.UserToken;

						App.CurrentUser = App.WSRest.GetMyInfo ();

						DependencyService.Get<ILocation>().AskWhenInUseAuthorization ();

						Application.Current.MainPage = new NavigationPage (new LocationServicePage()){
							BarBackgroundColor = ColorResource.PantoneBlackC5,
							BarTextColor = ColorResource.Pantone445c,
						};
					} else if (App.WSRest.Login (userCreated).Equals("Unauthorized")){
						DisplayAlert ("", "El usuario o contraseña es invalido", "OK");
					} else {
						DisplayAlert ("", "Ocurrió un error al intentar acceder", "OK");
					}
				} else {
					DisplayAlert ("", "No ingreso el correo o la contraseña", "OK");
				}
			};
			SignupButton.Clicked += (sender, e) => {
				Navigation.PushAsync (new SignupPage());
			};

			FacebookLoginButton.Clicked += (sender, e) => {
				EmailEntry.Unfocus();
				PasswordEntry.Unfocus();
				DependencyService.Get<IFacebook>().Login();

				/*
				var isLogin = DependencyService.Get<IFacebook>().Login();
				if (isLogin) {
					Application.Current.MainPage = new NavigationPage (new LocationServicePage()){
						BarBackgroundColor = ColorResource.PantoneBlackC5,
						BarTextColor = ColorResource.Pantone445c,
					};
				} else {
					System.Diagnostics.Debug.WriteLine ("No login");
				}
				*/
			};

			ForgetPasswordButton.Clicked += (sender, e) => Navigation.PushAsync (new ForgetPasswordPage ());

		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page LoginPage");
		}
	}
}

