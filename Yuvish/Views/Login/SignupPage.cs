﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class SignupPage : ContentPage
	{
		Image BackImage;
		Image LogoImage;

		Label ErrorLabel;
		EntryCustom NombreEntry;
		EntryCustom ApellidosEntry;
		EntryCustom TelefonoEntry;
		EntryCustom CorreoEntry;
		EntryCustom PasswordEntry;
		EntryCustom Password2Entry;
		Button SignupButton;

		StackLayout Stack;
		ScrollView Scroll;

		TapGestureRecognizer TapGestureBack = new TapGestureRecognizer();

		public SignupPage ()
		{
			Title = "Registrarse";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);
			BackgroundImage = "login_bg.jpg";
			Content = Scroll;
		}

		void createElements ()
		{
			BackImage = new Image {
				Source = ImageSource.FromResource ("back_white.png"),	
				Scale = 0.75f,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				GestureRecognizers = {
					TapGestureBack
				}
			};

			LogoImage = new Image {
				Source = ImageSource.FromResource ("registro_logo.png"),	
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			ErrorLabel = new Label {
				IsVisible = false,
				FontAttributes = FontAttributes.Italic,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				TextColor = Color.White,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			NombreEntry = new EntryCustom {
				Placeholder = "Nombre",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				BackgroundColor = Color.White,
				IsNoBorder = true,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			ApellidosEntry = new EntryCustom {
				Placeholder = "Apellidos",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				BackgroundColor = Color.White,
				IsNoBorder = true,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			TelefonoEntry = new EntryCustom {
				Placeholder = "Teléfono celular",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				Keyboard = Keyboard.Telephone,
				BackgroundColor = Color.White,
				IsNoBorder = true,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			CorreoEntry = new EntryCustom {
				Placeholder = "Correo",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				Keyboard = Keyboard.Email,
				BackgroundColor = Color.White,
				IsNoBorder = true,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			PasswordEntry = new EntryCustom {
				Placeholder = "Contraseña",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				IsPassword = true,
				BackgroundColor = Color.White,
				IsNoBorder = true,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			Password2Entry = new EntryCustom {
				Placeholder = "Repetir contraseña",
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				IsPassword = true,
				BackgroundColor = Color.White,
				IsNoBorder = true,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			SignupButton = new Button {
				Text = "Regístrate",
				TextColor = Color.White,
				BackgroundColor = ColorResource.PantoneBlackC,
				BorderRadius = 20,
				HeightRequest = 40,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 15,
				Padding = new Thickness (30, 20),
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			Scroll = new ScrollView ();
		}

		void setLayouts ()
		{
			Stack.Children.Add (BackImage);
			Stack.Children.Add (new StackLayout {
				Padding = new Thickness (0, 20),
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Children = {
					LogoImage,
					new Label {
						Text = "Registro",
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Entry)),
						TextColor = Color.White,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
					},
				},
			});
			Stack.Children.Add (ErrorLabel);
			Stack.Children.Add (new ContentViewCustom {
				Radius = 20,
				Padding = new Thickness (15, 0),
				BackgroundColor = Color.White,
				Content = NombreEntry,
			});
			Stack.Children.Add (new ContentViewCustom {
				Radius = 20,
				Padding = new Thickness (15, 0),
				BackgroundColor = Color.White,
				Content = ApellidosEntry,
			});
			Stack.Children.Add (new ContentViewCustom {
				Radius = 20,
				Padding = new Thickness (15, 0),
				BackgroundColor = Color.White,
				Content = TelefonoEntry,
			});
			Stack.Children.Add (new ContentViewCustom {
				Radius = 20,
				Padding = new Thickness (15, 0),
				BackgroundColor = Color.White,
				Content = CorreoEntry,
			});
			Stack.Children.Add (new ContentViewCustom {
				Radius = 20,
				Padding = new Thickness (15, 0),
				BackgroundColor = Color.White,
				Content = PasswordEntry,
			});
			Stack.Children.Add (new ContentViewCustom {
				Radius = 20,
				Padding = new Thickness (15, 0),
				BackgroundColor = Color.White,
				Content = Password2Entry,
			});
			Stack.Children.Add (SignupButton);

			Scroll.Content = Stack;
		}

		void setListeners ()
		{
			TapGestureBack.Tapped += (sender, e) => Navigation.PopAsync ();

			Password2Entry.Completed += (sender, e) => {
				if (Password2Entry.Text.Equals(PasswordEntry.Text)) {
					ErrorLabel.IsVisible = false;
				} else {
					ErrorLabel.Text = "Las contraseñas no coinciden";
					ErrorLabel.IsVisible = true;
				}
			};

			SignupButton.Clicked += (sender, e) => {
				NombreEntry.Unfocus();
				ApellidosEntry.Unfocus();
				TelefonoEntry.Unfocus();
				CorreoEntry.Unfocus();
				PasswordEntry.Unfocus();
				Password2Entry.Unfocus();

				if (string.IsNullOrEmpty(NombreEntry.Text) ||
					string.IsNullOrEmpty(ApellidosEntry.Text) ||
					string.IsNullOrEmpty(TelefonoEntry.Text) ||
					string.IsNullOrEmpty(CorreoEntry.Text) ||
					string.IsNullOrEmpty(PasswordEntry.Text)) {

					ErrorLabel.Text = "Debes ingresar todos los datos";
					ErrorLabel.IsVisible = true;
				} else {
					if (!PasswordEntry.Text.Equals(Password2Entry.Text)) {
						DisplayAlert ("", "Las contraseñas no coinciden", "OK");
					} else {
						ErrorLabel.IsVisible = false;

						var userCreated = new UserRegisterModel {
							firstName = NombreEntry.Text,
							lastName = ApellidosEntry.Text,
							mobile = TelefonoEntry.Text,
							email = CorreoEntry.Text,
							password = PasswordEntry.Text,
						};

						var signedUp = App.WSRest.Signup (userCreated);

						if (signedUp) {

							DependencyService.Get<ILocation>().AskWhenInUseAuthorization ();

							Application.Current.MainPage = new NavigationPage (new LocationServicePage()){
								BarBackgroundColor = ColorResource.PantoneBlackC5,
								BarTextColor = ColorResource.Pantone445c,
							};
						} else {
							DisplayAlert ("", "Ocurrió un error al intentar registrarse", "OK");
						}
					}
				}
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page SigupPage");
		}
	}
}

