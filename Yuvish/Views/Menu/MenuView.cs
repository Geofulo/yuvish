﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class MenuView : ContentViewCustom
	{
		Button CloseButton;
		Button LogoutButton;

		Image UserImage;
		Label UserNameLabel;

		Label ProfileLabel;
		Label OrdersLabel;
		Label NotificationsLabel;
		Label HelpLabel;
		Label FAQLabel;
		Label WebLabel;

		Image ProfileIcon;
		Image OrdersIcon;
		Image FAQIcon;
		Image WebIcon;

		ViewCell ProfileCell;
		ViewCell OrdersCell;
		ViewCell NotificationsCell;
		ViewCell HelpCell;
		ViewCell FAQCell;
		ViewCell WebCell;

		TableView MenuTableView;

		StackLayout Stack;

		TapGestureRecognizer TapGestureCall = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureMail = new TapGestureRecognizer();

		public MenuView ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			IsLightBlur = true;
			WidthRequest = App.ScreenWidth;
			HeightRequest = App.ScreenHeight - 20;
			Content = Stack;
		}

		void createElements ()
		{			
			CloseButton = new Button {
				Image = "ic_cancel.png",
				HorizontalOptions = LayoutOptions.StartAndExpand,
			};
			LogoutButton = new Button {
				Image = "ic_logout.png",
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};

			UserImage = new Image {
				//Source = ImageSource.FromFile (App.ProfilePhoto),
				Source = "ic_gota2.png",
				//HorizontalOptions = LayoutOptions.CenterAndExpand,
			};

			UserNameLabel = new Label {
				Text = App.CurrentUser.firstName + " " + App.CurrentUser.lastName,
				Font = Font.OfSize(Fonts.AvenirNext_M, NamedSize.Medium),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};

			ProfileLabel = getLabelMenu("Perfil");
			OrdersLabel = getLabelMenu("Servicios");
			NotificationsLabel = getLabelMenu("Notificaciones");
			HelpLabel = getLabelMenu("Ayuda");
			FAQLabel = getLabelMenu("Preguntas frecuentes");
			WebLabel = getLabelMenu("www.yuvish.com");


			ProfileIcon = new Image {
				Source = "ic_person.png",
			};
			OrdersIcon = new Image {
				Source = "ic_servicios.png",
			};
			FAQIcon = new Image {
				Source = "ic_question.png",
			};
			WebIcon = new Image {
				Source = "ic_web.png",
			};


			ProfileCell = new ViewCellCustom {
				SelectedBackgroundColor = ColorResource.Pantone445c,
				View = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Padding = new Thickness(30, 0),
					Spacing = 20,
					Children = {
						ProfileIcon,
						ProfileLabel
					}
				}
			};
			OrdersCell = new ViewCellCustom {		
				SelectedBackgroundColor = ColorResource.Pantone445c,
				View = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Padding = new Thickness(30, 0),
					Spacing = 20,
					Children = {
						OrdersIcon,
						OrdersLabel
					}
				}
			};
			NotificationsCell = new ViewCellCustom {
				SelectedBackgroundColor = ColorResource.Pantone445c,
				View = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Padding = new Thickness(30, 0),
					Spacing = 20,
					Children = {						
						NotificationsLabel
					}
				}
			};
			HelpCell = new ViewCellCustom {
				SelectedBackgroundColor = ColorResource.Pantone445c,
				View = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Padding = new Thickness(30, 0),
					Spacing = 20,
					Children = {
						HelpLabel
					}
				}
			};
			FAQCell = new ViewCellCustom {
				SelectedBackgroundColor = ColorResource.Pantone445c,
				View = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Padding = new Thickness(30, 0),
					Spacing = 20,
					Children = {
						FAQIcon,
						FAQLabel
					}
				}
			};
			WebCell = new ViewCellCustom {
				SelectedBackgroundColor = ColorResource.Pantone445c,
				View = new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Padding = new Thickness(30, 0),
					Spacing = 20,
					Children = {
						WebIcon,
						WebLabel
					}
				}
			};

			MenuTableView = new TableView {				
				Root = new TableRoot {															
					new TableSection {						
						ProfileCell,
						OrdersCell,
						//NotificationsCell,
						//HelpCell,
						FAQCell,
						WebCell
					},
				},
				Intent = TableIntent.Settings,
				BackgroundColor = Color.Transparent,
				RowHeight = 55,
				//VerticalOptions = LayoutOptions.Fill,
			};
		}

		Label getLabelMenu(string text)
		{
			return new Label
			{
				Text = text,
				Font = Font.OfSize(Fonts.AvenirNext, NamedSize.Medium),
				TextColor = ColorResource.Pantone446c,
				HorizontalTextAlignment = TextAlignment.Start,
				VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Spacing = 0,
			};
		}

		void setLayouts ()
		{
			Stack.Children.Add (new StackLayout {
				Padding = new Thickness (20, 10, 20, 0),
				Orientation = StackOrientation.Horizontal,
				Children = {
					CloseButton,
					LogoutButton
				}
			});

			Stack.Children.Add (new ContentView {
				Padding = new Thickness(0, 0, 0, 10),
				Content = UserImage,
			});

			if (UserNameLabel != null) {
				Stack.Children.Add (new StackLayout {
					Spacing = 0,
					Children = {
						UserNameLabel,
					}
				});
			}
			Stack.Children.Add (MenuTableView);

			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				HeightRequest = 100,
				VerticalOptions = LayoutOptions.EndAndExpand,
				Children = {
					new StackLayout {
						BackgroundColor = ColorResource.Pantone3252c,
						WidthRequest = App.ScreenWidth / 2,
						Children = {
							new StackLayout {
								BackgroundColor = ColorResource.Pantone3252c,
								WidthRequest = App.ScreenWidth / 2,
								VerticalOptions = LayoutOptions.CenterAndExpand,
								Children = {
									new Image {
										Source = "ic_phone_perfil.png",
										HorizontalOptions = LayoutOptions.CenterAndExpand,
									},
									new Label {
										Text = "01 800 3269274",
										Font = Font.OfSize(Fonts.AvenirNext, NamedSize.Medium),
										TextColor = ColorResource.Pantone446c,
										HorizontalOptions = LayoutOptions.CenterAndExpand,
									}
								}
							},
						},
						GestureRecognizers = {
							TapGestureCall
						}
					},
					new StackLayout {
						BackgroundColor = Color.FromHex("88e2df"),
						WidthRequest = App.ScreenWidth / 2,
						Children = {
							new StackLayout {								
								WidthRequest = App.ScreenWidth / 2,
								VerticalOptions = LayoutOptions.CenterAndExpand,
								Children = {
									new Image {
										Source = "ic_at.png",
										HorizontalOptions = LayoutOptions.CenterAndExpand,
									},
									new Label {
										Text = "hola@yuvish.com",
										Font = Font.OfSize(Fonts.AvenirNext, NamedSize.Medium),
										TextColor = ColorResource.Pantone446c,
										HorizontalOptions = LayoutOptions.CenterAndExpand,
									}
								}
							}
						},
						GestureRecognizers = {
							TapGestureMail
						}
					},
				}
			});
		}

		void setListeners ()
		{			
			CloseButton.Clicked += (sender, e) => {
				this.TranslateTo(-App.ScreenWidth, 0, easing: Easing.CubicOut);
			};
			LogoutButton.Clicked += (sender, e) => {
				var isLogout = App.WSRest.Logout(new UserLogoutModel {
					token = App.UserToken
				});

				if (isLogout)
				{
					Application.Current.MainPage = new LoginPage();
				}
				else {					
					//DisplayAlert("", "Ocurrió un error al intentar salir", "OK");
				}
			};

			ProfileCell.Tapped += (sender, e) => {				
//				ProfileLabel.TextColor = ColorResource.PantoneBlackC5;
//				ProfileIcon.Source = "ic_person_light.png";
				this.Navigation.PushAsync (new ProfilePage());
			};
			OrdersCell.Tapped += (sender, e) => {
//				OrdersLabel.TextColor = ColorResource.PantoneBlackC5;
//				OrdersIcon.Source = "ic_servicios_light.png";
				this.Navigation.PushAsync (new OrdersPage());
			};
			NotificationsCell.Tapped += (sender, e) => {
				NotificationsLabel.TextColor = ColorResource.PantoneBlackC5;
				//Navigation.PushAsync (new NotificationsPage());
			};
			HelpCell.Tapped += (sender, e) => {
				HelpLabel.TextColor = ColorResource.PantoneBlackC5;
				this.Navigation.PushAsync (new HelpPage());	
			};
			FAQCell.Tapped += (sender, e) => {
//				FAQLabel.TextColor = ColorResource.PantoneBlackC5;
//				FAQIcon.Source = "ic_question_light.png";
				Navigation.PushAsync(new FAQHelpPage());
			};
			WebCell.Tapped += (sender, e) => {
				//Navigation.PushAsync(new WebHelpPage());
//				WebLabel.TextColor = ColorResource.PantoneBlackC5;
//				WebIcon.Source = "ic_web_light.png";
				DependencyService.Get<IWeb>().OpenUrl("http://www.yuvish.com");
			};

			TapGestureCall.Tapped += (sender, e) => {
				DependencyService.Get<IPhone>().Call();
			};

			TapGestureMail.Tapped += (sender, e) => {
				DependencyService.Get<IEmail>().SendEmail();
			};
		}

		public void deselectCells ()
		{
			/*
			ProfileLabel.TextColor = ColorResource.Pantone445c;
			OrdersLabel.TextColor = ColorResource.Pantone445c;
			NotificationsLabel.TextColor = ColorResource.Pantone445c;
			HelpLabel.TextColor = ColorResource.Pantone445c;
			FAQLabel.TextColor = ColorResource.Pantone445c;
			WebLabel.TextColor = ColorResource.Pantone445c;

			ProfileIcon.Source = "ic_person.png";
			OrdersIcon.Source = "ic_servicios.png";
			FAQIcon.Source = "ic_question.png";
			WebIcon.Source = "ic_web.png";
			*/
		}
	}
}

