﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class OrdersPage : ContentPage
	{
		Button BackMenuImage;

		ContentView ScheduledView;
		ContentView HistorialView;

		Button ScheduledButton;
		Button HistorialButton;

		Label WashAvailableLabel;

		ContentView OrdersContent;

		StackLayout BarStack;
		StackLayout Stack;
		RelativeLayout Relative;

		public OrdersPage ()
		{
			Title = "Servicios";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void createElements ()
		{
			BackMenuImage = new Button ()
			{
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			ScheduledButton = new Button {
				Text = "Agendados",
				TextColor = ColorResource.PantoneBlackC5,
				BackgroundColor = ColorResource.Pantone445c,
				BorderRadius = 0,
				WidthRequest = App.ScreenWidth / 2,
				HeightRequest = 60,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			HistorialButton = new Button {
				Text = "Historial",
				TextColor = ColorResource.Pantone445c,
				BackgroundColor = ColorResource.PantoneBlackC5,
				BorderRadius = 0,
				WidthRequest = App.ScreenWidth / 2,
				HeightRequest = 60,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};

			ScheduledView = new ScheduledOrdersView ();

			HistorialView = new HistorialOrdersView ();

			WashAvailableLabel = new Label {
				Text = "Tienes 2 lavados disponibles",
				TextColor = Color.White,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center
			};
		}

		void createLayouts ()
		{
			OrdersContent = new ContentView {
				BackgroundColor = ColorResource.PantoneBlackC5,
			};

			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5, 20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 0,
				WidthRequest = App.ScreenWidth,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			OrdersContent.Content = ScheduledView;

			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Servicios",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});


			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				Children = {
					ScheduledButton, 
					HistorialButton
				}
			});
			Stack.Children.Add (new BoxView {
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});
			Stack.Children.Add (OrdersContent);
			/*
			Stack.Children.Add (new ContentViewCustom {
				IsDarkBlur = true,
				WidthRequest = App.ScreenWidth,
				HeightRequest = 40,
				VerticalOptions = LayoutOptions.EndAndExpand,
				Content = WashAvailableLabel,
			});*/

			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (OrdersContent, Constraint.Constant(0), Constraint.Constant(App.SpacingTopBar));
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync();

			ScheduledButton.Clicked += (sender, e) => {
				ScheduledButton.BackgroundColor = ColorResource.Pantone445c;
				ScheduledButton.TextColor = ColorResource.PantoneBlackC5;
				HistorialButton.BackgroundColor = ColorResource.PantoneBlackC5;
				HistorialButton.TextColor = ColorResource.Pantone445c;
				OrdersContent.Content = ScheduledView;
			};
			HistorialButton.Clicked += (sender, e) => {
				HistorialButton.BackgroundColor = ColorResource.Pantone445c;
				HistorialButton.TextColor = ColorResource.PantoneBlackC5;
				ScheduledButton.BackgroundColor = ColorResource.PantoneBlackC5;
				ScheduledButton.TextColor = ColorResource.Pantone445c;
				OrdersContent.Content = HistorialView;
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page OrdersPage");
		}
	}
}

