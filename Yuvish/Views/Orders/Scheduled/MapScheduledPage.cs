﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Yuvish
{
	public class MapScheduledPage : ContentPage
	{
		OrderRecModel Order;

		Button BackMenuButton;
		Image LogoImage;

		Label NotificationLabel;
		Button NotificationButton;

		ImageCustom YuvisherImage;
		Label YuvisherNameLabel;
		Label DateWorkLabel;
		Label TypeServiceLabel;
		Label CarLabel;
		Label PriceLabel;

		ContentViewCustom ProfileView;

		MapCustom Map;

		StackLayout BarStack;
		StackLayout NotificationStack;
		RelativeLayout Relative;

		TapGestureRecognizer TapGestureProfileOpen = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureProfileClose = new TapGestureRecognizer();

		public MapScheduledPage (OrderRecModel Order)
		{
			this.Order = Order;

			Title = "Yuvish";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void createElements ()
		{
			BackMenuButton = new Button {
				Image = "back_dark.png",
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			LogoImage = new Image {
				Source = ImageSource.FromResource("logo.png"),
				Scale = 1.2f,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			NotificationLabel = new Label {
				//Text = "Yuvish a llegado",
				FontFamily = Fonts.AvenirNext_M,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			NotificationButton = new Button {
				//Text = "Ver reporte",
				FontFamily = Fonts.AvenirNext_M,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
				TextColor = Color.White,
				BackgroundColor = ColorResource.VerdeAgua,
				BorderRadius = 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			YuvisherImage = new ImageCustom {
				//Source = ImageSource.FromResource("rodrigo.png"),
				IsCircle = true,
				WidthRequest = 65,
				HeightRequest = 65,
			};
			YuvisherNameLabel = new Label {
				Text = "Rodrigo Martínez",
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
			};
			DateWorkLabel = new Label {
				Text = Order.schedule,
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.PantoneBlackC5,
			};
			TypeServiceLabel = new Label {
				Text = Order.servicios[0].nombre,
				FontFamily = Fonts.AvenirNext_M,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.PantoneBlackC5,
			};
			CarLabel = new Label {
				Text = Order.Car,
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.PantoneBlackC5,
				HorizontalOptions = LayoutOptions.StartAndExpand,
			};
			PriceLabel = new Label {
				Text = "$" + Order.amount,
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.PantoneBlackC5,
				HorizontalOptions = LayoutOptions.End,
			};

			Map = new MapCustom {
				HasScrollEnabled = true,
				HasZoomEnabled = true,
				IsShowingUser = true,
				MapType = MapType.Street,
				WidthRequest = App.ScreenWidth,
				HeightRequest = App.ScreenHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			Map.MoveToRegion (MapSpan.FromCenterAndRadius (new Position (Order.lat, Order.lon), Distance.FromKilometers (0.18)));
			Map.Pins.Add(new Pin {
				Type = PinType.Place,
				Label = Order.address
			});
		}

		void createLayouts ()
		{						
			BarStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness (20, 10, 40, 10),
				BackgroundColor = Color.Transparent,
				WidthRequest = App.ScreenWidth - 60,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			NotificationStack = new StackLayout {
				Spacing = 0,
				BackgroundColor = Color.Transparent,
				WidthRequest = App.ScreenWidth,
			};

			ProfileView = new ContentViewCustom {
				WidthRequest = App.ScreenWidth,
				HeightRequest = 140,
				IsDarkBlur = true,
				GestureRecognizers = {
					TapGestureProfileOpen
				}
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuButton);
			BarStack.Children.Add (LogoImage);

			var auxStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 20,
				Children = {
					NotificationLabel
				}
			};
			if (Order.idStatus == 0) {
				NotificationLabel.Text = "Pendiente";
			}
			if (Order.idStatus == 1) {
				NotificationLabel.Text = "Yuvish en camino";
			}
			if (Order.idStatus == 2) {
				NotificationLabel.Text = "Yuvish ha terminado";
				NotificationButton.Text = "Ver reporte";
				auxStack.Children.Add (NotificationButton);
			}
			NotificationStack.Children.Add (new ContentView {
				BackgroundColor = Color.Transparent,
				Padding = new Thickness (20, 10),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Content = auxStack,
			});


			ProfileView.Content = new StackLayout {	
				Spacing = 0,
				WidthRequest = App.ScreenWidth,
				HeightRequest = ProfileView.HeightRequest + 1,
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Spacing = 10,
						Padding = new Thickness (20, 10),
						WidthRequest = App.ScreenWidth,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							new StackLayout {
								Spacing = 5,
								Padding = new Thickness (0, 10),
								HorizontalOptions = LayoutOptions.StartAndExpand,
								VerticalOptions = LayoutOptions.CenterAndExpand,
								Children = {
									//YuvisherNameLabel,
									new StackLayout {
										Orientation = StackOrientation.Horizontal,
										Spacing = 5,
										Children = {
											DateWorkLabel
										}
									}
								}
							},
//							new Button {
//								Image = "ic_phone.png",
//								HorizontalOptions = LayoutOptions.End,
//							},
//							new Button {
//								Image = "ic_chat.png",
//								HorizontalOptions = LayoutOptions.End,
//							}
						}
					},
					new BoxView {
						WidthRequest = App.ScreenWidth,
						HeightRequest = 0.5f,
						BackgroundColor = Color.White,
					},
					new StackLayout {
						Spacing = 10,
						HeightRequest = 100,
						Padding = new Thickness (20, 10),
						Children = {
							TypeServiceLabel,
							new StackLayout {
								Orientation = StackOrientation.Horizontal,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								Children = {
									CarLabel,
									PriceLabel
								}
							}
						}
					}
				}
			};

			Relative.Children.Add (Map,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (
				new ContentView {
					Opacity = 0.6,
					BackgroundColor = Color.White,
					WidthRequest = App.ScreenWidth,
					HeightRequest = 60,
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (BarStack,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (
				new ContentView {
					Opacity = 0.6,
					BackgroundColor = Color.White,
					WidthRequest = App.ScreenWidth,
					HeightRequest = 60,
				},
				Constraint.Constant(0),
				Constraint.Constant(60)
			);
			Relative.Children.Add (NotificationStack,
				Constraint.Constant(0),
				Constraint.Constant(60)
			);
			Relative.Children.Add (ProfileView,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height - 60; })
			);
			/*
			Relative.Children.Add (YuvisherImage,
				Constraint.Constant(20),
				Constraint.RelativeToParent((parent) => { return parent.Height - 70 - YuvisherImage.HeightRequest / 4; })
			);*/
		}

		void setListeners ()
		{
			BackMenuButton.Clicked += (sender, e) => {
				Navigation.PopAsync();
			};

			TapGestureProfileOpen.Tapped += (sender, e) => {
				ProfileView.TranslateTo (0, -75);

				ProfileView.GestureRecognizers.Clear ();
				ProfileView.GestureRecognizers.Add (TapGestureProfileClose);
			};

			TapGestureProfileClose.Tapped += (sender, e) => {
				ProfileView.TranslateTo (0, 0);

				ProfileView.GestureRecognizers.Clear ();
				ProfileView.GestureRecognizers.Add (TapGestureProfileOpen);
			};
				
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page MapScheduledPage");
		}
	}
}

