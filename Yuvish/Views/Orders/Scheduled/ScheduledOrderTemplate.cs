﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class ScheduledOrderTemplate : ViewCell
	{
		ImageCustom YuvisherImage;
		Label YuvisherNameLabel;
		Label DateLabel;
		Label TimeLabel;
		Label EndTimeLabel;
		Label TypeServiceLabel;
		Label AddressLabel;
		Label StatusLabel;
		Label PriceLabel;
		Label CarLabel;

		StackLayout Stack;

		public ScheduledOrderTemplate ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();
			setBindings ();

			View = Stack;
		}

		void createElements ()
		{
			YuvisherImage = new ImageCustom {
				//Source = ImageSource.FromResource("rodrigo.png"),
				IsCircle = true,
				WidthRequest = 40,
				HeightRequest = 40,
				VerticalOptions = LayoutOptions.StartAndExpand,
			};
			YuvisherNameLabel = new Label {
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone445c,
			};
			DateLabel = new Label {
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				TextColor = Color.Gray,
			};
			TimeLabel = new Label {
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				TextColor = Color.Gray,
			};
			TypeServiceLabel = new Label {
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone445c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			PriceLabel = new Label {
				FontFamily = Fonts.AvenirNext_M,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone445c,
				HorizontalTextAlignment = TextAlignment.End,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			StatusLabel = new Label {
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone3252c,
				HorizontalTextAlignment = TextAlignment.End,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			CarLabel = new Label {
				FontFamily = Fonts.AvenirNext_M,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			AddressLabel = new Label {
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				TextColor = Color.Gray,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(15),
				Spacing = 15,
				WidthRequest = App.ScreenWidth,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void setLayouts ()
		{
			//Stack.Children.Add (YuvisherImage);
			Stack.Children.Add (new StackLayout {
				Spacing = 5,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							new StackLayout {
								Children = {
									//YuvisherNameLabel,
									CarLabel,
									new StackLayout {
										Spacing = 5,
										Orientation = StackOrientation.Horizontal,
										Children = {
											DateLabel,
											TimeLabel
										}
									},
								}
							},
							new StackLayout {
								HorizontalOptions = LayoutOptions.EndAndExpand,
								Children = {
									PriceLabel,
									StatusLabel
								}
							}
						}
					},
					TypeServiceLabel,
					//CarLabel,
					AddressLabel
				}
			});
		}

		void setBindings ()
		{
			DateLabel.SetBinding (Label.TextProperty, "schedule");
			PriceLabel.SetBinding (Label.TextProperty, "amount", stringFormat : "${0}");
			CarLabel.SetBinding (Label.TextProperty, "Car");
			StatusLabel.SetBinding (Label.TextProperty, "Status");
			AddressLabel.SetBinding (Label.TextProperty, "address");
		}
	}
}

