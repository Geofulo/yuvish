﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class ScheduledOrdersView : ContentView
	{
		List<OrderRecModel> Orders;

		ListView SchenduledListView;

		Label NoOrdersLabel;

		public ScheduledOrdersView ()
		{
			getScheduledOrders ();
			createElements ();
			setListeners ();

			WidthRequest = App.ScreenWidth;
			VerticalOptions = LayoutOptions.FillAndExpand;
			if (Orders.Count > 0) {
				Content = SchenduledListView;
			} else {
				Content = NoOrdersLabel;
			}
		}

		void getScheduledOrders ()
		{
			//Orders = DataAuxiliar.GetScheduledOrders ();

			Orders = App.WSRest.GetMyOrders ();
		}

		void createElements ()
		{
			SchenduledListView = new ListView {
				ItemsSource = Orders,
				ItemTemplate = new DataTemplate(typeof(ScheduledOrderTemplate)),
				IsPullToRefreshEnabled = true,
				HasUnevenRows = true,
				HeightRequest = App.ScreenHeight - 80,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				BackgroundColor = ColorResource.PantoneBlackC5,
				SeparatorColor = ColorResource.Pantone445c,
			};

			NoOrdersLabel = new Label {
				Text = "No tienes ningún servicio agendado",
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HorizontalTextAlignment = TextAlignment.Center,
				VerticalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void setListeners ()
		{
			SchenduledListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;

				var item = e.SelectedItem as OrderRecModel;

				SchenduledListView.SelectedItem = null;

				Navigation.PushAsync (new MapScheduledPage(item));

			};

			SchenduledListView.Refreshing += (sender, e) => {
				getScheduledOrders();
				SchenduledListView.ItemsSource = Orders;

				SchenduledListView.IsRefreshing = false;
			};
		}

	}
}

