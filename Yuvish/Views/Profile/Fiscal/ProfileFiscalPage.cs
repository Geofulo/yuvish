﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class ProfileFiscalPage : ContentPage
	{
		FiscalModel Fiscal;

		Button BackMenuImage;
		Button SaveImage;

		EntryCustom NombreEntry;
		EntryCustom RFCEntry;
		EntryCustom CalleEntry;
		EntryCustom NumeroExternoEntry;
		EntryCustom NumeroInternoEntry;
		EntryCustom ColoniaEntry;
		EntryCustom MunicipioEntry;
		EntryCustom EstadoEntry;
		EntryCustom CodigoPostalEntry;

		StackLayout BarStack;
		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		public ProfileFiscalPage (FiscalModel Fiscal)
		{
			this.Fiscal = Fiscal;

			Title = "Nuevos Datos Fiscales";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;		
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			SaveImage = new Button {
				Image = "ic_save.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			if (Fiscal != null) {
				NombreEntry = getEntryNoEdit (Fiscal.Nombre);
				RFCEntry = getEntryNoEdit (Fiscal.RFC);
				CalleEntry = getEntryNoEdit (Fiscal.Calle);
				NumeroExternoEntry = getEntryNoEdit (Fiscal.NumeroExterior);
				NumeroInternoEntry = getEntryNoEdit (Fiscal.NumeroInterior);
				ColoniaEntry = getEntryNoEdit (Fiscal.Colonia);
				MunicipioEntry = getEntryNoEdit (Fiscal.Municipio);
				EstadoEntry = getEntryNoEdit (Fiscal.Estado);
				CodigoPostalEntry = getEntryNoEdit (Fiscal.CodigoPostal);
			} else {				
				NombreEntry = getEntry ("Nombre o Razón Social");
				RFCEntry = getEntry ("RFC");
				CalleEntry = getEntry ("Calle");
				NumeroExternoEntry = getEntry ("No. Exterior");
				NumeroInternoEntry = getEntry ("No. Interior");
				ColoniaEntry = getEntry ("Colonia");
				MunicipioEntry = getEntry ("Municipio");
				EstadoEntry = getEntry ("Estado");
				CodigoPostalEntry = getEntry ("Código Postal");
			}
		}

		EntryCustom getEntry (string placeholder)
		{
			return new EntryCustom {		
				Placeholder = placeholder,
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		EntryCustom getEntryNoEdit (string text)
		{
			return new EntryCustom {		
				Text = text,
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 20,
				Padding = new Thickness (15, 20, 15, 5),
				WidthRequest = App.ScreenWidth - 30,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - App.SpacingTopBar - 20,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Datos Fiscales",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			BarStack.Children.Add (SaveImage);


			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Children = {
					getLabelForm ("Nombre o Razón Social"),
					NombreEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Children = {
					getLabelForm ("RFC"),
					RFCEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Children = {
					getLabelForm ("Calle"),
					CalleEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabelForm ("No. Exterior"),
							NumeroExternoEntry
						}
					},
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabelForm ("No. Interior"),
							NumeroInternoEntry
						}
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabelForm ("Colonia"),
							ColoniaEntry
						}
					},
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabelForm ("Municipio/Delegación"),
							MunicipioEntry
						}
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabelForm ("Estado"),
							EstadoEntry
						}
					},
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabelForm ("Código Postal"),
							CodigoPostalEntry
						}
					}
				}
			});

			Scroll.Content = Stack;


			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Scroll,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		Label getLabelForm (string text)
		{
			return new Label {
				Text = text,
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				TextColor = Color.Gray,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync ();

			SaveImage.Clicked += (sender, e) => {
				var nombre = NombreEntry.Text;
				var rfc = RFCEntry.Text;
				var calle = CalleEntry.Text;
				var numeroExt = NumeroExternoEntry.Text;
				var numeroInt = NumeroInternoEntry.Text;
				var colonia = ColoniaEntry.Text;
				var municipio = MunicipioEntry.Text;
				var estado = EstadoEntry.Text;
				var codigoPostal = CodigoPostalEntry.Text;

				if (!string.IsNullOrEmpty (nombre) && !string.IsNullOrEmpty (rfc) && !string.IsNullOrEmpty (calle) && !string.IsNullOrEmpty (numeroExt) && 
					!string.IsNullOrEmpty (numeroInt) && !string.IsNullOrEmpty (colonia) && !string.IsNullOrEmpty (municipio) && 
					!string.IsNullOrEmpty (estado) && !string.IsNullOrEmpty (codigoPostal)) {

					if (Fiscal != null) {

						Fiscal.Nombre = nombre;
						Fiscal.RFC = rfc;
						Fiscal.Calle = calle;
						Fiscal.NumeroExterior = numeroExt;
						Fiscal.NumeroInterior = numeroInt;
						Fiscal.Colonia = colonia;
						Fiscal.Municipio = municipio;
						Fiscal.Estado = estado;
						Fiscal.CodigoPostal = codigoPostal;

						if (App.LocalDataMan.UpdateFiscal (Fiscal)) {
							Navigation.PopAsync ();
							DisplayAlert ("Correcto", "La información se guardó correctamente", "OK");
						} else {
							DisplayAlert ("", "Ocurrió un error al intentar guardar la información", "OK");
						}
					} else {

						var fiscal = new FiscalModel {
							Nombre = nombre,
							RFC = rfc,
							Calle = calle,
							NumeroExterior = numeroExt,
							NumeroInterior = numeroInt,
							Colonia = colonia,
							Municipio = municipio,
							Estado = estado,
							CodigoPostal = codigoPostal
						};

						if (App.LocalDataMan.CreateFiscal (fiscal)) {
							Navigation.PopAsync ();
							DisplayAlert ("Correcto", "La información se guardó correctamente", "OK");
						} else {
							DisplayAlert ("", "Ocurrió un error al intentar guardar la información", "OK");
						}
					}

				} else {
					DisplayAlert ("", "Se debe ingresar toda la información fiscal", "OK");				
				}
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page ProfileFiscalPage");
		}
	}
}

