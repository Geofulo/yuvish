﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class ProfileGeneralPage : ContentPage
	{
		Button BackMenuImage;
		Button SaveImage;

		ImageCustom ProfileImage;
		Entry NameEntry;
		Entry LastNameEntry;
		Entry EmailEntry;
		Entry PhoneEntry;
		Entry PasswordEntry;

		StackLayout BarStack;
		StackLayout Stack;
		RelativeLayout Relative;

		public ProfileGeneralPage ()
		{
			Title = "General";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			SaveImage = new Button {
				Image = "ic_save.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			ProfileImage = new ImageCustom {
				//Source = ImageSource.FromResource ("janet.png"),
				//Source = ImageSource.FromFile (App.ProfilePhoto),
				HeightRequest = 70,
				HorizontalOptions = LayoutOptions.Center,
			};
			NameEntry = new EntryCustom {
				Text = App.CurrentUser.firstName,
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				IsNoBorder = true,
				BorderRadius = 0,
				HorizontalTextAlignment = TextAlignment.End,
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			LastNameEntry = new EntryCustom {
				Text = App.CurrentUser.lastName,
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				IsNoBorder = true,
				BorderRadius = 0,
				TextColor = ColorResource.Pantone446c,
				HorizontalTextAlignment = TextAlignment.Start,
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			EmailEntry = new EntryCustom {				
				Text = App.CurrentUser.email,
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				IsNoBorder = true,
				BorderRadius = 0,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			PhoneEntry = new EntryCustom {				
				Text = App.CurrentUser.mobile,
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				IsNoBorder = true,
				BorderRadius = 0,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			PasswordEntry = new EntryCustom {				
				Text = "",
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				IsNoBorder = true,
				IsPassword = true,
				BorderRadius = 0,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 20,
				Padding = new Thickness (15, 20, 15, 5),
				WidthRequest = App.ScreenWidth - 30,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Datos Generales",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			BarStack.Children.Add (SaveImage);


			//Stack.Children.Add (ProfileImage);
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 20,
				Children = {
					new Image {
						Source = "ic_person.png",
					},
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Spacing = 5,
						Children = {
							NameEntry,
							LastNameEntry
						}
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Children = {
					new Image {
						Source = "ic_at.png",
					},
					EmailEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Children = {
					new Image {
						Source = "ic_phone_perfil.png",
					},
					PhoneEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Children = {
					new Image {
						Source = "ic_secure.png",
					},
					PasswordEntry
				}
			});
			Stack.Children.Add (new StackLayout ()
			{
				Padding = new Thickness (0, 40, 0, 0),
				Children = {
					new BoxView {				
						BackgroundColor = ColorResource.Pantone445c,
						HeightRequest = 1,
						HorizontalOptions = LayoutOptions.FillAndExpand,
					}
				}
			});


			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Stack,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync();

			SaveImage.Clicked += async (sender, e) => {
				var name = NameEntry.Text;
				var lastName = LastNameEntry.Text;
				var email = EmailEntry.Text;
				var phone = PhoneEntry.Text;
				var password = PasswordEntry.Text;

				if (!string.IsNullOrEmpty(password))
				{
					if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(phone))
					{
						var userEdited = new UserModel
						{
							firstName = name,
							lastName = lastName,
							email = email,
							mobile = phone
						};

						var canUpdate = await DisplayAlert("Actualizar datos", "¿Está seguro de querer actualizar tus datos?", "Actualizar", "Cancelar");

						if (canUpdate)
						{
							if (App.WSRest.EditMyInfo(userEdited, password))
							{
								App.CurrentUser = App.WSRest.GetMyInfo();

								Navigation.PopAsync();
								DisplayAlert("Correcto", "La información se actualizó correctamente", "OK");
							}
							else {
								DisplayAlert("", "Ocurrió un error al intentar actualizar tu información", "OK");
							}
						}
					}
					else {
						DisplayAlert("", "Algunos datos están vacíos", "OK");
					}
				}
				else 
					DisplayAlert("", "Debes ingresar tu contraseña para actualizar tus datos", "OK");
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page ProfileGeneralPage");
		}
	}
}

