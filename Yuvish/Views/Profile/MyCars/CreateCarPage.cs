﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class CreateCarPage : ContentPage
	{
		List<BrandModel> Brands;
		List<ModeloModel> Modelos = new List<ModeloModel> ();
		Dictionary<string, BrandModel> BrandsDicc = new Dictionary<string, BrandModel>();
		Dictionary<string, ModeloModel> ModelosDicc = new Dictionary<string, ModeloModel>();

		Button BackMenuImage;
		Button SaveImage;

		EntryCustom NombreEntry;
		PickerCustom MarcaPicker;
		PickerCustom ModeloPicker;
		EntryCustom PlacasEntry;
		EntryCustom ColorEntry;
		EditorCustom DetallesEditor;

		StackLayout BarStack;
		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		public CreateCarPage ()
		{
			Title = "Nuevo Auto";

			getBrands ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;			
		}

		void getBrands ()
		{
			Brands = App.WSRest.GetMarcasModelos ();

			foreach(var brand in Brands) {
				BrandsDicc.Add (brand.brand, brand);
			}
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			SaveImage = new Button {
				Image = "ic_save.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			NombreEntry = getEntry ();
			NombreEntry.Placeholder = "Nombre";

			MarcaPicker = new PickerCustom {
				Title = "Marca",
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			foreach(var brandD in BrandsDicc) {
				MarcaPicker.Items.Add (brandD.Key);
			}
			ModeloPicker = new PickerCustom {
				Title = "Modelo",
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			ColorEntry = getEntry ();
			ColorEntry.Placeholder = "Color";
			PlacasEntry = getEntry ();
			PlacasEntry.Placeholder = "Placas";

			DetallesEditor = new EditorCustom {
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				HeightRequest = 120,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		EntryCustom getEntry ()
		{
			return new EntryCustom {
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 20,
				Padding = new Thickness (15, 10),
				WidthRequest = App.ScreenWidth - 30,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - App.SpacingTopBar - 20,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Nuevo Auto",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			BarStack.Children.Add (SaveImage);

			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Children = {
					new Label {
						Text = "Nombre",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						TextColor = Color.Gray,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
					NombreEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							new Label {
								Text = "Marca",
								FontFamily = Fonts.AvenirNext,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = Color.Gray,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							MarcaPicker
						}
					},
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							new Label {
								Text = "Modelo",
								FontFamily = Fonts.AvenirNext,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = Color.Gray,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							ModeloPicker
						}
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							new Label {
								Text = "Color",
								FontFamily = Fonts.AvenirNext,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = Color.Gray,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							ColorEntry
						}
					},
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							new Label {
								Text = "Placas",
								FontFamily = Fonts.AvenirNext,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = Color.Gray,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							PlacasEntry
						}
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Children = {
					new Label {
						Text = "Información adicional",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						TextColor = Color.Gray,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
					DetallesEditor
				}
			});

			Scroll.Content = Stack;


			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Scroll,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync ();

			SaveImage.Clicked += (sender, e) => {
				var brand = Brands [MarcaPicker.SelectedIndex].brand;
				var modelo = Modelos [ModeloPicker.SelectedIndex].name;
				var brandId = Brands [MarcaPicker.SelectedIndex].idBrand;
				var modeloId = Modelos [ModeloPicker.SelectedIndex].idModel;
				var size = Modelos [ModeloPicker.SelectedIndex].size;
				var nombre = NombreEntry.Text;
				var color = ColorEntry.Text;
				var placas = PlacasEntry.Text;
				var detalles = DetallesEditor.Text;
				if (!string.IsNullOrEmpty(brand) && !string.IsNullOrEmpty(modelo) && !string.IsNullOrEmpty(nombre) && 
					!string.IsNullOrEmpty(color) && !string.IsNullOrEmpty(placas)) {
					var carCreated = new CarLocalModel {
						Nombre = nombre,
						Marca = brand,
						Modelo = modelo,
						Placas = placas,
						MarcaId = brandId,
						ModeloId = modeloId,
						Color = color,
						Detalles = detalles,
						Size = size,
					};
					if (App.LocalDataMan.CreateCar (carCreated)) {
						Navigation.PopAsync ();
					} else {
						DisplayAlert ("", "Ocurrió un error al intentar crear el auto", "OK");
					}
				} else {
					DisplayAlert ("", "Se debe ingresar toda la información del auto", "OK");
				}
			};

			MarcaPicker.SelectedIndexChanged += (sender, e) => {

				Modelos.Clear();
				ModelosDicc.Clear();
				ModeloPicker.Items.Clear();

				var brandSelected = Brands [MarcaPicker.SelectedIndex];

				foreach (var mod in brandSelected.modelo) {
					Modelos.Add(mod);
				}

				foreach(var mod in Modelos) {
					ModelosDicc.Add (mod.name, mod);
				}

				foreach(var modeloD in ModelosDicc) {
					ModeloPicker.Items.Add (modeloD.Key);
				}
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page CreateCarPage");
		}
	}
}

