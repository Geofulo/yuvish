﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class DetailCarPage : ContentPage
	{
		CarLocalModel Car;
		List<BrandModel> Brands;
		List<ModeloModel> Modelos = new List<ModeloModel> ();
		Dictionary<string, BrandModel> BrandsDicc = new Dictionary<string, BrandModel>();
		Dictionary<string, ModeloModel> ModelosDicc = new Dictionary<string, ModeloModel>();
		int MarcaIndexSelected;
		int ModeloIndexSelected;

		Button BackMenuImage;
		Button SaveImage;
		Label TitleLabel;

		EntryCustom NombreEntry;
		PickerCustom MarcaPicker;
		PickerCustom ModeloPicker;
		EntryCustom PlacasEntry;
		EntryCustom ColorEntry;
		EditorCustom DetallesEditor;

		Image DeleteImage;

		StackLayout BarStack;
		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		TapGestureRecognizer TapGestureDeleteMenu = new TapGestureRecognizer();

		public DetailCarPage (CarLocalModel Car)
		{
			this.Car = Car;

			Title = "Detalle Auto";

			getBrands ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;			

			System.Diagnostics.Debug.WriteLine ("Marca ID: " + Car.MarcaId);
		}

		void getBrands ()
		{
			Brands = App.WSRest.GetMarcasModelos ();

			int i = 0;
			foreach(var brand in Brands) {
				BrandsDicc.Add (brand.brand, brand);
				if (brand.idBrand.Equals (Car.MarcaId))
					MarcaIndexSelected = i;
				i++;
			}
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			SaveImage = new Button {
				Image = "ic_save.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			TitleLabel = new Label
			{
				Text = "Editar Auto",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};


			NombreEntry = getEntry ();
			NombreEntry.Text = Car.Nombre;

			MarcaPicker = new PickerCustom {
				Title = "Marca",
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			foreach(var brandD in BrandsDicc) {
				MarcaPicker.Items.Add (brandD.Key);
			}
			MarcaPicker.SelectedIndex = MarcaIndexSelected;

			ModeloPicker = new PickerCustom {
				Title = "Modelo",
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			setModeloPicker ();

			ColorEntry = getEntry ();
			ColorEntry.Text = Car.Color;

			PlacasEntry = getEntry ();
			PlacasEntry.Text = Car.Placas;

			DetallesEditor = new EditorCustom {
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				HeightRequest = 120,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			DetallesEditor.Text = Car.Detalles;

			DeleteImage = new Image {
				Source = "ic_trash.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.End,
				GestureRecognizers = {
					TapGestureDeleteMenu
				}
			};
		}

		EntryCustom getEntry ()
		{
			return new EntryCustom {
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setModeloPicker ()
		{
			var brandSelected = Brands [MarcaPicker.SelectedIndex];

			int i = 0;
			foreach (var mod in brandSelected.modelo) {
				Modelos.Add(mod);
				if (mod.idModel.Equals (Car.ModeloId))
					ModeloIndexSelected = i;	
				i++;
			}

			foreach(var mod in Modelos) {
				ModelosDicc.Add (mod.name, mod);
			}

			foreach(var modeloD in ModelosDicc) {
				ModeloPicker.Items.Add (modeloD.Key);
			}
			ModeloPicker.SelectedIndex = ModeloIndexSelected;
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 20,
				Padding = new Thickness (15, 10),
				WidthRequest = App.ScreenWidth - 30,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - App.SpacingTopBar - 20,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (TitleLabel);
			BarStack.Children.Add (SaveImage);

			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Children = {
					getLabel ("Nombre"),
					NombreEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabel ("Marca"),
							MarcaPicker
						}
					},
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabel ("Modelo"),
							ModeloPicker
						}
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabel ("Color"),
							ColorEntry
						}
					},
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabel ("Placas"),
							PlacasEntry
						}
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Children = {
					getLabel ("Información adicional"),
					DetallesEditor
				}
			});
			Stack.Children.Add (DeleteImage);

			Scroll.Content = Stack;


			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Scroll,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		Label getLabel (string text)
		{
			return new Label {
				Text = text,
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = Color.Gray,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync ();

			SaveImage.Clicked += async (sender, e) => {
				var nombre = NombreEntry.Text;
				var brandId = Brands [MarcaPicker.SelectedIndex].idBrand;
				var modeloId = Modelos [ModeloPicker.SelectedIndex].idModel;
				var brand = Brands [MarcaPicker.SelectedIndex].brand;
				var modelo = Modelos [ModeloPicker.SelectedIndex].name;
				var size = Modelos [ModeloPicker.SelectedIndex].size;
				var color = ColorEntry.Text;
				var placas = PlacasEntry.Text;
				var detalles = DetallesEditor.Text;

				if (!string.IsNullOrEmpty(brand) && !string.IsNullOrEmpty(modelo) && !string.IsNullOrEmpty(color) && !string.IsNullOrEmpty(placas)) {
					Car.Nombre = nombre;
					Car.Marca = brand;
					Car.Modelo = modelo;
					Car.MarcaId = brandId;
					Car.ModeloId = modeloId;
					Car.Color = color;
					Car.Placas = placas;
					Car.Detalles = detalles;
					Car.Size = size;

					var canUpdate = await DisplayAlert ("Actualizar auto", "¿Está seguro de querer actualizar éste auto?", "Actualizar", "Cancelar");

					if (canUpdate)
					{
						if (App.LocalDataMan.UpdateCar (Car)) {
							Navigation.PopAsync ();
						} else {
							DisplayAlert ("", "Ocurrió un error al intentar actualizar el auto", "OK");
						}
					}
				} else {
					DisplayAlert ("", "Algunos datos están vacíos", "OK");
				}
			};

			TapGestureDeleteMenu.Tapped += async (sender, e) => {
				var canDelete = await DisplayAlert ("Borrar", "¿Estás seguro de querer eliminar éste auto?", "Eliminar", "Cancelar");
				if (canDelete) {
					if (App.LocalDataMan.DeleteCar (Car)) {
						Navigation.PopAsync ();
					} else {
						DisplayAlert ("", "Ocurrió un error al intentar eliminar el auto", "OK");
					}
				}
			};

			MarcaPicker.SelectedIndexChanged += (sender, e) => {

				Modelos.Clear();
				ModelosDicc.Clear();
				ModeloPicker.Items.Clear();

				var brandSelected = Brands [MarcaPicker.SelectedIndex];

				foreach (var mod in brandSelected.modelo) {
					Modelos.Add(mod);
				}

				foreach(var mod in Modelos) {
					ModelosDicc.Add (mod.name, mod);
				}

				foreach(var modeloD in ModelosDicc) {
					ModeloPicker.Items.Add (modeloD.Key);
				}
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page DetailCarPage");
		}
	}
}

