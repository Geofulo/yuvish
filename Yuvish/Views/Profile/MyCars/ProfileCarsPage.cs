﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class ProfileCarsPage : ContentPage
	{
		List<CarLocalModel> Cars;
		bool IsFromService;

		Button BackMenuImage;
		Button AddImage;

		List<ContentView> CarsViews = new List<ContentView>();
		Label NoCarsLabel;

		StackLayout BarStack;
		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		List<TapGestureRecognizer> CarsTapGestures = new List<TapGestureRecognizer>();

		public ProfileCarsPage (bool IsFromService)
		{
			this.IsFromService = IsFromService;

			Title = "Mis Autos";

			getCars ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void getCars ()
		{
			Cars = App.LocalDataMan.GetCars ();
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			AddImage = new Button {
				Image = "ic_plus.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			if (IsFromService)
				AddImage.IsVisible = false;

			createCarsViews ();
		}

		void createCarsViews ()
		{
			CarsViews.Clear ();
			CarsTapGestures.Clear ();

			if (Cars != null) {
				if (Cars.Count > 0) {					
					foreach (var car in Cars) {
						var tapGesture = new TapGestureRecognizer ();

						var nombreLabel = new Label {
							FontFamily = Fonts.AvenirNext,
							FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
							TextColor = ColorResource.Pantone446c,
						};
						if (string.IsNullOrEmpty (car.Nombre)) {
							nombreLabel.Text = "Auto sin nombre";
							nombreLabel.FontAttributes = FontAttributes.Italic;
						} else
							nombreLabel.Text = car.Nombre;
						
						CarsViews.Add (new ContentView {
							Padding = new Thickness (20, 2),
							Content = new StackLayout {
								Orientation = StackOrientation.Horizontal,
								Children = {
									new StackLayout {
										Spacing = 10,
										HorizontalOptions = LayoutOptions.FillAndExpand,
										Children = {
											nombreLabel,
											new Label {
												Text = car.Marca + " " + car.Modelo,
												FontFamily = Fonts.AvenirNext,
												FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
												TextColor = ColorResource.Pantone446c,
											},
											new Label {
												Text = "Placas: " + car.Placas,
												FontFamily = Fonts.AvenirNext,
												FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
												TextColor = ColorResource.Pantone446c,
											},
										}
									},
									new Image {
										Source = "ic_list_view.png",
										VerticalOptions = LayoutOptions.CenterAndExpand,
										HorizontalOptions = LayoutOptions.End,
									}
								}
							},
							GestureRecognizers = {
								tapGesture
							}
						});

						CarsTapGestures.Add (tapGesture);
					}
				} else {
					NoCarsLabel = new Label {
						Text = "No tienes ningún auto. \nAgrega uno ahora.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalTextAlignment = TextAlignment.Center,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					};
				}
			}
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 20,
				Padding = new Thickness(0, 10),
				WidthRequest = App.ScreenWidth,
				//HeightRequest = App.ScreenHeight - 160,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - App.SpacingTopBar - 10,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Clear ();
			Stack.Children.Clear ();
			Relative.Children.Clear ();


			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Mis Autos",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			BarStack.Children.Add (AddImage);


			if (CarsViews.Count > 0) {
				foreach (var view in CarsViews) {
					Stack.Children.Add (view);
					Stack.Children.Add (new BoxView {				
						BackgroundColor = ColorResource.Pantone445c,
						HeightRequest = 0.5,
						HorizontalOptions = LayoutOptions.FillAndExpand,
					});
				}
			} else {
				Stack.Children.Add (NoCarsLabel);
			}

			Scroll.Content = Stack;

			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Scroll,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		void setListeners ()
		{			
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync ();

			AddImage.Clicked += (sender, e) => Navigation.PushAsync (new CreateCarPage ());

			setCarsViewsListeners ();
		}

		void setCarsViewsListeners ()
		{
			for (int i = 0; i < CarsTapGestures.Count; i++) {				
				var tap = CarsTapGestures [i];
				var car = Cars [i];
				tap.Tapped += (sender, e) => {
					var page = new DetailCarPage (car);
					if (IsFromService) {
						var carSelected = new CarModel {
							idBrand = int.Parse (car.MarcaId),
							idModel = int.Parse (car.ModeloId),
							idColor = car.Color,
							licensePlate = car.Placas,
							details = car.Detalles,
						};
						App.Order.clientCar = carSelected;

						this.Navigation.PopAsync ();
					} else {
						Navigation.PushAsync (page);						
					}
				};			
			}
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page ProfileCarsPage");

			getCars ();
			createCarsViews ();
			setLayouts ();
			setCarsViewsListeners ();
		}
	}
}

