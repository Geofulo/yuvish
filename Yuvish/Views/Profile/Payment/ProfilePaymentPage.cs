﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class ProfilePaymentPage : ContentPage
	{
		List<CreditCardModel> CreditCards;

		Button BackMenuImage;
		Button AddImage;

		List<ContentViewCustom> CardsViews = new List<ContentViewCustom>();
		Label NoCardsLabel;

		StackLayout BarStack;
		StackLayout Stack;
		RelativeLayout Relative;

		List<TapGestureRecognizer> CardsTapGestures = new List<TapGestureRecognizer>();

		public ProfilePaymentPage ()
		{
			Title = "Pago";

			getCreditCards ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void getCreditCards ()
		{
			CreditCards = App.LocalDataMan.GetCreditCards ();
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			AddImage = new Button {
				Image = "ic_plus.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			createCardViews ();

		}

		void createCardViews ()
		{
			CardsViews.Clear ();
			CardsTapGestures.Clear ();

			if (CreditCards != null) {
				if (CreditCards.Count > 0) {	
					foreach (var card in CreditCards) {
						var tapGesture = new TapGestureRecognizer ();

						CardsViews.Add (new ContentViewCustom {
							//Radius = 20,
							Padding = new Thickness(10),
							BackgroundColor = Color.White,
							ColorBorder = Color.Gray,
							WidthRequest = App.ScreenWidth - 40,
							HorizontalOptions = LayoutOptions.CenterAndExpand,
							Content = new StackLayout {
								Padding = new Thickness (10, 0),
								Spacing = 10,
								Children = {									
									new Label {
										Text = card.BankName,
										FontFamily = Fonts.AvenirNext,
										FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
										TextColor = ColorResource.Pantone445c,
										VerticalOptions = LayoutOptions.CenterAndExpand,
									},
									new StackLayout {
										Orientation = StackOrientation.Horizontal,
										Spacing = 20,
										Children = {
											new Label {
												Text = "**** **** **** " + card.Number.Substring(card.Number.Length - 4, 4),
												FontFamily = Fonts.AvenirNext,
												FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
												TextColor = ColorResource.Pantone445c,
												VerticalOptions = LayoutOptions.CenterAndExpand,
											},
											new Image {
												Source = card.CreditCardImage,
												HorizontalOptions = LayoutOptions.EndAndExpand,
												VerticalOptions = LayoutOptions.CenterAndExpand,
											},
										}
									},

									/*
									new StackLayout {
										Spacing = 20,
										Children = {
											new StackLayout {
												Orientation = StackOrientation.Horizontal,
												Spacing = 20,
												Children = {
													new Label {
														Text = "Banco",
														FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
														TextColor = Color.Gray,
														VerticalOptions = LayoutOptions.CenterAndExpand,
													},
													new Label {
														Text = card.BankName,
														FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
														VerticalOptions = LayoutOptions.CenterAndExpand,
													},
												}
											},
											new StackLayout {
												Orientation = StackOrientation.Horizontal,
												Spacing = 20,
												Children = {
													new Label {
														Text = "Tarjeta",
														FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
														TextColor = Color.Gray,
														VerticalOptions = LayoutOptions.CenterAndExpand,
													},
													new Label {
														Text = "XXXX-XXXX-XXXX-" + card.Number.Substring(card.Number.Length - 4, 4),
														FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
														VerticalOptions = LayoutOptions.CenterAndExpand,
													},
												}
											},
										}
									},
									new Image {
										Source = "ic_list_view.png",
										VerticalOptions = LayoutOptions.CenterAndExpand,
										HorizontalOptions = LayoutOptions.EndAndExpand,
									}*/
								}
							},
							GestureRecognizers = {
								tapGesture
							}
						});

						CardsTapGestures.Add (tapGesture);
					}
				} else {
					NoCardsLabel = new Label {
						Text = "No tienes ninguna tarjeta. \nAgrega una ahora.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalTextAlignment = TextAlignment.Center,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					};
				}
			}				
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 15,
				Padding = new Thickness(0, 10),
				//WidthRequest = App.ScreenWidth,
				WidthRequest = App.ScreenWidth,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Clear ();
			Stack.Children.Clear ();
			Relative.Children.Clear ();


			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Mis Tarjetas",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			BarStack.Children.Add (AddImage);

			if (CardsViews.Count > 0) {
				foreach (var view in CardsViews) {
					Stack.Children.Add (view);
					Stack.Children.Add (new BoxView {				
						BackgroundColor = ColorResource.Pantone445c,
						HeightRequest = 1,
						HorizontalOptions = LayoutOptions.FillAndExpand,
					});
				}
			} else {
				Stack.Children.Add (NoCardsLabel);
			}

			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Stack,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync ();
			AddImage.Clicked += (sender, e) => Navigation.PushAsync (new CreateTDCPage(false));

			setCardsViewsListeners ();
		}

		void setCardsViewsListeners ()
		{
			for (int i = 0; i < CardsTapGestures.Count; i++) {
				var cardTap = CardsTapGestures [i];
				var card = CreditCards [i];
				var detailPage = new DetailTDCPage (card);

				cardTap.Tapped += (sender, e) => Navigation.PushAsync (detailPage);
			}
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page ProfilePaymentPage");

			getCreditCards ();
			createCardViews ();
			setLayouts ();
			setCardsViewsListeners ();
		}
	}
}

