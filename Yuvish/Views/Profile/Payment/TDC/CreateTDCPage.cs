﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Yuvish
{
	public class CreateTDCPage : ContentPage
	{
		Dictionary<string, string> MesDicc = new Dictionary<string, string>();
		Dictionary<string, string> AñoDicc = new Dictionary<string, string> ();
		bool IsFromService;

		Button BackMenuImage;
		Button SaveMenuImage;

		EntryCustom NameEntry;
		EntryCustom NumeroTDCEntry;
		EntryCustom CVCEntry;
		PickerCustom MesEntry;
		PickerCustom AñoEntry;
		Image TypeCardImage;

		ContentViewCustom CreditCardView;

		StackLayout BarStack;
		StackLayout Stack;

		RelativeLayout Relative;

		public CreateTDCPage (bool IsFromService)
		{
			this.IsFromService = IsFromService;

			Title = "Nueva TDC";

			setDiccionaries ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void setDiccionaries ()
		{
			for (int i = 1; i < 13; i++) {
				if (i < 10) 
					MesDicc.Add ("0" + i, "0" + i);
				else 
					MesDicc.Add (i.ToString(), i.ToString());
			}

			for (int i = 2016; i < 2025; i++) {
				AñoDicc.Add (i.ToString(), i.ToString());				
			}
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			SaveMenuImage = new Button {
				Image = "ic_save.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			CreditCardView = new ContentViewCustom
			{
				BackgroundColor = ColorResource.PantoneBlackC,
				Radius = 20,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			NameEntry = getEntry("Alias");
			NameEntry.HorizontalOptions = LayoutOptions.FillAndExpand;
			NumeroTDCEntry = getEntry("Número de tarjeta");
			NumeroTDCEntry.HorizontalOptions = LayoutOptions.FillAndExpand;
			CVCEntry = getEntry("CVV");

			MesEntry = new PickerCustom()
			{				
				Title = "Mes",
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				BackgroundColor = Color.White,			
				TextColor = ColorResource.Pantone446c,
				//WidthRequest = 40,
				HeightRequest = 40,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			foreach(var mes in MesDicc) {
				MesEntry.Items.Add (mes.Key);
			}

			AñoEntry = new PickerCustom()
			{
				Title = "Año",
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				BackgroundColor = Color.White,
				TextColor = ColorResource.Pantone446c,
				//WidthRequest = 40,
				HeightRequest = 40,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			foreach(var año in AñoDicc) {
				AñoEntry.Items.Add (año.Key);
			}

			TypeCardImage = new Image { 
				HorizontalOptions = LayoutOptions.EndAndExpand
			};
		}

		EntryCustom getEntry (string text)
		{
			return new EntryCustom {
				Placeholder = text,
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Color.White,
				TextColor = ColorResource.Pantone446c,
				//IsEnabled = false,
				HeightRequest = 40,
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 20,
				Padding = new Thickness (15, 10),
				WidthRequest = App.ScreenWidth - 30,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Nueva Tarjeta",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			BarStack.Children.Add (SaveMenuImage);

			CreditCardView.Content = new StackLayout
			{
				Padding = new Thickness(20),
				Children = { 
					new StackLayout ()
					{
						Orientation = StackOrientation.Horizontal,
						Spacing = 20,
						Children = {
							new Image {
								Source = "ic_gota.png",
								Scale = 1.5f,
							},
							NameEntry
						}
					},
					new StackLayout ()
					{
						Orientation = StackOrientation.Horizontal,
						Spacing = 20,
						Children = {
							NumeroTDCEntry,
							CVCEntry
						}
					},
					new StackLayout ()
					{
						Orientation = StackOrientation.Horizontal,
						Spacing = 10,
						Children = {
							new Label {
								Text = "VALID",
								FontFamily = Fonts.AvenirNext,
								FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
								TextColor = Color.Gray,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							MesEntry,
							new Label {
								Text = "/",
								FontFamily = Fonts.AvenirNext,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.PantoneBlackC5,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							AñoEntry,
							TypeCardImage
						}
					},
				}
			};

			Stack.Children.Add(CreditCardView);

			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (
				Stack,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync ();

			SaveMenuImage.Clicked += async (sender, e) => {
				string bankTDC = NameEntry.Text;
				string numeroTDC = NumeroTDCEntry.Text;
				string cvcTDC = CVCEntry.Text;

				var mesSelected = MesEntry.SelectedIndex + 1;
				string mesTDC = "";
				if (mesSelected < 10)
					mesTDC = "0" + mesSelected.ToString();
				else
					mesTDC = mesSelected.ToString();

				var añoSelected = 2016 + AñoEntry.SelectedIndex;
				string añoTDC = añoSelected.ToString();				

				if (!string.IsNullOrEmpty (numeroTDC) && !string.IsNullOrEmpty (cvcTDC) && !string.IsNullOrEmpty (mesTDC) && !string.IsNullOrEmpty (añoTDC) && !string.IsNullOrEmpty (bankTDC)) {
					var creditCard = new CreditCardModel {
						BankName = bankTDC,
						Number = numeroTDC,
						UserEmail = App.CurrentUser.email,
						ExpMonth = mesTDC,
						ExpYear = añoTDC,
						CVC = cvcTDC
					};

					if (!App.LocalDataMan.ExistCreditCard (numeroTDC)) {

						if (App.LocalDataMan.CreateCreditCard (creditCard)) {
							if (!IsFromService) {
								Navigation.PopAsync ();
							}
						} else {
							if (!IsFromService) {
								DisplayAlert ("", "Ocurrió un error al intentar guardar la tarjeta", "OK");
							}
						}

						if (IsFromService) {
							var isTokenValid = await DependencyService.Get<IStripe>().GetToken (numeroTDC, cvcTDC, añoTDC, mesTDC);
							if (isTokenValid) {								
								Navigation.PopAsync ();
							} else {
								DisplayAlert ("", "Ocurrió un error al intentar generar el token", "OK");
							}
						}
					} else {
						DisplayAlert ("", "Ésta tarjeta ya se encuentra registrada", "OK");
					}	
				} else {
					DisplayAlert ("", "Se deben llenar todos los campos", "OK");
				}
			};

			NumeroTDCEntry.TextChanged += (sender, e) => {
				var num = NumeroTDCEntry.Text;

				if (num.StartsWith("4"))
					TypeCardImage.Source = "tarjeta_visa.png";
				else if (num.StartsWith("51") || num.StartsWith("52") || num.StartsWith("53") || num.StartsWith("54") || num.StartsWith("55"))
					TypeCardImage.Source = "tarjeta_master.png";
				else if (num.StartsWith("34") || num.StartsWith("37"))
					TypeCardImage.Source = "tarjeta_americanr.png";
				else
					TypeCardImage.Source = "";
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page CreateTDCPage");
		}
	}
}

