﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Yuvish
{
	public class DetailTDCPage : ContentPage
	{
		CreditCardModel CreditCard;
		Dictionary<string, string> MesDicc = new Dictionary<string, string>();
		Dictionary<string, string> AñoDicc = new Dictionary<string, string> ();

		Button BackMenuImage;
		Button SaveImage;
		Label TitleLabel;

		EntryCustom NameEntry;
		EntryCustom CreditCardEntry;
		EntryCustom CVCEntry;
		PickerCustom MonthEntry;
		PickerCustom YearEntry;
		Image TypeCardImage;

		ContentViewCustom CreditCardView;

		Image DeleteImage;

		StackLayout BarStack;
		StackLayout Stack;
		RelativeLayout Relative;

		TapGestureRecognizer TapGestureDeleteMenu = new TapGestureRecognizer();

		public DetailTDCPage (CreditCardModel CreditCard)
		{
			this.CreditCard = CreditCard;	

			Title = "Detalle TDC";

			setDiccionaries ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void setDiccionaries ()
		{
			for (int i = 1; i < 13; i++) {
				if (i < 10) 
					MesDicc.Add ("0" + i, "0" + i);
				else 
					MesDicc.Add (i.ToString(), i.ToString());
			}

			for (int i = 2016; i < 2025; i++) {
				AñoDicc.Add (i.ToString(), i.ToString());				
			}
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			SaveImage = new Button()
			{
				Image = "ic_save.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			TitleLabel = new Label
			{
				Text = "Detalle",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			CreditCardView = new ContentViewCustom
			{
				BackgroundColor = ColorResource.PantoneBlackC,
				//HeightRequest = 80,
				Radius = 20,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			NameEntry = getEntry (CreditCard.BankName);
			NameEntry.HorizontalOptions = LayoutOptions.FillAndExpand;
			CreditCardEntry = getEntry (CreditCard.Number);
			CreditCardEntry.HorizontalOptions = LayoutOptions.FillAndExpand;
			CVCEntry = getEntry ("");
			CVCEntry.Placeholder = "CVV";
			CVCEntry.WidthRequest = 60;
			//CVCEntry.IsVisible = false;

			MonthEntry = new PickerCustom()
			{								
				Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
				BackgroundColor = Color.White,
				TextColor = ColorResource.Pantone446c,		
				HeightRequest = 40,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			foreach(var mes in MesDicc) {
				MonthEntry.Items.Add (mes.Key);
			}
			MonthEntry.SelectedIndex = int.Parse(CreditCard.ExpMonth) - 1;

			YearEntry = new PickerCustom()
			{				
				Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
				BackgroundColor = Color.White,
				TextColor = ColorResource.Pantone446c,
				//WidthRequest = 40,
				HeightRequest = 40,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			foreach(var año in AñoDicc) {
				YearEntry.Items.Add (año.Key);
			}
			YearEntry.SelectedIndex = int.Parse(CreditCard.ExpYear) - 2016;

			TypeCardImage = new Image { 
				HorizontalOptions = LayoutOptions.EndAndExpand
			};
			var num = CreditCard.Number;

			if (num.StartsWith("4"))
				TypeCardImage.Source = "tarjeta_visa.png";
			else if (num.StartsWith("51") || num.StartsWith("52") || num.StartsWith("53") || num.StartsWith("54") || num.StartsWith("55"))
				TypeCardImage.Source = "tarjeta_master.png";
			else if (num.StartsWith("34") || num.StartsWith("37"))
				TypeCardImage.Source = "tarjeta_americanr.png";
			else
				TypeCardImage.Source = "";


			DeleteImage = new Image {
				Source = "ic_trash.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.End,
				GestureRecognizers = {
					TapGestureDeleteMenu
				}
			};
		}

		EntryCustom getEntry (string text)
		{
			return new EntryCustom {
				Text = text,
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = Color.White,
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 40,
				//HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 20,
				Padding = new Thickness (15, 10),
				WidthRequest = App.ScreenWidth - 30,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (TitleLabel);
			BarStack.Children.Add (SaveImage);


			CreditCardView.Content = new StackLayout
			{
				Padding = new Thickness(20),
				Children = { 
					new StackLayout ()
					{
						Orientation = StackOrientation.Horizontal,
						Spacing = 20,
						Children = {
							new Image {
								Source = "ic_gota.png",
								Scale = 1.5f,
							},
							NameEntry
						}
					},
					new StackLayout ()
					{
						Orientation = StackOrientation.Horizontal,
						Spacing = 15,
						Children = {
							CreditCardEntry,
							CVCEntry
						}
					},
					new StackLayout ()
					{
						Orientation = StackOrientation.Horizontal,
						Spacing = 10,
						Children = {
							new Label {
								Text = "VALID",
								FontFamily = Fonts.AvenirNext,
								FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
								TextColor = Color.Gray,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							MonthEntry,
							new Label {
								Text = "/",
								FontFamily = Fonts.AvenirNext,
								FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
								TextColor = ColorResource.PantoneBlackC5,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							YearEntry,
							TypeCardImage
						}
					},
				}
			};

			/*
			Stack.Children.Add (new StackLayout {
				Spacing = 15,
				Children = {
					getLabel ("Nombre del Banco"),
					NameEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				//Orientation = StackOrientation.Horizontal,
				Spacing = 15,
				Children = {
					getLabel ("Tarjeta"),
					CreditCardEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				//Orientation = StackOrientation.Horizontal,
				Spacing = 15,
				Children = {
					getLabel ("CVC"),
					CVCEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {					
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabel ("Mes"),
							MonthEntry,
						}
					},
					new StackLayout {
						Spacing = 10,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							getLabel ("Año"),
							YearEntry,
						}
					}
				}
			});
			*/

			Stack.Children.Add (CreditCardView);
			Stack.Children.Add (DeleteImage);

			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Stack,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		Label getLabel (string text)
		{
			return new Label {
				Text = text,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = Color.Gray,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync ();

			SaveImage.Clicked += async (sender, e) => {
				var nombre = NameEntry.Text;
				var number = CreditCardEntry.Text;
				var cvc = CVCEntry.Text;

				var mesSelected = MonthEntry.SelectedIndex + 1;
				string month = "";
				if (mesSelected < 10)
					month = "0" + mesSelected.ToString();
				else
					month = mesSelected.ToString();

				var añoSelected = 2016 + YearEntry.SelectedIndex;
				string year = añoSelected.ToString();			


				if (!string.IsNullOrEmpty (number) && !string.IsNullOrEmpty (month) && !string.IsNullOrEmpty (year) && !string.IsNullOrEmpty (nombre) && !string.IsNullOrEmpty (cvc)) {
					CreditCard.BankName = nombre;
					CreditCard.Number = number;
					CreditCard.CVC = cvc;
					CreditCard.ExpMonth = month;
					CreditCard.ExpYear = year;

					var canUpdate = await DisplayAlert ("Actualizar tarjeta", "¿Está seguro de querer actualizar ésta tarjeta?", "Actualizar", "Cancelar");

					if (canUpdate)
					{
						if (App.LocalDataMan.UpdateCreditCard (CreditCard)) {
							Navigation.PopAsync ();
						} else {
							DisplayAlert ("", "Ocurrió un error al intentar actualizar la información", "OK");
						}
					}
				} else {
					DisplayAlert ("", "Algunos datos están vacíos", "OK");
				}
			};

			TapGestureDeleteMenu.Tapped += async (sender, e) => {
				var canDelete = await DisplayAlert ("Borrar", "¿Estás seguro de querer eliminar ésta tarjeta?", "Eliminar", "Cancelar");
				if (canDelete) {
					if (App.LocalDataMan.DeleteCreditCard (CreditCard)) {
						Navigation.PopAsync ();
					} else {
						DisplayAlert ("", "Ocurrió un error al intentar eliminar el auto", "OK");
					}
				}
			};

			CreditCardEntry.TextChanged += (sender, e) => {
				var num = CreditCardEntry.Text;

				if (num.StartsWith("4"))
					TypeCardImage.Source = "tarjeta_visa.png";
				else if (num.StartsWith("51") || num.StartsWith("52") || num.StartsWith("53") || num.StartsWith("54") || num.StartsWith("55"))
					TypeCardImage.Source = "tarjeta_master.png";
				else if (num.StartsWith("34") || num.StartsWith("37"))
					TypeCardImage.Source = "tarjeta_americanr.png";
				else
					TypeCardImage.Source = "";
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page DetailTDCPage");
		}
	}
}

