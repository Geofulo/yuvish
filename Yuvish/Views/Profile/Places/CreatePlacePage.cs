﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Yuvish
{
	public class CreatePlacePage : ContentPage
	{
		Button BackMenuImage;
		Button SaveImage;

		MapCustom Map;
		Image CurrentLocationImage;
		Image LocationIconImage;
		Button SetIconAddressButton;

		EntryCustom NombreEntry;
		EntryCustom DireccionEntry;
		EditorCustom ObservacionesEditor;

		StackLayout BarStack;
		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		TapGestureRecognizer TapGestureCurrentLocation = new TapGestureRecognizer();

		public CreatePlacePage ()
		{
			Title = "Nuevo Lugar";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;			
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			SaveImage = new Button {
				Image = "ic_save.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			Map = new MapCustom {
				HasScrollEnabled = true,
				HasZoomEnabled = true,
				IsShowingUser = true,
				MapType = MapType.Street,
				WidthRequest = App.ScreenWidth,
				HeightRequest = 160,
			};
			Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position (19.4326, -99.1332), Distance.FromKilometers(0.18)));
			var positionCurrent = DependencyService.Get<ILocation> ().GetCurrentLocation ();
			if (positionCurrent != null) {
				Map.MoveToRegion(MapSpan.FromCenterAndRadius(positionCurrent, Distance.FromKilometers(0.18)));
			}

			CurrentLocationImage = new Image {
				Source = "ic_current_location.png",
				GestureRecognizers = {
					TapGestureCurrentLocation
				}
			};
			LocationIconImage = new Image {
				Source = "ic_pin.png",
			};
			SetIconAddressButton = new Button {
				Text = "Usar ubicación de pin",
				TextColor = Color.White,
				BackgroundColor = ColorResource.Pantone445c,
				BorderRadius = 15,
				WidthRequest = 160,
				HeightRequest = 30,
			};

			NombreEntry = getEntry ("Nombre");
			DireccionEntry = getEntry ("Dirección");
			DireccionEntry.FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Entry));

			ObservacionesEditor = new EditorCustom {
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 120,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		EntryCustom getEntry (string placeholder)
		{
			return new EntryCustom {		
				Placeholder = placeholder,
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 0,
				WidthRequest = App.ScreenWidth,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - App.SpacingTopBar - 25,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Nueva Ubicación",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			BarStack.Children.Add (SaveImage);


			var auxRelative = new RelativeLayout() 
			{ 
				WidthRequest = App.ScreenWidth,
				HeightRequest = HeightRequest = 160,
			};
			auxRelative.Children.Add(Map, Constraint.Constant(0), Constraint.Constant(0));
			auxRelative.Children.Add(CurrentLocationImage, Constraint.Constant(App.ScreenWidth - 60), Constraint.Constant(100));
			/*
			auxRelative.Children.Add(SetIconAddressButton,
				Constraint.Constant((App.ScreenWidth / 2) - (SetIconAddressButton.WidthRequest / 2)),
				Constraint.Constant(45)
			);
			*/
			auxRelative.Children.Add(LocationIconImage, Constraint.Constant((App.ScreenWidth / 2) - 10), Constraint.Constant(70));

			Stack.Children.Add (auxRelative);
			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Padding = new Thickness (15, 10),
				Children = {
					getLabelForm ("Nombre"),
					NombreEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Padding = new Thickness (15, 10),
				Children = {
					getLabelForm ("Dirección"),
					DireccionEntry
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 10,
				Padding = new Thickness (15, 10),
				Children = {
					new Label {
						Text = "Si el mapa no es lo suficientemente preciso para indicarnos la ubicación de tu auto, escribe aquí los detalles que nos permitan ubicarlo.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Micro, typeof(Label)),
						TextColor = Color.Gray,
					},
					getLabelForm ("Observaciones"),
					ObservacionesEditor
				}
			});

			Scroll.Content = Stack;

			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			//Relative.Children.Add (auxRelative, Constraint.Constant(0), Constraint.Constant(App.SpacingTopBar));
			Relative.Children.Add (Scroll, Constraint.Constant(0), Constraint.Constant(App.SpacingTopBar));
		}

		Label getLabelForm (string text)
		{
			return new Label {
				Text = text,
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				TextColor = Color.Gray,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync ();

			SaveImage.Clicked += async (sender, e) => {
				var posCenter = Map.VisibleRegion.Center;
				System.Diagnostics.Debug.WriteLine ("Center position: " + posCenter.Latitude + ", " + posCenter.Longitude);

				var possibleAddresses = await new Geocoder ().GetAddressesForPositionAsync (posCenter);	
				string address = "";
				foreach (var addr in possibleAddresses) {
					System.Diagnostics.Debug.WriteLine ("Position to Address: " + addr);
					address = addr;
				}

				var nombre = NombreEntry.Text;
				var observaciones = ObservacionesEditor.Text;
				var direccion = address;
				var latitud = posCenter.Latitude;
				var longitud = posCenter.Longitude;

				if (!string.IsNullOrEmpty (nombre) && !string.IsNullOrEmpty (direccion)) {
					if (!App.LocalDataMan.ExistPlace(nombre))
					{
						if (!App.LocalDataMan.ExistPlace(latitud, longitud))
						{
							var placeCreated = new PlaceModel ()
							{
								Nombre = nombre,
								Direccion = direccion,
								Observaciones = observaciones,
								Latitud = latitud,
								Longitud = longitud
							};
							if (App.LocalDataMan.CreatePlace (placeCreated)) {
								Navigation.PopAsync ();
							} else 
								DisplayAlert ("", "Ocurrió un error al intentar crear el auto", "OK");							
						}
						else 
							DisplayAlert ("", "Ya se encuentra registrado éste lugar", "OK");
					}
					else 
						DisplayAlert ("", "Ya existe un lugar con ese nombre", "OK");
				} else 
					DisplayAlert ("", "Se debe ingresar toda la información del lugar", "OK");
			};

			DireccionEntry.Completed += async (sender, e) => {
				var possiblePositions = await new Geocoder ().GetPositionsForAddressAsync(DireccionEntry.Text);
				foreach (var pos in possiblePositions) {					
					Map.MoveToRegion (MapSpan.FromCenterAndRadius (pos, Distance.FromKilometers(0.18)));
				}
			};

			TapGestureCurrentLocation.Tapped += (sender, e) => {
				var position = DependencyService.Get<ILocation>().GetCurrentLocation();
				Map.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromKilometers(0.18)));
			};

			SetIconAddressButton.Clicked += async (sender, e) => {
				var posCenter = Map.VisibleRegion.Center;
				var possibleAddresses = await new Geocoder ().GetAddressesForPositionAsync (posCenter);	
				string address = "";
				foreach (var addr in possibleAddresses) {
					System.Diagnostics.Debug.WriteLine ("Position to Address: " + addr);
					address = addr;
				}
				DireccionEntry.Text = address;
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page CreatePlacePage");
		}
	}
}

