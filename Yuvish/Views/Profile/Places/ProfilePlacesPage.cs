﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class ProfilePlacesPage : ContentPage
	{
		List<PlaceModel> Places;

		Button BackMenuImage;
		Button AddImage;

		List<ContentView> PlacesViews = new List<ContentView>();
		Label NoPlacesLabel;

		StackLayout BarStack;
		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		List<TapGestureRecognizer> PlacesTapGestures = new List<TapGestureRecognizer>();

		public ProfilePlacesPage ()
		{
			Title = "Mis Lugares";

			getPlaces ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void getPlaces ()
		{
			Places = App.LocalDataMan.GetPlaces ();
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			AddImage = new Button {
				Image = "ic_plus.png",
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			setPlacesViews ();
		}

		void setPlacesViews ()
		{
			PlacesViews.Clear ();
			PlacesTapGestures.Clear ();

			if (Places != null) {
				if (Places.Count > 0) {	
					foreach (var place in Places) {
						var tapGesture = new TapGestureRecognizer ();

						PlacesViews.Add (new ContentView {
							Padding = new Thickness (20, 2),
							Content = new StackLayout {
								Orientation = StackOrientation.Horizontal,
								Children = {
									new StackLayout {
										Spacing = 10,
										HorizontalOptions = LayoutOptions.FillAndExpand,
										Children = {
											new Label {
												Text = place.Nombre,
												FontFamily = Fonts.AvenirNext,
												FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
												TextColor = ColorResource.Pantone446c,
											},
											new Label {
												Text = place.Direccion,
												FontFamily = Fonts.AvenirNext,
												FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
												TextColor = ColorResource.Pantone446c,
											},
										}
									},
									new Image {
										Source = "ic_list_view.png",
										VerticalOptions = LayoutOptions.CenterAndExpand,
										HorizontalOptions = LayoutOptions.End,
									}
								}
							},
							GestureRecognizers = {
								tapGesture
							}
						});

						PlacesTapGestures.Add (tapGesture);
					}
				} else {
					NoPlacesLabel = new Label {
						Text = "No tienes ninguna ubicación. \nAgrega una ahora.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
						HorizontalTextAlignment = TextAlignment.Center,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					};
				}
			}
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Spacing = 20,
				Padding = new Thickness(0, 10),
				WidthRequest = App.ScreenWidth,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - App.SpacingTopBar + 10,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Clear ();
			Stack.Children.Clear ();
			Relative.Children.Clear ();


			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Mis Ubicaciones",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			BarStack.Children.Add (AddImage);


			if (PlacesViews.Count > 0) {
				foreach (var view in PlacesViews) {
					Stack.Children.Add (view);
					Stack.Children.Add (new BoxView {				
						BackgroundColor = ColorResource.Pantone445c,
						HeightRequest = 1,
						HorizontalOptions = LayoutOptions.FillAndExpand,
					});
				}
			} else {
				Stack.Children.Add (NoPlacesLabel);
			}

			Scroll.Content = Stack;

			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Scroll,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync ();

			AddImage.Clicked += (sender, e) => Navigation.PushAsync (new CreatePlacePage ());

			setPLacesViewsListeners ();
		}

		void setPLacesViewsListeners ()
		{
			
			for (int i = 0; i < PlacesTapGestures.Count; i++) {
				var tap = PlacesTapGestures [i];
				var placePage = new DetailPlacePage (Places [i]);

				tap.Tapped += (sender, e) => Navigation.PushAsync (placePage);
			}
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page ProfilePlacesPage");

			getPlaces ();
			setPlacesViews ();
			setLayouts ();
			setPLacesViewsListeners ();
		}
	}
}

