﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class ProfilePage : ContentPage
	{
		Button BackMenuImage;

		StackLayout GeneralStack;
		StackLayout PaymentStack;
		StackLayout PlacesStack;
		StackLayout CarsStack;
		StackLayout FiscalStack;

		StackLayout BarStack;
		StackLayout Stack;
		RelativeLayout Relative;

		TapGestureRecognizer TapGestureGeneral = new TapGestureRecognizer();
		TapGestureRecognizer TapGesturePayment = new TapGestureRecognizer();
		TapGestureRecognizer TapGesturePlaces = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureCars = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureFiscal = new TapGestureRecognizer();

		public ProfilePage ()
		{
			Title = "Perfil";

			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void createElements ()
		{
			BackMenuImage = new Button ()
			{
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createLayouts ()
		{
			GeneralStack = getStackMenu ();
			GeneralStack.GestureRecognizers.Add (TapGestureGeneral);

			PaymentStack = getStackMenu ();
			PaymentStack.GestureRecognizers.Add (TapGesturePayment);

			PlacesStack = getStackMenu ();
			PlacesStack.GestureRecognizers.Add (TapGesturePlaces);

			CarsStack = getStackMenu ();
			CarsStack.GestureRecognizers.Add (TapGestureCars);

			FiscalStack = getStackMenu ();
			FiscalStack.GestureRecognizers.Add (TapGestureFiscal);

			BarStack = new StackLayout {
				Spacing = 20,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 5),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout {
				Padding = new Thickness(0, 10),
				Spacing = 20,
				WidthRequest = App.ScreenWidth,
			};

			Relative = new RelativeLayout ();
		}

		StackLayout getStackMenu ()
		{
			return new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 20,
				Padding = new Thickness (20, 10, 20, 10),
			};
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (new Label {
				Text = "Perfil",
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});


			GeneralStack.Children.Add (new Image {
				Source = "ic_perfil_datos.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			GeneralStack.Children.Add (getLabelMenu ("Datos Generales"));
			GeneralStack.Children.Add (new Image {
				Source = "ic_list_view.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
			});

			PaymentStack.Children.Add (new Image {
				Source = "ic_perfil_pago.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			PaymentStack.Children.Add (getLabelMenu ("Mis Tarjetas"));
			PaymentStack.Children.Add (new Image {
				Source = "ic_list_view.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
			});

			PlacesStack.Children.Add (new Image {
				Source = "ic_perfil_ubicaciones.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			PlacesStack.Children.Add (getLabelMenu ("Mis Ubicaciones"));
			PlacesStack.Children.Add (new Image {
				Source = "ic_list_view.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
			});

			CarsStack.Children.Add (new Image {
				Source = "ic_perfil_autos2.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			CarsStack.Children.Add (getLabelMenu ("Mis Autos"));
			CarsStack.Children.Add (new Image {
				Source = "ic_list_view.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
			});

			FiscalStack.Children.Add (new Image {
				Source = "ic_perfil_fiscales.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
			});
			FiscalStack.Children.Add (getLabelMenu ("Datos Fiscales"));
			FiscalStack.Children.Add (new Image {
				Source = "ic_list_view.png",
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.End,
			});

			Stack.Children.Add (GeneralStack);
			Stack.Children.Add (PaymentStack);
			Stack.Children.Add (PlacesStack);
			Stack.Children.Add (CarsStack);
			Stack.Children.Add (FiscalStack);


			Relative.Children.Add (
				new StackLayout ()
				{
					Children = {
						BarStack,
						new BoxView {				
							BackgroundColor = ColorResource.Pantone445c,
							HeightRequest = 1,
							HorizontalOptions = LayoutOptions.FillAndExpand,
						}
					}
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Stack,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
		}

		Label getLabelMenu (string text)
		{
			return new Label {
				Text = text,
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
				TextColor = ColorResource.Pantone446c,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync();

			TapGestureGeneral.Tapped += (sender, e) => Navigation.PushAsync (new ProfileGeneralPage());
			TapGesturePayment.Tapped += (sender, e) => Navigation.PushAsync (new ProfilePaymentPage());
			TapGesturePlaces.Tapped += (sender, e) => Navigation.PushAsync (new ProfilePlacesPage());
			TapGestureCars.Tapped += (sender, e) => Navigation.PushAsync (new ProfileCarsPage(false));

			TapGestureFiscal.Tapped += (sender, e) => {

				var fiscal = App.LocalDataMan.GetFiscal ();

				if (fiscal != null) {
					Navigation.PushAsync (new ProfileFiscalPage(fiscal));
				} else {
					Navigation.PushAsync (new ProfileFiscalPage(null));
				}
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page ProfilePage");
		}
	}
}

