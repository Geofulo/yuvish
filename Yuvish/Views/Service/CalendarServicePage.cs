﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class CalendarServicePage : ContentPage
	{
		Dictionary<string, string> HoursDicc = new Dictionary<string, string> ();
		Dictionary<string, string> MinutesDicc = new Dictionary<string, string> ();

		List<int> DaysSelected = new List<int> ();
		List<int> DaysSelectedByRepeat = new List<int> ();

		Button BackMenuImage;
		Image LogoImage;
		Button OKImagen;

		Label MonthLabel;
		Label YearLabel;
		List<Button> DaysLabels = new List<Button>();
		List<StackLayout> CalendarDayStacks = new List<StackLayout> ();

		StepperCustom RepeatStepper;
		Label NumberRepeatLabel;

		PickerCustom HourTimePicker;
		PickerCustom MinuteTimePicker;

		StackLayout BarStack;
		StepsView StepsView;

		Button NextButton;
		Button PrevButton;

		StackLayout CalendarStack;
		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		//TapGestureRecognizer TapGestureNextMonth = new TapGestureRecognizer();
		//TapGestureRecognizer TapGesturePrevMonth = new TapGestureRecognizer();

		int oldValueStepper = 0;

		static int _labelCalendarWidth = (App.ScreenWidth - 40) / 7;
		static int _labelCalendarHeight = 30;
		static double _calendarDaysFontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label));
		static Font _calendarDaysFont = Font.OfSize (Fonts.AvenirNext, NamedSize.Small);

		//int DaysOfMonth;
		//int DayOfWeek;
		//int BeginDayOfMonth;

		int countDatesSelected = 0;

		public CalendarServicePage ()
		{
			Title = "Yuvish";

			setDiccionaries ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void setDiccionaries ()
		{
			for (int i = 8; i < 18; i++) {
				if (i < 10) 
					HoursDicc.Add ("0" + i, "0" + i);
				else 
					HoursDicc.Add (i.ToString(), i.ToString());
			}

			for (int i = 0; i < 4; i++) {
				var min = i * 15;
				if (min == 0) {
					MinutesDicc.Add ("00", "00");
				} else {
					MinutesDicc.Add (min.ToString(), min.ToString());
				}
			}
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			LogoImage = new Image {
				Source = ImageSource.FromResource("logo.png"),
				Scale = 1.2f,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			OKImagen = new Button {
				Image = "continuar_dark.png",
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			MonthLabel = new Label {
				Font = Font.OfSize(Fonts.AvenirNext, NamedSize.Medium),
				TextColor = Color.White,
				HorizontalTextAlignment = TextAlignment.End,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			YearLabel = new Label {
				Font = Font.OfSize(Fonts.AvenirNext, NamedSize.Medium),
				TextColor = Color.White,
				HorizontalTextAlignment = TextAlignment.Start,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			PrevButton = new Button {
				Text = "<",	
				Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Large),
				TextColor = Color.White,
				WidthRequest = 40,
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			NextButton = new Button {
				Text = ">",	
				Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Large),
				TextColor = Color.White,
				WidthRequest = 40,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};


			setCalendar (DateTime.Now.Year, DateTime.Now.Month);

			RepeatStepper = new StepperCustom {
				Value = 0,
				Minimum = 0,
				Increment = 1,
				TintColor = ColorResource.Pantone3252c,
			};
			NumberRepeatLabel = new Label {
				Text = RepeatStepper.Value.ToString(),
				FontAttributes = FontAttributes.Bold,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				TextColor = ColorResource.Pantone3252c,
				VerticalTextAlignment = TextAlignment.Center,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			HourTimePicker = new PickerCustom {
				//BackgroundColor = Color.Transparent,	
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				IsTextAligmentCenter = true,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			foreach(var hour in HoursDicc) {
				HourTimePicker.Items.Add (hour.Key);
			}

			MinuteTimePicker = new PickerCustom {
				//BackgroundColor = Color.Transparent,
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				IsTextAligmentCenter = true,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			foreach(var min in MinutesDicc) {
				MinuteTimePicker.Items.Add (min.Key);
			}

			var hourNow = DateTime.Now.Hour;
			var minNow = DateTime.Now.Minute;


			if (minNow < 15) {
				MinuteTimePicker.SelectedIndex = 1;
			} else if (minNow < 30) {
				MinuteTimePicker.SelectedIndex = 2;
			} else if (minNow < 45) {
				MinuteTimePicker.SelectedIndex = 3;
			} else if (minNow >= 45) {
				MinuteTimePicker.SelectedIndex = 0;
			}


			if (hourNow < 8) {
				HourTimePicker.SelectedIndex = 0;
				if (minNow < 30)
					MinuteTimePicker.SelectedIndex = 2;
			} else if (hourNow > 18) {
				HourTimePicker.SelectedIndex = 0;
				if (minNow > 30)
					MinuteTimePicker.SelectedIndex = 2;
			} else {
				HourTimePicker.SelectedIndex = hourNow - 8;
			}


		}

		void setCalendar (int year, int month)
		{
			DaysLabels.Clear ();

			int daysOfMonth = DateTime.DaysInMonth (year, month);
			int beginDayOfMonth = 0;

			switch (new DateTime (year, month, 1).DayOfWeek.ToString()) {
			case "Sunday": 
				beginDayOfMonth = 0;
				break;
			case "Monday": 
				beginDayOfMonth = 1;
				break;
			case "Tuesday": 
				beginDayOfMonth = 2;
				break;
			case "Wednesday": 
				beginDayOfMonth = 3;
				break;
			case "Thursday": 
				beginDayOfMonth = 4;
				break;
			case "Friday": 
				beginDayOfMonth = 5;
				break;
			case "Saturday": 
				beginDayOfMonth = 6;
				break;
			default :
				break;
			}

			System.Diagnostics.Debug.WriteLine ("month: " + month);
			System.Diagnostics.Debug.WriteLine ("year: " + year);
			System.Diagnostics.Debug.WriteLine ("beginDayOfMonth: " + beginDayOfMonth);
			System.Diagnostics.Debug.WriteLine ("daysOfMonth: " + daysOfMonth);

			if (month == 1) {
				MonthLabel.Text = "Enero";
			} else if (month == 2) {
				MonthLabel.Text = "Febrero";
			} else if (month == 3) {
				MonthLabel.Text = "Marzo";
			} else if (month == 4) {
				MonthLabel.Text = "Abril";
			} else if (month == 5) {
				MonthLabel.Text = "Mayo";
			} else if (month == 6) {
				MonthLabel.Text = "Junio";
			} else if (month == 7) {
				MonthLabel.Text = "Julio";
			} else if (month == 8) {
				MonthLabel.Text = "Agosto";
			} else if (month == 9) {
				MonthLabel.Text = "Septiembre";
			} else if (month == 10) {
				MonthLabel.Text = "Octubre";
			} else if (month == 11) {
				MonthLabel.Text = "Noviembre";
			} else if (month == 12) {
				MonthLabel.Text = "Diciembre";
			}

			YearLabel.Text = year.ToString ();

			bool auxCal = false;
			int indice = 1;
			for (int i = 0; i < 42; i++) {
				if (i == beginDayOfMonth) {
					auxCal = true;
				}
				if (!auxCal) {
					DaysLabels.Add (new Button {
						Font = _calendarDaysFont,
						WidthRequest = _labelCalendarHeight,
						HeightRequest = _labelCalendarHeight,
						BorderRadius = _labelCalendarHeight / 2,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
					});
				} else {
					if (indice > daysOfMonth) {
						DaysLabels.Add (new Button {
							Font = _calendarDaysFont,
							WidthRequest = _labelCalendarHeight,
							HeightRequest = _labelCalendarHeight,
							BorderRadius = _labelCalendarHeight / 2,
							HorizontalOptions = LayoutOptions.CenterAndExpand,
						});
					} else {												
						DaysLabels.Add (new Button {
							Text = indice.ToString (),
							Font = _calendarDaysFont,
							TextColor = Color.White,
							WidthRequest = _labelCalendarHeight,
							HeightRequest = _labelCalendarHeight,
							BorderRadius = _labelCalendarHeight / 2,
							HorizontalOptions = LayoutOptions.CenterAndExpand,
						});

						if (indice == DateTime.Now.Day && month == DateTime.Now.Month && year == DateTime.Now.Year ) {
							DaysLabels [i].TextColor = ColorResource.PantoneBlackC;
						}
						indice = indice + 1;
					}
				}


			}
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 10),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			StepsView = new StepsView ();
			StepsView.OneStepCompleted ();

			CalendarStack = new StackLayout {				
				Spacing = 10,
				Padding = new Thickness (20),
				BackgroundColor = ColorResource.Pantone3252c,
			};

			Stack = new StackLayout {				
				Spacing = 0,
				WidthRequest = App.ScreenWidth,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - 70 - StepsView.HeightRequest - 40,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (LogoImage);
			BarStack.Children.Add (OKImagen);

			Stack.Children.Add (new ContentView {
				Padding = new Thickness(20, 0, 0, 20),
				Content = new Label {
					Text = "Agenda",
					Font = Font.OfSize(Fonts.AvenirNext_D, NamedSize.Large),
					FontAttributes = FontAttributes.Bold,
					TextColor = ColorResource.Pantone446c,
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 0),
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 50,
				Children = {
					PrevButton,
					new StackLayout {
						Spacing = 15,
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions = LayoutOptions.CenterAndExpand,
						Children = {
							MonthLabel,
							YearLabel
						}
					},
					NextButton
				}
			});

			setCalendarInLayout ();

			Stack.Children.Add (CalendarStack);

			/*
			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15),
				Children = {
					new Label {
						Text = "¿Cuántas veces quiere repetir el servicio?",
						FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
						TextColor = ColorResource.Pantone446c,
					},
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Spacing = 10,
						Children = {
							RepeatStepper,
							NumberRepeatLabel,
							new Label {
								Text = "semanas",
								FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
								TextColor = ColorResource.Pantone446c,
								VerticalTextAlignment = TextAlignment.Center,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							}
						}
					}
				}
			});
			*/
			Stack.Children.Add (new StackLayout {
				Padding = new Thickness(15),
				Children = {
					new Label {
						Text = "¿Qué horario prefieres?",
						Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
						TextColor = ColorResource.Pantone446c,
					},
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Children = {
							HourTimePicker,
							new Label {
								Text = ":",
								Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
								TextColor = ColorResource.Pantone446c,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							MinuteTimePicker
						}
					}
				}
			});

			Scroll.Content = Stack;

			Relative.Children.Add (BarStack,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Scroll,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
			Relative.Children.Add (StepsView,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height - StepsView.HeightRequest - 20; })
			);
		}

		void setCalendarInLayout ()
		{
			CalendarDayStacks.Clear ();
			CalendarStack.Children.Clear ();

			for (int i = 0; i < 6; i++) {				
				CalendarDayStacks.Add (new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Spacing = 0,
				});
			}

			CalendarStack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				Children = {
					new Label {
						Text = "Dom",
						FontFamily = Fonts.AvenirNext,
						FontSize = _calendarDaysFontSize,
						WidthRequest = _labelCalendarWidth,
						HeightRequest = _labelCalendarHeight,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
					},
					new Label {
						Text = "Lun",
						FontFamily = Fonts.AvenirNext,
						FontSize = _calendarDaysFontSize,
						WidthRequest = _labelCalendarWidth,
						HeightRequest = _labelCalendarHeight,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
					},
					new Label {
						Text = "Mar",
						FontFamily = Fonts.AvenirNext,
						FontSize = _calendarDaysFontSize,
						WidthRequest = _labelCalendarWidth,
						HeightRequest = _labelCalendarHeight,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
					},
					new Label {
						Text = "Mie",
						FontFamily = Fonts.AvenirNext,
						FontSize = _calendarDaysFontSize,
						WidthRequest = _labelCalendarWidth,
						HeightRequest = _labelCalendarHeight,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
					},
					new Label {
						Text = "Jue",
						FontFamily = Fonts.AvenirNext,
						FontSize = _calendarDaysFontSize,
						WidthRequest = _labelCalendarWidth,
						HeightRequest = _labelCalendarHeight,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
					},
					new Label {
						Text = "Vie",
						FontFamily = Fonts.AvenirNext,
						FontSize = _calendarDaysFontSize,
						WidthRequest = _labelCalendarWidth,
						HeightRequest = _labelCalendarHeight,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
					},
					new Label {
						Text = "Sab",
						FontFamily = Fonts.AvenirNext,
						FontSize = _calendarDaysFontSize,
						WidthRequest = _labelCalendarWidth,
						HeightRequest = _labelCalendarHeight,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
					},
				}
			});

			for (int i = 0; i < 6; i++) {
				for (int j = 0; j < 7; j++) {
					var stackC = CalendarDayStacks [i];
					stackC.Children.Add (DaysLabels[(i * 7) + j]);
					CalendarStack.Children.Add (stackC);
				}
			}

		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync();

			OKImagen.Clicked += (sender, e) => {
				if (DaysSelected.Count > 0) {			
					var hours = HourTimePicker.Items [HourTimePicker.SelectedIndex];
					var minutes = MinuteTimePicker.Items [MinuteTimePicker.SelectedIndex];
					var day = DaysSelected[0].ToString();
					var month = getMonthInt (MonthLabel.Text).ToString();

					if (DaysSelected[0] < 10) day = "0" + day;

					if (int.Parse (month) < 10) month = "0" + month;

					var datetime = day + "/" + month + "/" + YearLabel.Text + " " + hours + ":" + minutes;
					System.Diagnostics.Debug.WriteLine ("Datetime Service: " + datetime);

					App.Order.datetime = datetime;

					Navigation.PushAsync (new CarServicePage ());
				} else {
					DisplayAlert ("", "Debe ingresar una fecha y una hora", "Entiendo");
				}
			};

			/*
			RepeatStepper.ValueChanged += (sender, e) => {
				NumberRepeatLabel.Text = RepeatStepper.Value.ToString();

				if (RepeatStepper.Value > 0) {
					if (RepeatStepper.Value > oldValueStepper) {	// Incremento
						for (int i = 1; i < RepeatStepper.Value + 1; i++) {
							foreach (var day in DaysSelected) {
								var dayRepeat = day + (7 * i);
								DaysSelectedByRepeat.Add (dayRepeat);
								selectCalendarLabel (DaysLabels [dayRepeat - 1]);
							}
						}
					} else {	// Disminuyo
						for (int i = 0; i < DaysSelected.Count; i++) {
							var dayEliminated = DaysSelectedByRepeat[DaysSelectedByRepeat.Count - 1];
							DaysSelectedByRepeat.Remove (dayEliminated);
							deselectCalendarLabel (DaysLabels [dayEliminated - 1]);
						}
					}
				} else {
					foreach (var day in DaysSelectedByRepeat) {
						deselectCalendarLabel (DaysLabels [day - 1]);
					}
					DaysSelectedByRepeat.Clear ();
				}
				oldValueStepper = (int) RepeatStepper.Value;
			};
			*/

			setDayLabelsListeners ();

			/*
			TapGesture.Tapped += (sender, e) => {
				var label = sender as Label;
				int indexDay = int.Parse(label.Text);
				if (indexDay >= DayOfMonth && Month >= DateTime.Now.Month) {
					if (DaysSelected.Contains(int.Parse(label.Text))) {					
						label.BackgroundColor = ColorResource.Pantone3252c;
						label.TextColor = ColorResource.PantoneBlackC5;
						DaysSelected.Remove (int.Parse(label.Text));
					} else {
						label.BackgroundColor = ColorResource.PantoneBlackC5;
						label.TextColor = ColorResource.Pantone3252c;
						DaysSelected.Add (int.Parse(label.Text));
					}
				}
			};*/

			PrevButton.Clicked += (sender, e) => {
				var m = 0;

				switch (MonthLabel.Text) {
				case "Enero":
					m = 1 - 1;
					break;
				case "Febrero":
					m = 2 - 1;
					break;
				case "Marzo":
					m = 3 - 1;
					break;
				case "Abril":
					m = 4 - 1;
					break;
				case "Mayo":
					m = 5 - 1;
					break;
				case "Junio":
					m = 6 - 1;
					break;
				case "Julio":
					m = 7 - 1;
					break;
				case "Agosto":
					m = 8 - 1;
					break;
				case "Septiembre":
					m = 9 - 1;
					break;
				case "Octubre":
					m = 10 - 1;
					break;
				case "Noviembre":
					m = 11 - 1;
					break;
				case "Diciembre":
					m = 12 - 1;
					break;			
				default :
					break;
				}

				var y = int.Parse (YearLabel.Text);
				if (m < 1) {
					m = 12;
					y--;
				}

				/*
				Month = Month - 1;
				if (Month < 1) {
					Month = 12;
					Year--;
				}
				var daysMonth = DateTime.DaysInMonth (Year, Month);
				BeginDayOfMonth = BeginDayOfMonth - (daysMonth - 29) - 1;
				System.Diagnostics.Debug.WriteLine ("BDM: " + BeginDayOfMonth);
				int auxBD = 7 - (BeginDayOfMonth * (-1));
				if (BeginDayOfMonth < 0) {
					BeginDayOfMonth = auxBD;
				}*/

				setCalendar (y, m);
				setCalendarInLayout ();
				setDayLabelsListeners ();
			};

			NextButton.Clicked += (sender, e) => {
				var m = getMonthInt (MonthLabel.Text);
				m++;
				var y = int.Parse (YearLabel.Text);
				if (m > 12) {
					m = 1;
					y++;
				}

				setCalendar (y, m);
				setCalendarInLayout ();
				setDayLabelsListeners ();
			};
		}

		void setDayLabelsListeners ()
		{
			foreach (var btn in DaysLabels)
			{
				btn.Clicked += (sender, e) => { 					
					int indexDay = int.Parse(btn.Text);
					int y = int.Parse (YearLabel.Text);
					int m = getMonthInt (MonthLabel.Text);
					
					var dateAux = new DateTime (y, m, indexDay);
					if (dateAux.DayOfWeek.ToString() != "Sunday") 
					{
						if (m == DateTime.Now.Month) 
						{
							if (indexDay >= DateTime.Now.Day) 
								markLabel (btn);							
						}
						else if (m > DateTime.Now.Month)
							markLabel (btn);
					}
				};
			}
		}

		void markLabel (Button btn)
		{
			if (!DaysSelected.Contains (int.Parse (btn.Text))) {			
				DaysSelected.Clear ();
				foreach (var dayBtn in DaysLabels) {
					dayBtn.BackgroundColor = ColorResource.Pantone3252c;
					dayBtn.TextColor = ColorResource.PantoneBlackC5;
				}
				btn.BackgroundColor = ColorResource.PantoneBlackC5;
				btn.TextColor = ColorResource.Pantone3252c;
				DaysSelected.Add (int.Parse (btn.Text));		
			}
		}

		void selectCalendarLabel (Button label) 
		{
			label.BackgroundColor = ColorResource.PantoneBlackC5;
			label.TextColor = ColorResource.Pantone3252c;
		}

		void deselectCalendarLabel (Button label) 
		{
			label.BackgroundColor = ColorResource.Pantone3252c;
			label.TextColor = ColorResource.PantoneBlackC5;
		}

		int getMonthInt (string month)
		{
			if (month.Equals ("Enero"))
				return 1;
			else if (month.Equals ("Febrero"))
				return 2;
			else if (month.Equals ("Marzo"))
				return 3;
			else if (month.Equals ("Abril"))
				return 4;
			else if (month.Equals ("Mayo"))
				return 5;
			else if (month.Equals ("Junio"))
				return 6;
			else if (month.Equals ("Julio"))
				return 7;
			else if (month.Equals ("Agosto"))
				return 8;
			else if (month.Equals ("Septiembre"))
				return 9;
			else if (month.Equals ("Octubre"))
				return 10;
			else if (month.Equals ("Noviembre"))
				return 11;
			else if (month.Equals ("Diciembre"))
				return 12;
			else
				return 0;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page CalendarServicePage");
		}
	}
}

