﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yuvish
{
	public class CarServicePage : ContentPage
	{
		List<BrandModel> Brands;
		List<ModeloModel> Modelos = new List<ModeloModel> ();

		Dictionary<string, BrandModel> BrandsDicc = new Dictionary<string, BrandModel>();
		Dictionary<string, ModeloModel> ModelosDicc = new Dictionary<string, ModeloModel>();

		string PlacasAux;
		bool IsFromList = false;

		Button BackMenuImage;
		Image LogoImage;
		Button OKImagen;

		Button CarListButton;
		PickerCustom MarcaPicker;
		PickerCustom ModeloPicker;
		EntryCustom ColorEntry;
		EntryCustom PlacasEntry;
		EditorCustom InfoEditor;

		StackLayout BarStack;
		StepsView StepsView;

		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		static double _labelFontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Button));
		static Font _labelFont = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium);

		public CarServicePage ()
		{
			Title = "Yuvish";

			getCars ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void getCars ()
		{
			Brands = App.WSRest.GetMarcasModelos ();

			foreach(var brand in Brands) {
				BrandsDicc.Add (brand.brand, brand);
			}
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			LogoImage = new Image {
				Source = ImageSource.FromResource("logo.png"),
				Scale = 1.2f,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			OKImagen = new Button {
				Image = "continuar_dark.png",
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			CarListButton = new Button {
				Text = "Elegir de mi lista",	
				Font = Font.OfSize (Fonts.AvenirNext_M, NamedSize.Medium),
				TextColor = ColorResource.Pantone3252c,
				BackgroundColor = Color.Transparent,
				HeightRequest = 25,
				HorizontalOptions = LayoutOptions.EndAndExpand,
			};

			MarcaPicker = new PickerCustom {
				Title = "Selecciona la marca",
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				TextColor = ColorResource.Pantone446c,
				WidthRequest = App.ScreenWidth / 2 - 30,
				HeightRequest = 40,
				//HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			foreach(var brandD in BrandsDicc) {
				MarcaPicker.Items.Add (brandD.Key);
			}
			ModeloPicker = new PickerCustom {
				Title = "Selecciona el modelo",
				Font = Font.OfSize (Fonts.Caslon, NamedSize.Medium),
				TextColor = ColorResource.Pantone446c,
				WidthRequest = App.ScreenWidth / 2 - 30,
				HeightRequest = 40,
				//HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			ColorEntry = new EntryCustom {
				Placeholder = "Indica el color",
				FontFamily = Fonts.Caslon,
				FontSize = _labelFontSize,
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 40,
				WidthRequest = App.ScreenWidth / 2 - 30,
			};
			PlacasEntry = new EntryCustom {
				Placeholder = "Indica las placas",	
				FontFamily = Fonts.Caslon,
				FontSize = _labelFontSize,
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 40,
				WidthRequest = App.ScreenWidth / 2 - 30,
			};
			InfoEditor = new EditorCustom {
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Editor)),
				TextColor = ColorResource.Pantone446c,
				HeightRequest = 120,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 10),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			StepsView = new StepsView ();
			StepsView.OneStepCompleted ();
			StepsView.TwoStepCompleted ();

			Stack = new StackLayout {
				Spacing = 15,
				Padding = new Thickness (15, 0, 15, 10),
				WidthRequest = App.ScreenWidth - 30,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - 70 - StepsView.HeightRequest - 40,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (LogoImage);
			BarStack.Children.Add (OKImagen);

			Stack.Children.Add (new ContentView {
				Padding = new Thickness(5, 0),
				Content = new Label {
					Text = "Mi auto",
					Font = Font.OfSize (Fonts.AvenirNext_D, NamedSize.Large),
					FontAttributes = FontAttributes.Bold,
					TextColor = ColorResource.Pantone446c,
				}
			});
			Stack.Children.Add (CarListButton);
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					new StackLayout {
						Spacing = 5,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						Children = {
							new Label {
								Text = "Marca",
								Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand,
							},
							MarcaPicker
						}
					},
					new StackLayout {
						Spacing = 5,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						Children = {
							new Label {
								Text = "Modelo",
								Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand,
							},
							ModeloPicker
						}
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					new StackLayout {
						Spacing = 5,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						Children = {
							new Label {
								Text = "Color",
								Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand,
							},
							ColorEntry
						}
					},
					new StackLayout {
						Spacing = 5,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						Children = {
							new Label {
								Text = "Placas",
								Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand,
							},
							PlacasEntry
						}
					}
				}
			});
			Stack.Children.Add (new StackLayout {
				Spacing = 5,
				Padding = new Thickness(10, 0),
				Children = {
					new Label {						
						Text = "Información adicional",
						Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
						TextColor = ColorResource.Pantone446c,
						HorizontalOptions = LayoutOptions.FillAndExpand,
					},
					InfoEditor
				}
			});

			Scroll.Content = Stack;

			Relative.Children.Add (BarStack,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Scroll,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
			Relative.Children.Add (StepsView,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height - StepsView.HeightRequest - 20; })
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync();

			OKImagen.Clicked += async (sender, e) => {							
				string brandId = null; 
				string modeloId = null; 
				string brand = null; 
				string modelo = null;
				string size = null;
				if (MarcaPicker.SelectedIndex >= 0 && ModeloPicker.SelectedIndex >= 0) {
					brandId = Brands [MarcaPicker.SelectedIndex].idBrand;
					modeloId = Modelos [ModeloPicker.SelectedIndex].idModel;
					brand = Brands [MarcaPicker.SelectedIndex].brand;
					modelo = Modelos [ModeloPicker.SelectedIndex].name;
					size = Modelos [ModeloPicker.SelectedIndex].size;
				}
				var color = ColorEntry.Text;
				var placas = PlacasEntry.Text;
				var detalles = InfoEditor.Text;

				if (!string.IsNullOrEmpty(brand) && !string.IsNullOrEmpty(modelo) && !string.IsNullOrEmpty(color) && !string.IsNullOrEmpty(placas)) {
					System.Diagnostics.Debug.WriteLine ("Id Brand: " + brand);
					System.Diagnostics.Debug.WriteLine ("Model: " + modeloId + " " + modelo);

					if (!IsFromList) {
						var carLocalCreated = new CarLocalModel {
							Marca = brand,
							Modelo = modelo,
							Placas = placas,
							Color = color,
							Detalles = detalles,
							ModeloId = modeloId,
							MarcaId = brandId,
							Size = size,
						};
						if (string.IsNullOrEmpty (PlacasAux)) {
							if (App.LocalDataMan.ExistCar (placas)) {
								var canUpdateCar = await DisplayAlert ("Actualizar auto", "El auto con las placas " + placas + " ya existe. \n¿Desea actualizarlo con la información ingresada?", "Actualizar", "Cancelar");
								if (canUpdateCar) {
									actualizarAuto (placas, brand, modelo, color, detalles);
								}
							} else {
								if (App.LocalDataMan.CreateCar (carLocalCreated)) {
									PlacasAux = placas;
								}
							}
						} else {
							if (PlacasAux.Equals (placas)) {
								actualizarAuto (placas, brand, modelo, color, detalles);

//								var canUpdateCar = await DisplayAlert ("Actualizar auto", "El auto con las placas " + placas + " ya existe. \n¿Desea actualizarlo con la información ingresada?", "Actualizar", "Cancelar");
//								if (canUpdateCar) {
//								}
							} else {
								if (App.LocalDataMan.ExistCar (placas)) {
									var canUpdateCar = await DisplayAlert ("Actualizar auto", "El auto con las placas " + placas + " ya existe. \n¿Desea actualizarlo con la información ingresada?", "Actualizar", "Cancelar");
									if (canUpdateCar) {
										actualizarAuto (placas, brand, modelo, color, detalles);
									}
								} else {
									if (App.LocalDataMan.CreateCar (carLocalCreated)) {
										PlacasAux = placas;
									}
								}
							}
						}

						var carCreated = new CarModel {
							idBrand = int.Parse (brandId),
							idModel = int.Parse (modeloId),
							idColor = color,
							licensePlate = placas,
							details = detalles,
						};

						App.Order.clientCar = carCreated;
					}

					Navigation.PushAsync (new WashServicePage(size));
				} else {
					DisplayAlert ("", "Se debe ingresar toda la información del auto", "OK");
				}
			};

			MarcaPicker.SelectedIndexChanged += (sender, e) => {

				if (MarcaPicker.SelectedIndex >= 0) {
					Modelos.Clear();
					ModelosDicc.Clear();
					ModeloPicker.Items.Clear();

					var brandSelected = Brands [MarcaPicker.SelectedIndex];

					System.Diagnostics.Debug.WriteLine("::: MARCA: " + brandSelected.idBrand + " " + brandSelected.brand);

					foreach (var mod in brandSelected.modelo) {
						Modelos.Add(mod);
					}

					foreach(var mod in Modelos) {
						ModelosDicc.Add (mod.name, mod);
					}

					foreach(var modeloD in ModelosDicc) {
						ModeloPicker.Items.Add (modeloD.Key);
					}

					IsFromList = false;
				}
			};

			CarListButton.Clicked += (sender, e) => {
				IsFromList = true;
				this.Navigation.PushAsync (new ProfileCarsPage (true));
			};

			PlacasEntry.TextChanged += (sender, e) => IsFromList = false;
			ColorEntry.TextChanged += (sender, e) => IsFromList = false;
			InfoEditor.TextChanged += (sender, e) => IsFromList = false;

		}

		void actualizarAuto (string placas, string brand, string modelo, string color, string detalles)
		{
			var carUpdate = App.LocalDataMan.GetCar (placas);
			carUpdate.Marca = brand;
			carUpdate.Modelo = modelo;
			carUpdate.Color = color;
			carUpdate.Detalles = detalles;
			
			if (App.LocalDataMan.UpdateCar (carUpdate))
				PlacasAux = placas;			
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page CarServicePage");

			if (IsFromList) {
				var car = App.Order.clientCar;

				if (car != null) {
					ColorEntry.Text = car.idColor;
					PlacasEntry.Text = car.licensePlate;
					InfoEditor.Text = car.details;

					setMarcaPicker (car.idBrand.ToString());
					setModeloPicker (car.idModel.ToString());

					PlacasAux = car.licensePlate;
				}
			}
		}

		void setMarcaPicker (string idBrand)
		{
			BrandsDicc.Clear ();
			MarcaPicker.Items.Clear ();

			int i = 0;
			int indexBrandSel = 0;

			foreach(var brand in Brands) {
				BrandsDicc.Add (brand.brand, brand);
				if (brand.idBrand.Equals (idBrand))
					indexBrandSel = i;
				i++;
			}
			foreach(var brandD in BrandsDicc) {
				MarcaPicker.Items.Add (brandD.Key);
			}
			MarcaPicker.SelectedIndex = indexBrandSel;
		}

		void setModeloPicker (string idModelo)
		{			
			var brandSelected = Brands [MarcaPicker.SelectedIndex];
			int indexModeloSel = 0;
			int i = 0;

			foreach (var mod in brandSelected.modelo) {
				if (mod.idModel.Equals (idModelo))
					indexModeloSel = i;
				i++;
			}
			ModeloPicker.SelectedIndex = indexModeloSel;
		}
	}
}

