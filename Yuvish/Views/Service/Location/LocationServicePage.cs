﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xam.Plugin.MapExtend.Abstractions;
using System.Collections.Generic;

namespace Yuvish
{
	public class LocationServicePage : ContentPage
	{
		List<PlaceModel> Places;

		Button OpenMenuImage;
		Image LogoImage;
		Button OKImagen;

		EntryCustom AddressEntry;
		Button AddressSearchButton;

		Button SetIconAddressButton;
		Image YuvishIconImage;

		MapCustom ServiceMap;
		Image CurrentLocationImage;

		MapExtend Map;

		ContentView SearchView;
		ListView SearchPlacesListView;

		MenuView MenuView;

		StackLayout BarStack;
		StackLayout SearchStack;
		StepsView StepsView;
		RelativeLayout Relative;

		TapGestureRecognizer TapGestureCurrentLocation = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureLocationService = new TapGestureRecognizer();

		public LocationServicePage ()
		{
			Title = "Yuvish";

			getPlaces ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setBindings ();
			setListeners ();
			setMessaging ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void getPlaces ()
		{
			Places = App.LocalDataMan.GetPlaces ();
		}

		void createElements ()
		{
			OpenMenuImage = new Button {
				Image = "menu_dark.png",
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			LogoImage = new Image {
				Source = ImageSource.FromResource("logo.png"),
				Scale = 1.2f,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			OKImagen = new Button {
				Image = "continuar_dark.png",
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			AddressEntry = new EntryCustom {
				Placeholder = "Ingresa tu dirección",
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Entry)),
				TextColor = ColorResource.Pantone446c,
				BackgroundColor = Color.White,	
				IsNoBorder = true,
				BorderRadius = 0,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			AddressSearchButton = new Button {
				Text = "Buscar",
				Font = Font.OfSize(Fonts.AvenirNext_D, NamedSize.Small),
				TextColor = Color.White,
				BackgroundColor = ColorResource.Pantone3252c,
				BorderRadius = 0,
				WidthRequest = 80,
				HeightRequest = 40,
			};

			YuvishIconImage = new Image {
				Source = ImageSource.FromResource ("ic_pin.png"),	
				WidthRequest = 40,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				GestureRecognizers = {
					TapGestureLocationService
				}
			};

			SetIconAddressButton = new Button {
				Text = "Usar ubicación de pin",
				TextColor = Color.White,
				BackgroundColor = ColorResource.Pantone445c,
				BorderRadius = 15,
				WidthRequest = 160,
				HeightRequest = 30,
			};

			ServiceMap = new MapCustom {
				HasScrollEnabled = true,
				HasZoomEnabled = true,
				IsShowingUser = true,
				MapType = MapType.Street,
				WidthRequest = App.ScreenWidth,
				HeightRequest = App.ScreenHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			ServiceMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position (19.4326, -99.1332), Distance.FromKilometers(0.24)));

			var positionCurrent = DependencyService.Get<ILocation> ().GetCurrentLocation ();
			if (positionCurrent != null) {
				ServiceMap.MoveToRegion(MapSpan.FromCenterAndRadius(positionCurrent, Distance.FromKilometers(0.18)));
			}

			Map = new MapExtend {
				HasScrollEnabled = true,
				HasZoomEnabled = true,
				IsShowingUser = true,
				MapType = MapType.Street,
				WidthRequest = App.ScreenWidth,
				HeightRequest = App.ScreenHeight,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			CurrentLocationImage = new Image {
				Source = "ic_current_location.png",
				GestureRecognizers = {
					TapGestureCurrentLocation
				}
			};

			MenuView = new MenuView();

			SearchPlacesListView = new ListView {
				ItemsSource = Places,
				ItemTemplate = new DataTemplate(typeof(PlaceLocationServiceTemplate)),
				HasUnevenRows = true,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createLayouts ()
		{			
			StepsView = new StepsView ();

			BarStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(20, 10),
				BackgroundColor = Color.Transparent,
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			SearchStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = 0,
				//Padding = new Thickness(20, 0),
				//WidthRequest = App.ScreenWidth - 40,
				WidthRequest = App.ScreenWidth - 80,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};

			SearchView = new ContentView {
				IsVisible = false,
				BackgroundColor = ColorResource.PantoneBlackC5,
				WidthRequest = App.ScreenWidth - 20,
				HeightRequest = App.ScreenHeight - 240,
			};

			Relative = new RelativeLayout ();
		}

		void setBindings ()
		{
			//SearchPlacesListView.SetBinding (ListView.ItemsSourceProperty, new Binding ("Places", BindingMode.TwoWay));
		}

		void setLayouts ()
		{			
			BarStack.Children.Add (OpenMenuImage);
			BarStack.Children.Add (LogoImage);
			BarStack.Children.Add (OKImagen);

			SearchStack.Children.Add (AddressEntry);
			SearchStack.Children.Add (AddressSearchButton);

			var searchRelative = new RelativeLayout ();

			searchRelative.Children.Add (
				new ContentViewCustom {
					WidthRequest = 40,
					HeightRequest = 40,
					Radius = 20,
					BackgroundColor = Color.White,
				},
				Constraint.Constant(20),
				Constraint.Constant(0)
			);
			searchRelative.Children.Add (
				new ContentViewCustom {
					WidthRequest = 40,
					HeightRequest = 40,
					Radius = 20,
					BackgroundColor = ColorResource.Pantone3252c,
				},
				Constraint.Constant(SearchStack.WidthRequest + 20),
				Constraint.Constant(0)
			);
			searchRelative.Children.Add (SearchStack,
				Constraint.Constant(40),
				Constraint.Constant(0)
			);

			SearchView.Content = SearchPlacesListView;

			Relative.Children.Add (ServiceMap,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (CurrentLocationImage,
				Constraint.Constant(App.ScreenWidth - 70),
				Constraint.Constant(App.ScreenHeight - 150)
			);
			/*
			Relative.Children.Add (SetIconAddressButton,
				Constraint.Constant((App.ScreenWidth / 2) - (SetIconAddressButton.WidthRequest / 2)),
				Constraint.Constant(((App.ScreenHeight / 2) - (YuvishIconImage.HeightRequest)) - 25)
			);*/
			Relative.Children.Add (YuvishIconImage,
				Constraint.Constant((App.ScreenWidth / 2) - (YuvishIconImage.WidthRequest / 2)),
				Constraint.Constant((App.ScreenHeight / 2) - (YuvishIconImage.HeightRequest))
			);
			Relative.Children.Add (
				new ContentView {
					Opacity = 0.6,
					BackgroundColor = Color.White,
					WidthRequest = App.ScreenWidth,
					HeightRequest = 60,
				},
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (BarStack,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (searchRelative, Constraint.Constant(0), Constraint.Constant(70));
			Relative.Children.Add (StepsView,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height - StepsView.HeightRequest - 20; })
			);
			Relative.Children.Add (MenuView, Constraint.Constant(-App.ScreenWidth), Constraint.Constant(0));
			Relative.Children.Add (SearchView, Constraint.Constant(10), Constraint.Constant(120));
		}

		void setListeners ()
		{
			OpenMenuImage.Clicked += (sender, e) => {				
				MenuView.TranslateTo(App.ScreenWidth, 0, easing: Easing.CubicOut);
			};

			OKImagen.Clicked += async (sender, e) => { 
				var posCenter = ServiceMap.VisibleRegion.Center;
				System.Diagnostics.Debug.WriteLine ("Center position: " + posCenter.Latitude + ", " + posCenter.Longitude);

				var possibleAddresses = await new Geocoder ().GetAddressesForPositionAsync (posCenter);	
				string address = "";
				foreach (var addr in possibleAddresses) {
					System.Diagnostics.Debug.WriteLine ("Position to Address: " + addr);
					address = addr;
				}

				if (App.WSRest.ValidateCoverageArea (posCenter.Latitude, posCenter.Longitude)) {

					if (!App.LocalDataMan.ExistPlace(posCenter.Latitude, posCenter.Longitude))
					{
						var placeCreated = new PlaceModel ()
						{
							Nombre = "Ubicación sin titulo",
							Direccion = address,
							Latitud = posCenter.Latitude,
							Longitud = posCenter.Longitude
						};

						App.LocalDataMan.CreatePlace(placeCreated);
					}

					App.Order.lat = posCenter.Latitude;
					App.Order.lon = posCenter.Longitude;
					App.Order.address = address;

					Navigation.PushAsync (new CalendarServicePage ());
				} else {
					DisplayAlert ("", "No se encuentra dentro del área de cobertura del servicio", "OK");
				}
			};

			AddressEntry.Focused += (sender, e) => {
				if (Places.Count > 0) {
					SearchView.IsVisible = true;
					ServiceMap.Opacity = 0.6f;
					StepsView.Opacity = 0.6f;
				}
			};

			AddressEntry.Unfocused += (sender, e) => {
				SearchView.IsVisible = false;
				ServiceMap.Opacity = 1;
				StepsView.Opacity = 1;
			};

			AddressSearchButton.Clicked += async (sender, e) => {
				AddressEntry.Unfocus();
				if (!string.IsNullOrEmpty(AddressEntry.Text)) {
					await Map.SearchAdress (AddressEntry.Text);
					System.Diagnostics.Debug.WriteLine (Map.Pins.Count + " Pins");
					if (Map.Pins.Count > 0) {
						var pin = Map.Pins[Map.Pins.Count - 1];
						System.Diagnostics.Debug.WriteLine ("Pin position: " + pin.Position.Latitude + ", " + pin.Position.Longitude);

						ServiceMap.MoveToRegion(MapSpan.FromCenterAndRadius(pin.Position, Distance.FromKilometers(0.24)));
					}
				}
			};

			SearchPlacesListView.ItemSelected += (sender, e) => {
				if (e.SelectedItem == null)
					return;
				var item = e.SelectedItem as PlaceModel;

				ServiceMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(item.Latitud, item.Longitud), Distance.FromKilometers(0.18)));

				SearchPlacesListView.SelectedItem = null;

				SearchView.IsVisible = false;
				ServiceMap.Opacity = 1;
				StepsView.Opacity = 1;
			};

			TapGestureCurrentLocation.Tapped += (sender, e) => {
				var position = DependencyService.Get<ILocation>().GetCurrentLocation();
				ServiceMap.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromKilometers(0.18)));
			};
			SetIconAddressButton.Clicked += async (sender, e) => {
				var posCenter = ServiceMap.VisibleRegion.Center;
				var possibleAddresses = await new Geocoder ().GetAddressesForPositionAsync (posCenter);	
				string address = "";
				foreach (var addr in possibleAddresses) {
					System.Diagnostics.Debug.WriteLine ("Position to Address: " + addr);
					address = addr;
				}
				AddressEntry.Text = address;
			};
				
			/*
			TapGestureLocationService.Tapped += async (sender, e) => {
				var posCenter = ServiceMap.VisibleRegion.Center;
				var possibleAddresses = await new Geocoder ().GetAddressesForPositionAsync (posCenter);	
				string address = "";
				foreach (var addr in possibleAddresses) {
					System.Diagnostics.Debug.WriteLine ("Position to Address: " + addr);
					address = addr;
				}
				AddressEntry.Text = address;
			};
			*/

		}

		void setMessaging ()
		{
			MessagingCenter.Subscribe<PayServicePage> (this, "OrderSent", (sender) => {			
				Navigation.PushModalAsync (new NavigationPage(new LocationServicePage()));	
				//Navigation.PushModalAsync (new LocationServicePage(), false);
				DisplayAlert ("Solicitud enviada", "En breve recibirás un correo de confirmación", "OK");
			});
		}

		protected override void OnAppearing ()
		{			
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page LocationServicePage");

			MenuView.deselectCells ();
			getPlaces ();
			SearchPlacesListView.ItemsSource = Places;
		}

	}
}

