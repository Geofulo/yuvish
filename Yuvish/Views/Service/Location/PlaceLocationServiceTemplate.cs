﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class PlaceLocationServiceTemplate : ViewCell
	{
		Label NameLabel;
		Label AddressLabel;

		StackLayout Stack;

		public PlaceLocationServiceTemplate ()
		{
			createELements ();
			createLayouts ();
			setLayouts ();
			setBindings ();

			View = Stack;
		}

		void createELements ()
		{
			NameLabel = new Label {
				Text = "Ubicacion sin titulo",
				Font = Font.OfSize(Fonts.AvenirNext, NamedSize.Medium),
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorResource.Pantone446c,
			};

			AddressLabel = new Label {
				Font = Font.OfSize(Fonts.AvenirNext, NamedSize.Small),
				TextColor = ColorResource.Pantone446c,
			};
		}

		void createLayouts ()
		{
			Stack = new StackLayout {
				Padding = new Thickness (10),
				Spacing = 10,
			};				
		}

		void setLayouts ()
		{
			Stack.Children.Add (NameLabel);
			Stack.Children.Add (AddressLabel);
		}

		void setBindings ()
		{			
			NameLabel.SetBinding(Label.TextProperty, "Nombre");
			AddressLabel.SetBinding(Label.TextProperty, "Direccion");
		}
	}
}

