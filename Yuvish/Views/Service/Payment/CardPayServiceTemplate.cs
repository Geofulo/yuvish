﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class CardPayServiceTemplate : ViewCell
	{		
		Label BankNameLabel;
		Label CreditCardLabel;
		Image BankImage;

		public CardPayServiceTemplate ()
		{
			createELements ();
			setBindings ();

			View = new ContentView {
				BackgroundColor = Color.Transparent,
				Padding = new Thickness (20, 10),
				Content = new StackLayout { 
					Spacing = 20,
					Orientation = StackOrientation.Horizontal,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					Children = { 
						BankNameLabel,
						CreditCardLabel,
						BankImage
					}
				}
			};
		}

		void createELements ()
		{
			BankNameLabel = new Label {
				FontFamily = Fonts.AvenirNext_M,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				TextColor = ColorResource.Pantone445c,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			CreditCardLabel = new Label {
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				TextColor = Color.Gray,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			BankImage = new Image {
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void setBindings ()
		{
			BankNameLabel.SetBinding (Label.TextProperty, "BankName");
			CreditCardLabel.SetBinding (Label.TextProperty, "Card");
			BankImage.SetBinding (Image.SourceProperty, "CreditCardImage");
		}

		protected override void OnTapped ()
		{
			base.OnTapped ();

			//CreditCardLabel.TextColor = ColorResource.Pantone445c;
		}

	}
}

