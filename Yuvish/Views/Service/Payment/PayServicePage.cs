﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class PayServicePage : ContentPage
	{
		List<CreditCardModel> Cards;
		string NameService;
		string PriceService;

		Button BackMenuImage;
		Image LogoImage;

		EntryCustom CodigoDescuentoEntry;

		Button AddTDCButton;
		ListView CardsListView;
		//Button TDCButton;
		Button FinishOrderButton;

		StackLayout BarStack;
		StepsView StepsView;

		StackLayout Stack;
		ScrollView Scroll;
		RelativeLayout Relative;

		public PayServicePage (string NameService, string PriceService)
		{
			this.NameService = NameService;
			this.PriceService = PriceService;

			Title = "Yuvish";

			getCards ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void getCards ()
		{
			Cards = App.LocalDataMan.GetCreditCards ();
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			LogoImage = new Image {
				Source = ImageSource.FromResource("logo.png"),
				Scale = 1.2f,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			CodigoDescuentoEntry = new EntryCustom {
				Placeholder = "¿Tienes un código de descuento?",
				FontFamily = Fonts.Caslon,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Entry)),
				TextColor = ColorResource.Pantone445c,
				IsNoBorder = true,
				IsEnabled = false,
				BorderRadius = 0,
				BackgroundColor = Color.Transparent,
				HorizontalTextAlignment = TextAlignment.End,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			AddTDCButton = new Button {
				Text = "Ingresa Tarjeta de Crédito / Débito",
				Font = Font.OfSize (Fonts.AvenirNext, NamedSize.Medium),
				TextColor = ColorResource.Pantone446c,
				BackgroundColor = Color.Transparent,
				BorderWidth = 0,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			CardsListView = new ListView {
				ItemsSource = Cards,
				ItemTemplate = new DataTemplate(typeof(CardPayServiceTemplate)),
				BackgroundColor = Color.Transparent,
				RowHeight = 40,
				HeightRequest = 40 * Cards.Count,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			if (Cards.Count == 0) {
				CardsListView.IsVisible = false;
			}

			FinishOrderButton = new Button {
				Text = "Solicitar servicio",
				Font = Font.OfSize (Fonts.AvenirNext_D, NamedSize.Medium),
				TextColor = ColorResource.Pantone446c,
				BackgroundColor = ColorResource.Pantone3252c,
				BorderWidth = 0,
				BorderRadius = 20,
				//WidthRequest = App.ScreenWidth / 2 - 20,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness (20, 10, 40, 10),
				WidthRequest = App.ScreenWidth - 60,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			StepsView = new StepsView ();
			StepsView.OneStepCompleted ();
			StepsView.TwoStepCompleted ();
			StepsView.ThreeStepCompleted ();
			StepsView.FourStepCompleted ();

			Stack = new StackLayout {
				Spacing = 20,
				WidthRequest = App.ScreenWidth,
			};

			Scroll = new ScrollView {
				HeightRequest = App.ScreenHeight - 70 - StepsView.HeightRequest - 40,
			};

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (LogoImage);

			var car = App.LocalDataMan.GetCar (App.Order.clientCar.licensePlate);
			var service = App.Order.services [0];

			Stack.Children.Add (new ContentView {
				Padding = new Thickness(20, 0),
				Content = new Label {
					Text = "Resumen de la orden",
					FontFamily = Fonts.AvenirNext_D,
					FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
					FontAttributes = FontAttributes.Bold,
					TextColor = ColorResource.Pantone446c,
				}
			});
			Stack.Children.Add (new BoxView {
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});
			Stack.Children.Add(new StackLayout { 
				Padding = new Thickness(15, 0),
				Spacing = 20,
				Children = { 
					new Label { 
						Text = "Verifica los datos y solicita el servicio, recuerda que el cargo a tu tarjeta de crédito/débito no se aplicará sino hasta que el servicio sea realizado.",
						FontFamily = Fonts.AvenirNext,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = Color.Gray,
						HorizontalTextAlignment = TextAlignment.Center,
					},
					new Frame {
						BackgroundColor = Color.White,
						HasShadow = false,
						OutlineColor = ColorResource.Pantone3252c,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Content = new StackLayout {
							Spacing = 20,
							Children = {
								new StackLayout {
									Orientation = StackOrientation.Horizontal,
									//Padding = new Thickness (5, 0),
									Children = {
										new Label {
											Text = NameService,
											FontFamily = Fonts.AvenirNext,
											FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
											TextColor = ColorResource.Pantone445c,
											HorizontalTextAlignment = TextAlignment.Start,
											HorizontalOptions = LayoutOptions.StartAndExpand,
											VerticalOptions = LayoutOptions.CenterAndExpand,
										},
										new Label {
											Text = "$" + PriceService,
											FontFamily = Fonts.AvenirNext,
											FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
											TextColor = Color.Gray,
											HorizontalTextAlignment = TextAlignment.End,
											HorizontalOptions = LayoutOptions.EndAndExpand,
											VerticalOptions = LayoutOptions.CenterAndExpand,
										}
									}
								},
								new BoxView {
									BackgroundColor = ColorResource.Pantone445c,
									WidthRequest = App.ScreenWidth - 40,
									HeightRequest = 1,
									HorizontalOptions = LayoutOptions.CenterAndExpand,
								},
								new StackLayout {
									Spacing = 20,
									Children = {
										new StackLayout {
											Orientation = StackOrientation.Horizontal,
											Children = {
												new Image {
													Source = "ic_ubica_dark.png",
													HeightRequest = 30,
													HorizontalOptions = LayoutOptions.Start,
													VerticalOptions = LayoutOptions.CenterAndExpand
												},
												new Label {
													Text = App.Order.address,
													FontFamily = Fonts.AvenirNext,
													FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
													TextColor = ColorResource.Pantone445c,
													HorizontalTextAlignment = TextAlignment.End,
													HorizontalOptions = LayoutOptions.EndAndExpand,
													VerticalOptions = LayoutOptions.CenterAndExpand
												}
											}
										},
										new StackLayout {
											Orientation = StackOrientation.Horizontal,
											Children = {
												new Image {
													Source = "ic_auto_dark.png",
													HeightRequest = 30,
													HorizontalOptions = LayoutOptions.Start,
													VerticalOptions = LayoutOptions.CenterAndExpand,
												},
												new Label {
													Text = car.Marca + " / " + car.Modelo + " / " + car.Placas,
													FontFamily = Fonts.AvenirNext,
													FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
													TextColor = ColorResource.Pantone445c,
													HorizontalTextAlignment = TextAlignment.End,
													HorizontalOptions = LayoutOptions.EndAndExpand,
													VerticalOptions = LayoutOptions.CenterAndExpand
												}
											}
										},
										new StackLayout {
											Orientation = StackOrientation.Horizontal,
											Children = {
												new Image {
													Source = "ic_agenda_dark.png",
													HeightRequest = 30,
													HorizontalOptions = LayoutOptions.Start,
													VerticalOptions = LayoutOptions.CenterAndExpand
												},
												new Label {
													Text = App.Order.datetime,
													FontFamily = Fonts.AvenirNext,
													FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
													TextColor = ColorResource.Pantone445c,
													HorizontalTextAlignment = TextAlignment.End,
													HorizontalOptions = LayoutOptions.EndAndExpand,
													VerticalOptions = LayoutOptions.CenterAndExpand
												}
											}
										},
										new BoxView {
											BackgroundColor = ColorResource.Pantone445c,
											WidthRequest = App.ScreenWidth - 40,
											HeightRequest = 1,
											HorizontalOptions = LayoutOptions.CenterAndExpand,
										},
										new StackLayout {
											Orientation = StackOrientation.Horizontal,
											Padding = new Thickness (20, 0),
											Children = {
												CodigoDescuentoEntry,
											}
										},
										new BoxView {
											BackgroundColor = ColorResource.Pantone445c,
											WidthRequest = App.ScreenWidth - 40,
											HeightRequest = 1,
											HorizontalOptions = LayoutOptions.CenterAndExpand,
										},
										new StackLayout {
											Orientation = StackOrientation.Horizontal,
											Children = {
												new Label {
													Text = "Total",
													FontFamily = Fonts.AvenirNext_D,
													FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
													TextColor = Color.Gray,
													HorizontalTextAlignment = TextAlignment.Start,
													HorizontalOptions = LayoutOptions.StartAndExpand,
													VerticalOptions = LayoutOptions.CenterAndExpand,
												},
												new Label {
													Text = "$" + PriceService,
													FontFamily = Fonts.AvenirNext_D,
													FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
													TextColor = ColorResource.Pantone3252c,
													HorizontalTextAlignment = TextAlignment.End,
													HorizontalOptions = LayoutOptions.EndAndExpand,
													VerticalOptions = LayoutOptions.CenterAndExpand,
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});

			Stack.Children.Add (AddTDCButton);

			Stack.Children.Add (CardsListView);

			Stack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness (20, 0),
				Children = {
					FinishOrderButton,
				}
			});

			Scroll.Content = Stack;

			Relative.Children.Add (BarStack,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (Scroll,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
			Relative.Children.Add (StepsView,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height - StepsView.HeightRequest - 20; })
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync();

			AddTDCButton.Clicked += (sender, e) => {
				Navigation.PushAsync (new CreateTDCPage(true));
			};

			FinishOrderButton.Clicked += async (sender, e) => {
				if (CardsListView.SelectedItem != null) {
					var card = CardsListView.SelectedItem as CreditCardModel;

					var isTokenValid = await DependencyService.Get<IStripe>().GetToken (card.Number, card.CVC, card.ExpYear, card.ExpMonth);
					if (isTokenValid) {
						App.Order.token = App.UserToken;

						var sendCorrect = App.WSRest.SendOrder (App.Order);
						if (sendCorrect) {
							MessagingCenter.Send<PayServicePage>(this, "OrderSent");
						} else {
							DisplayAlert ("", "Ocurrió un error al intentar solicitar el servicio", "OK");
						}
					}	
				} else {
					DisplayAlert ("", "Debes seleccionar una tarjeta ", "OK");
				}
			};
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page PayServicePage");

			getCards ();
			if (Cards.Count > 0) {
				CardsListView.IsVisible = true;
				CardsListView.ItemsSource = Cards;
				CardsListView.HeightRequest = 40 * Cards.Count;
			} else {
				CardsListView.IsVisible = false;
			}
		}
	}
}

