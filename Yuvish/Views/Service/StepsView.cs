﻿using System;
using Xamarin.Forms;

namespace Yuvish
{
	public class StepsView : RelativeLayout
	{
		public int StepsCompleted = 0;

		ContentViewCustom OneStepView;
		ContentViewCustom TwoStepView;
		ContentViewCustom ThreeStepView;
		ContentViewCustom FourStepView;
		ContentViewCustom FiveStepView;

		ContentView StepsContent;

		public StepsView ()
		{
			createElements ();
			createLayouts ();
			setLayouts ();

			HeightRequest = 44;
			BackgroundColor = Color.Transparent;

			setSuperLayouts ();
		}

		void createElements ()
		{
			OneStepView = new ContentViewCustom {
				BackgroundColor = ColorResource.Pantone3252c,
				Radius = 22,
				WidthRequest = 44,
				HeightRequest = 44,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			TwoStepView = new ContentViewCustom {
				BackgroundColor = ColorResource.Pantone3252c,
				Radius = 22,
				WidthRequest = 44,
				HeightRequest = 44,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			ThreeStepView = new ContentViewCustom {
				BackgroundColor = ColorResource.Pantone3252c,
				Radius = 22,
				WidthRequest = 44,
				HeightRequest = 44,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			FourStepView = new ContentViewCustom {
				BackgroundColor = ColorResource.Pantone3252c,
				Radius = 22,
				WidthRequest = 44,
				HeightRequest = 44,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
			FiveStepView = new ContentViewCustom {
				BackgroundColor = ColorResource.Pantone3252c,
				Radius = 22,
				WidthRequest = 44,
				HeightRequest = 44,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};
		}

		void createLayouts ()
		{
			StepsContent = new ContentView {				
				Padding = new Thickness (10, 0, 10, 0),
				WidthRequest = App.ScreenWidth - 20,
				HeightRequest = 44,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void setLayouts ()
		{
			OneStepView.Content = new Image {
				Source = ImageSource.FromResource ("ic_localiza.png"),
				Scale = 0.5f,
			};
			TwoStepView.Content = new Image {
				Source = ImageSource.FromResource ("ic_agenda.png"),
				Scale = 0.5f,
			};
			ThreeStepView.Content = new Image {
				Source = ImageSource.FromResource ("ic_auto.png"),
				Scale = 0.7f,
			};
			FourStepView.Content = new Image {
				Source = ImageSource.FromResource ("ic_lava.png"),
				Scale = 0.5f,
			};
			FiveStepView.Content = new Image {
				Source = "ic_costo2.png",
				Scale = 0.5f,
			};

			StepsContent.Content = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Spacing = (App.ScreenWidth - (50*5) - 20) / 4,
				HorizontalOptions = LayoutOptions.Center,
				Children = {
					OneStepView,
					TwoStepView,
					ThreeStepView,
					FourStepView,
					FiveStepView
				}
			};
		}

		void setSuperLayouts()
		{
			Children.Clear ();
			Children.Add (
				new BoxView {
					BackgroundColor = ColorResource.Pantone3252c,
					WidthRequest = App.ScreenWidth,
					HeightRequest = 2,
					HorizontalOptions = LayoutOptions.FillAndExpand,
				},
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height - (StepsContent.HeightRequest / 2); })
			);
			if (StepsCompleted > 0) {
				var sizeLine = App.ScreenWidth / 6;
				Children.Add (
					new BoxView {
						BackgroundColor = ColorResource.Pantone446c,
						WidthRequest = sizeLine * StepsCompleted,
						HeightRequest = 2,
						HorizontalOptions = LayoutOptions.FillAndExpand,
					},
					Constraint.Constant(0),
					Constraint.RelativeToParent((parent) => { return parent.Height - (StepsContent.HeightRequest / 2); })
				);
			}
			Children.Add (StepsContent,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
		}

		public void OneStepCompleted () 
		{
			OneStepView.BackgroundColor = ColorResource.Pantone446c;
			OneStepView.Content = new Image {
				Source = ImageSource.FromResource ("ic_done.png"),
				Scale = 0.5f,
			};
			StepsCompleted = 1;
			setSuperLayouts ();
		}

		public void TwoStepCompleted () 
		{
			TwoStepView.BackgroundColor = ColorResource.Pantone446c;
			TwoStepView.Content = new Image {
				Source = ImageSource.FromResource ("ic_done.png"),
				Scale = 0.5f,
			};
			StepsCompleted = 2;
			setSuperLayouts ();
		}

		public void ThreeStepCompleted () 
		{
			ThreeStepView.BackgroundColor = ColorResource.Pantone446c;
			ThreeStepView.Content = new Image {
				Source = ImageSource.FromResource ("ic_done.png"),
				Scale = 0.5f,
			};
			StepsCompleted = 3;
			setSuperLayouts ();
		}

		public void FourStepCompleted () 
		{
			FourStepView.BackgroundColor = ColorResource.Pantone446c;
			FourStepView.Content = new Image {
				Source = ImageSource.FromResource ("ic_done.png"),
				Scale = 0.5f,
			};
			StepsCompleted = 4;
			setSuperLayouts ();
		}

	}
}

