﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace Yuvish
{
	public class WashServicePage : ContentPage
	{
		string CarSize;
		List<ServiceModel> Services;

		int IndexSelectedService = 0;

		Button BackMenuImage;
		Image LogoImage;
		Button OKImagen;

		ContentViewCustom Service1RadioButton;
		ContentViewCustom Service2RadioButton;
		ContentViewCustom Service3RadioButton;
		//List<ContentViewCustom> RadioButtons = new List<ContentViewCustom>();

		Label HelpService2;

		Label TotalPriceLabel;

		StackLayout BarStack;
		StackLayout WashServiceStack;
		StepsView StepsView;

		RelativeLayout Relative;

		//List<TapGestureRecognizer> TapGestures = new List<TapGestureRecognizer>();
		TapGestureRecognizer TapGesture = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureService1 = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureService2 = new TapGestureRecognizer();
		TapGestureRecognizer TapGestureService3 = new TapGestureRecognizer();

		static double _labelFontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label));

		public WashServicePage (string CarSize)
		{
			this.CarSize = CarSize;

			Title = "Yuvish";

			getServices ();
			createElements ();
			createLayouts ();
			setLayouts ();
			setListeners ();

			NavigationPage.SetHasNavigationBar (this, false);

			Padding = new Thickness (0, 20, 0, 0);
			BackgroundColor = ColorResource.PantoneBlackC5;
			Content = Relative;
		}

		void getServices ()
		{			
			Services = App.WSRest.GetServices (CarSize);
		}

		void createElements ()
		{
			BackMenuImage = new Button {
				Image = "back_dark.png",
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			LogoImage = new Image {
				Source = ImageSource.FromResource("logo.png"),
				Scale = 1.2f,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.Fill,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};
			OKImagen = new Button {
				Image = "continuar_dark.png",
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			/*
			foreach (var service in Services)
			{
				var tap = new TapGestureRecognizer();

				RadioButtons.Add(new ContentViewCustom
				{
					BackgroundColor = Color.White,
					Radius = 10,
					WidthRequest = 20,
					HeightRequest = 20,
					VerticalOptions = LayoutOptions.CenterAndExpand,
					HorizontalOptions = LayoutOptions.Start,
					GestureRecognizers = {
						tap
					},
					Content = new ContentViewCustom
					{
						BackgroundColor = ColorResource.Pantone3252c,
						Radius = 5,
						WidthRequest = 10,
						HeightRequest = 10,
						HorizontalOptions = LayoutOptions.CenterAndExpand,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					}
				});
				TapGestures.Add(tap);
			}
			*/


			Service1RadioButton = new ContentViewCustom {
				BackgroundColor = Color.White,
				Radius = 10,
				WidthRequest = 20,
				HeightRequest = 20,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
				Content = new ContentViewCustom {
					BackgroundColor = ColorResource.Pantone3252c,
					Radius = 5,
					WidthRequest = 10,
					HeightRequest = 10,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
				},
			};
			Service2RadioButton = new ContentViewCustom {
				BackgroundColor = Color.White,
				Radius = 10,
				WidthRequest = 20,
				HeightRequest = 20,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
				Content = new ContentViewCustom {
					BackgroundColor = ColorResource.Pantone3252c,
					Radius = 5,
					IsVisible = false,
					WidthRequest = 10,
					HeightRequest = 10,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
				}
			};
			Service3RadioButton = new ContentViewCustom {
				BackgroundColor = Color.White,
				Radius = 10,
				WidthRequest = 20,
				HeightRequest = 20,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.Start,
				Content = new ContentViewCustom {
					BackgroundColor = ColorResource.Pantone3252c,
					Radius = 5,
					IsVisible = false,
					WidthRequest = 10,
					HeightRequest = 10,
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
				}
			};


			TotalPriceLabel = new Label {
				Text = "$" + Services [0].precio,
				FontFamily = Fonts.AvenirNext_D,
				FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
				TextColor = ColorResource.Pantone3252c,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			HelpService2 = new Label {
				//Text = "Yuvish necesita tener acceso a tu auto. Tendrás un máximo de 15 minutos antes de que Yuvish se retire.",
				Text = "Para ésta opción Yuvish necesita tener acceso a tu auto. Una vez que lleguemos, dispondrás de 15 minutos para darnos acceso y aspirarlo antes de que nos retiremos.",
				FontFamily = Fonts.AvenirNext,
				FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
				TextColor = Color.Gray,
				IsVisible = false,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
		}

		void createLayouts ()
		{
			BarStack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness (20, 10),
				WidthRequest = App.ScreenWidth - 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			WashServiceStack = new StackLayout {
				Spacing = 15,
				WidthRequest = App.ScreenWidth,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			StepsView = new StepsView ();
			StepsView.OneStepCompleted ();
			StepsView.TwoStepCompleted ();
			StepsView.ThreeStepCompleted ();

			Relative = new RelativeLayout ();
		}

		void setLayouts ()
		{
			BarStack.Children.Add (BackMenuImage);
			BarStack.Children.Add (LogoImage);
			BarStack.Children.Add (OKImagen);

			WashServiceStack.Children.Add (new ContentView {
				Padding = new Thickness (20, 0, 0, 15),
				Content = new Label {
					Text = "Tipo de lavado",
					FontFamily = Fonts.AvenirNext_D,
					FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
					TextColor = ColorResource.Pantone446c,
				}
			});
			WashServiceStack.Children.Add (new BoxView {
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});

			/*
			var stackAux = new StackLayout
			{
				Spacing = 15,
				Padding = new Thickness(15, 0, 15, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Fill,
			};

			int index = 0;
			foreach (var service in Services)
			{
				stackAux.Children.Add(new StackLayout {
					Orientation = StackOrientation.Horizontal,
					Spacing = 15,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					Children = {
						RadioButtons[index],
						new Label {
							Text = service.nombre,
							FontSize = _labelFontSize,
							TextColor = ColorResource.Pantone446c,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							VerticalOptions = LayoutOptions.CenterAndExpand,
						},
						new Label {
							//Text = "$" + PriceService1,
							Text = "$" + service.precio,
							FontSize = _labelFontSize,
							TextColor = Color.Gray,
							HorizontalTextAlignment = TextAlignment.End,
							HorizontalOptions = LayoutOptions.End,
							VerticalOptions = LayoutOptions.CenterAndExpand,
						}
					}
				});
				index++;
			}
			*/


			WashServiceStack.Children.Add (new StackLayout {
				Spacing = 15,
				Padding = new Thickness(15, 0, 15, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Fill,
				Children = {
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Spacing = 15,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							Service1RadioButton,
							new Label {
								Text = Services[0].nombre,
								//Text = "Lavado",
								FontFamily = Fonts.AvenirNext,
								FontSize = _labelFontSize,
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							new Label {
								//Text = "$" + PriceService1,
								Text = "$" + Services[0].precio,
								FontFamily = Fonts.AvenirNext_D,
								FontSize = _labelFontSize,
								TextColor = Color.Gray,
								HorizontalTextAlignment = TextAlignment.End,
								HorizontalOptions = LayoutOptions.End,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							}
						},
						GestureRecognizers = {
							TapGestureService1
						}
					},
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Spacing = 15,
						Children = {
							Service2RadioButton,
							new Label {
								Text = Services[1].nombre,
								FontFamily = Fonts.AvenirNext,
								FontSize = _labelFontSize,
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							new Label {
								Text = "$" + Services[1].precio,
								FontFamily = Fonts.AvenirNext_D,
								FontSize = _labelFontSize,
								TextColor = Color.Gray,
								HorizontalTextAlignment = TextAlignment.End,
								HorizontalOptions = LayoutOptions.End,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							}
						},
						GestureRecognizers = {
							TapGestureService2
						}
					},
					new StackLayout {
						Orientation = StackOrientation.Horizontal,
						Spacing = 15,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions = LayoutOptions.CenterAndExpand,
						Children = {
							Service3RadioButton,
							new Label {
								Text = Services[2].nombre,
								FontFamily = Fonts.AvenirNext,
								FontSize = _labelFontSize,
								TextColor = ColorResource.Pantone446c,
								HorizontalOptions = LayoutOptions.FillAndExpand,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							},
							new Label {
								Text = "$" + Services[2].precio,
								FontFamily = Fonts.AvenirNext_D,
								FontSize = _labelFontSize,
								TextColor = Color.Gray,
								HorizontalTextAlignment = TextAlignment.End,
								HorizontalOptions = LayoutOptions.End,
								VerticalOptions = LayoutOptions.CenterAndExpand,
							}
						},
						GestureRecognizers = {
							TapGestureService3
						}
					},
				}
			});

			WashServiceStack.Children.Add (new BoxView {
				BackgroundColor = ColorResource.Pantone445c,
				HeightRequest = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			});
			WashServiceStack.Children.Add (new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(70, 0, 30, 0),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Children = {
					new Label {
						Text = "SUBTOTAL",
						FontFamily = Fonts.AvenirNext_D,
						FontSize = Device.GetNamedSize (NamedSize.Small, typeof(Label)),
						TextColor = ColorResource.Pantone445c,
						VerticalOptions = LayoutOptions.CenterAndExpand,
					},
					TotalPriceLabel
				}
			});
			WashServiceStack.Children.Add(new StackLayout { 
				Padding = new Thickness(20, 5),
				Children = { 
					HelpService2
				}
			});

			Relative.Children.Add (BarStack,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);
			Relative.Children.Add (WashServiceStack,
				Constraint.Constant(0),
				Constraint.Constant(App.SpacingTopBar)
			);
			Relative.Children.Add (StepsView,
				Constraint.Constant(0),
				Constraint.RelativeToParent((parent) => { return parent.Height - StepsView.HeightRequest - 20; })
			);
		}

		void setListeners ()
		{
			BackMenuImage.Clicked += (sender, e) => Navigation.PopAsync();

			OKImagen.Clicked += (sender, e) => {
				App.Order.services = new int[] {IndexSelectedService + 1};
				var nameService = Services [IndexSelectedService].nombre;
				var priceService = Services [IndexSelectedService].precio;

				Navigation.PushAsync (new PayServicePage (nameService, priceService));
			};

			/*
			int index = 0;
			foreach (var tap in TapGestures)
			{
				tap.Tapped += (sender, e) => {
					RadioButtons[index].IsVisible = true;
					TotalPriceLabel.Text = "$" + Services [index].precio;
					IndexSelectedService = index;
				};

				index++;
			}
			*/

			TapGestureService1.Tapped += (sender, e) => {
				Service1RadioButton.Content.IsVisible = true;
				Service2RadioButton.Content.IsVisible = false;
				Service3RadioButton.Content.IsVisible = false;
				HelpService2.IsVisible = false;
				TotalPriceLabel.Text = "$" + Services [0].precio;
				IndexSelectedService = 0;
			};

			TapGestureService2.Tapped += (sender, e) => {
				Service2RadioButton.Content.IsVisible = true;
				Service1RadioButton.Content.IsVisible = false;
				Service3RadioButton.Content.IsVisible = false;
				HelpService2.IsVisible = true;
				TotalPriceLabel.Text = "$" + Services [1].precio;
				IndexSelectedService = 1;
			};

			TapGestureService3.Tapped += (sender, e) => {
				Service3RadioButton.Content.IsVisible = true;
				Service1RadioButton.Content.IsVisible = false;
				Service2RadioButton.Content.IsVisible = false;
				HelpService2.IsVisible = true;
				TotalPriceLabel.Text = "$" + Services [2].precio;
				IndexSelectedService = 2;
			};

			/*
			TapGesture.Tapped += (sender, e) => {
				if (sender.Equals(Service1RadioButton)) {
					Service1RadioButton.Content.IsVisible = true;
					Service2RadioButton.Content.IsVisible = false;
					Service3RadioButton.Content.IsVisible = false;
					HelpService2.IsVisible = false;
					TotalPriceLabel.Text = "$" + Services [0].precio;
					IndexSelectedService = 0;
				}
				if (sender.Equals(Service2RadioButton)) {
					Service2RadioButton.Content.IsVisible = true;
					Service1RadioButton.Content.IsVisible = false;
					Service3RadioButton.Content.IsVisible = false;
					HelpService2.IsVisible = true;
					TotalPriceLabel.Text = "$" + Services [1].precio;
					IndexSelectedService = 1;
				}
				if (sender.Equals(Service3RadioButton)) {
					Service3RadioButton.Content.IsVisible = true;
					Service1RadioButton.Content.IsVisible = false;
					Service2RadioButton.Content.IsVisible = false;
					HelpService2.IsVisible = true;
					TotalPriceLabel.Text = "$" + Services [2].precio;
					IndexSelectedService = 2;
				}
			};*/
				
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			System.Diagnostics.Debug.WriteLine ("In-Page WashServicePage");
		}
	}
}

