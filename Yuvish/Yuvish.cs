﻿using System;

using Xamarin.Forms;
using Yuvish.Helpers;

namespace Yuvish
{
	public class App : Application
	{
		static public int ScreenWidth;
		static public int ScreenHeight;
		static public int SpacingTopBar = 65;
		static public RestService WSRest;
		//static public string APP_FB_ID = "1664834120451605";
		//static public string APP_FB_ID = "693053097487716";
		static public string APP_FB_ID = "1700532536825905";
		static public UserModel CurrentUser;
		static public string ProfilePhoto;
		static public string UserToken;
		static public OrderModel2 Order = new OrderModel2 ();
		static public LocalDataManager LocalDataMan;
		static public Help Helper = new Help();

		public App ()
		{
			LocalDataMan = new LocalDataManager ();
			WSRest = new RestService ();

			UserToken = Settings.TokenLogin;

			if (!string.IsNullOrEmpty (UserToken)) {
				var isTokenValid = WSRest.ValidateToken ();

				if (isTokenValid) {
					CurrentUser = App.WSRest.GetMyInfo ();

					MainPage = new NavigationPage (new LocationServicePage ()) {
						BarBackgroundColor = ColorResource.PantoneBlackC5,
						BarTextColor = ColorResource.Pantone445c,
					};
				} else {
					MainPage = new NavigationPage(new LoginPage ());
				}
			} else {
				MainPage = new NavigationPage(new LoginPage ());
			}

			/*
			MainPage = new NavigationPage (new LocationServicePage()){
				BarBackgroundColor = ColorResource.PantoneBlackC5,
				BarTextColor = ColorResource.Pantone445c,
			};*/


		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

