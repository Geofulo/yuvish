﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Xamarin.Forms;

namespace Yuvish.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();
			Xamarin.Insights.Initialize ("993d46ff28aaae47bf6ba116c1c038f166dc04a9");
			Xamarin.FormsMaps.Init ();
			Xam.Plugin.MapExtend.iOSUnified.MapExtendRenderer.Init();

			App.ScreenWidth = (int)UIScreen.MainScreen.Bounds.Width;
			App.ScreenHeight = (int)UIScreen.MainScreen.Bounds.Height;
	
			DependencyService.Register<FacebookDependency> ();
			DependencyService.Register<LocationDependency> ();
			DependencyService.Register<SQLiteDependency> ();
			DependencyService.Register<StripeDependency> ();
			DependencyService.Register<PhoneDependency> ();
			DependencyService.Register<EmailDependency> ();
			DependencyService.Register<WebDependency> ();

			//Stripe.StripeClient.DefaultPublishableKey = "pk_test_8TyNDn5tVbygjP9rrKMVjA30";
			//Stripe.StripeClient.DefaultPublishableKey = "pk_test_FC0DEW6tqgTQFqf7F4797z3E";
			Stripe.StripeClient.DefaultPublishableKey = "pk_live_pYXOYjHlAUdjtsyTt71zolDb";

			LoadApplication (new App ());

			return base.FinishedLaunching (app, options);
		}
	}

}

