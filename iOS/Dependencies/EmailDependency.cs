﻿using System;
using Yuvish;
using MessageUI;
using UIKit;

[assembly: Xamarin.Forms.Dependency (typeof (IEmail))]

namespace Yuvish.iOS
{
	public class EmailDependency : IEmail
	{
		MFMailComposeViewController mailController;

		#region IEmail implementation
		public void SendEmail ()
		{
			var window = UIApplication.SharedApplication.KeyWindow;
			var vc = window.RootViewController;

			mailController = new MFMailComposeViewController ();

			if (MFMailComposeViewController.CanSendMail) {

				mailController.SetToRecipients (new string[]{"hola@yuvish.com"});
				mailController.SetSubject ("Solicitud de Soporte");

				mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
					Console.WriteLine (args.Result.ToString ());
					args.Controller.DismissViewController (true, null);
				};
					
				vc.PresentViewController (mailController, true, null);
			}
		}
		#endregion
		
	}
}

