﻿using System;
using Yuvish;
using System.Collections.Generic;
using UIKit;
using Foundation;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Auth;
using Yuvish.iOS;
using MonoTouch.NUnit.UI;

[assembly: Xamarin.Forms.Dependency (typeof (FacebookDependency))]

namespace Yuvish.iOS
{
	
	public class FacebookDependency : IFacebook
	{
		#region IFacebook implementation

		public void Login ()
		{
			var window = UIApplication.SharedApplication.KeyWindow;
			var vc = window.RootViewController;

			var auth = new OAuth2Authenticator (
				clientId: App.APP_FB_ID,
				scope: "public_profile,email",
				authorizeUrl: new Uri ("https://m.facebook.com/dialog/oauth/"),
				//redirectUrl: new Uri ("http://www.facebook.com/connect/login_success.html")
				//redirectUrl: new Uri ("http://rbenavides.com/yuvish-client-webapp/")
				redirectUrl: new Uri ("https://www.yuvish.com/")
				//redirectUrl: new Uri ("http://qa.yuvish.com/")

				//redirectUrl: new Uri ("www.facebook.com")
			);

			auth.Completed += (sender, eventArgs) => {
				// We presented the UI, so it's up to us to dimiss it on iOS.

				//DismissViewController (true, null);

				if (eventArgs.IsAuthenticated) {
					// Use eventArgs.Account to do wonderful things

					string accessToken = eventArgs.Account.Properties["access_token"];

					Console.WriteLine ("AUTH COMPLETED: " + eventArgs.Account.ToString());
					Console.WriteLine ("AUTH COMPLETED TOKEN: " + accessToken);

					if (App.WSRest.LoginFacebook (accessToken)) {

						/*
						App.ProfilePhoto = jpgFilename;

						App.CurrentUser = new UserModel {
							firstName = profileName,
						};
						*/


						Xamarin.Forms.Application.Current.MainPage = new NavigationPage (new LocationServicePage()){
							BarBackgroundColor = ColorResource.PantoneBlackC5,
							BarTextColor = ColorResource.Pantone445c,
						};

						//vc.DismissViewController (true, null);

						DependencyService.Get<ILocation>().AskWhenInUseAuthorization ();
					}

				} else {
					// The user cancelled
					Console.WriteLine ("USER CANCEL AUTH");

					vc.DismissViewController (true, null);
				}


			};

			vc.PresentViewController (auth.GetUI (), true, null);
		}

		#endregion


		/*
		FacebookClient fbClient;
		//private const string ExtendedPermissions = "user_about_me,read_stream,publish_stream";
		private const string ExtendedPermissions = "public_profile,email,user_birthday";

		#region IFacebook implementation

		public void Login ()
		{
			var tcs = new TaskCompletionSource<bool> ();
			fbClient = new FacebookClient ();

			var parameters = new Dictionary<string, object>();
			parameters["client_id"] = App.APP_FB_ID;
			parameters ["authorize_uri"] = "https://m.facebook.com/dialog/oauth/";
			parameters["redirect_uri"] = "https://www.facebook.com/connect/login_success.html";
			//parameters["redirect_uri"] = "http://www.facebook.com/connect/login_success.html";
			parameters["response_type"] = "token";
			//parameters["display"] = "touch";

			if (!string.IsNullOrEmpty (ExtendedPermissions))
			{
				// A comma-delimited list of permissions
				parameters["scope"] = ExtendedPermissions;
			}

			var urlLogin = new NSUrl(fbClient.GetLoginUrl (parameters).AbsoluteUri);

			UIWebView web = new UIWebView (UIScreen.MainScreen.Bounds) {
				BackgroundColor = UIColor.White,
				AutoresizingMask = UIViewAutoresizing.All
			};					

			web.LoadFinished += (webview, e) =>  {
				//NetworkActivity = false;
				var wb = webview as UIWebView;
				FacebookOAuthResult oauthResult;
				if (!fbClient.TryParseOAuthCallbackUrl (new Uri (wb.Request.Url.ToString()), out oauthResult))
				{
					return;
				}

				if (oauthResult.IsSuccess)
				{
					// Facebook Granted Token
					var accessToken = oauthResult.AccessToken;
					//LoginSucceded(accessToken, dvc);


					Console.WriteLine ("FB accessToken: " + accessToken);

					var fb = new FacebookClient(accessToken);

					fbClient = new FacebookClient (accessToken);

					var res = fbClient.GetTaskAsync("me").Result;

					var result = (IDictionary<string, object>) res;

					var id = (string) result["id"];
					string profileName = (string) result["name"];

					Console.WriteLine ("FB RES: " + res);
					Console.WriteLine ("FB ID: " + id);
					Console.WriteLine ("FB USERNAME: " + profileName);

					string profilePictureUrl = string.Format("https://graph.facebook.com/{0}/picture?type={1}&access_token={2}", id, "square", accessToken);
					UIImage profileImage = UIImage.LoadFromData (NSData.FromUrl (new NSUrl (profilePictureUrl)));

					var documentsDirectory = Environment.GetFolderPath
						(Environment.SpecialFolder.Personal);
					string jpgFilename = System.IO.Path.Combine (documentsDirectory, "ProfilePhoto.jpg"); // hardcoded filename, overwritten each time
					NSData imgData = profileImage.AsJPEG();
					NSError err = null;
					if (imgData.Save(jpgFilename, false, out err)) {
						Console.WriteLine("saved as " + jpgFilename);
					} else {
						Console.WriteLine("NOT saved as " + jpgFilename + " because" + err.LocalizedDescription);
					}						

					var logged = App.WSRest.LoginFacebook (accessToken);
					if (logged) {
						App.ProfilePhoto = jpgFilename;

						App.CurrentUser = new UserModel {
							firstName = profileName,
						};

						DependencyService.Get<ILocation>().AskWhenInUseAuthorization ();

						Xamarin.Forms.Application.Current.MainPage = new NavigationPage (new LocationServicePage()){
							BarBackgroundColor = ColorResource.PantoneBlackC5,
							BarTextColor = ColorResource.Pantone445c,
						};
					}

				}
				else
				{
					tcs.SetResult(false);
					// user cancelled login
					//LoginSucceded(string.Empty, dvc);
				}

			};

			var window = UIApplication.SharedApplication.KeyWindow;
			var vc = window.RootViewController;

			vc.View.AutosizesSubviews = true;
			vc.View.AddSubview (web);

			web.LoadRequest (NSUrlRequest.FromUrl (urlLogin));

			//return tcs.Task.Result;
		}

		#endregion

		*/
		
	}
}

