﻿using System;
using Yuvish;
using CoreLocation;
using Xamarin.Forms.Maps;

[assembly: Xamarin.Forms.Dependency (typeof (ILocation))]

namespace Yuvish.iOS
{
	public class LocationDependency : ILocation
	{
		#region ILocation implementation

		CLLocationManager locationManager = new CLLocationManager();

		public bool AskWhenInUseAuthorization ()
		{
			//CLLocationManager locationManager = new CLLocationManager();
			locationManager = new CLLocationManager();
			locationManager.RequestWhenInUseAuthorization();

			return true;
		}

		public Position GetCurrentLocation ()
		{
			//CLLocationManager locationManager = new CLLocationManager();
			locationManager = new CLLocationManager();
			locationManager.RequestWhenInUseAuthorization();

			var loc = locationManager.Location;

			if (loc != null) {
				var coo = loc.Coordinate;
				return new Position (coo.Latitude, coo.Longitude);
			} else {
				locationManager.RequestWhenInUseAuthorization();
				return new Position (19.4326, -99.1332);
			}

		}

		#endregion

	}
}

