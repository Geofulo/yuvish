﻿using System;
using Yuvish;
using Foundation;
using UIKit;

[assembly: Xamarin.Forms.Dependency (typeof (IPhone))]

namespace Yuvish.iOS
{
	public class PhoneDependency : IPhone
	{
		#region IPhone implementation
		public void Call ()
		{
			var url = new NSUrl ("tel:" + "44556677");
			UIApplication.SharedApplication.OpenUrl (url);

			if (!UIApplication.SharedApplication.OpenUrl (url)) {
				var av = new UIAlertView ("Not supported",
					"Scheme 'tel:' is not supported on this device",
					null,
					"OK",
					null);
				av.Show ();
			};
		}
		#endregion
		
	}
}

