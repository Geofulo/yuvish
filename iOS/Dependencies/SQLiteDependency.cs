﻿using System;
using System.IO;
using Yuvish.iOS;

[assembly: Xamarin.Forms.Dependency (typeof (SQLiteDependency))]

namespace Yuvish.iOS
{
	public class SQLiteDependency : ISQLite
	{		
		public SQLiteDependency() {}

		public SQLite.SQLiteConnection GetConnection ()
		{
			var sqliteFilename = "yuvish.db3";
			string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
			string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
			var path = Path.Combine(libraryPath, sqliteFilename);
			// Create the connection
			var conn = new SQLite.SQLiteConnection(path);
			// Return the database connection
			return conn;

		}
	}
}

