﻿using System;
using Stripe;
using System.Threading.Tasks;
using Yuvish;

[assembly: Xamarin.Forms.Dependency (typeof (IStripe))]

namespace Yuvish.iOS
{
	public class StripeDependency : IStripe
	{
		public async Task<bool> GetToken (string creditCard, string cvc, string expYear, string expMonth)
		{
			try {
				var token = await StripeClient.CreateToken (new Card {
					Number = creditCard,
					ExpiryYear = int.Parse(expYear),
					ExpiryMonth = int.Parse(expMonth),
					CVC = cvc,
				});

				System.Diagnostics.Debug.WriteLine ("Stripe Token: " + token.Id);

				var last4 = creditCard.Substring (creditCard.Length - 4, 4);

				var day = token.Created.Day.ToString();
				var month = token.Created.Month.ToString();
				var year = token.Created.Year.ToString();
				var hours = token.Created.Hour.ToString();
				var minutes = token.Created.Minute.ToString();

				if (int.Parse(day) < 10)	day = "0" + day;
				if (int.Parse(month) < 10)	month = "0" + month;
				if (int.Parse(hours) < 10)	hours = "0" + hours;
				if (int.Parse(minutes) < 10)	minutes = "0" + minutes;

				var datetime = day + "/" + month + "/" + year + " " + hours + ":" + minutes;

				System.Diagnostics.Debug.WriteLine ("Stripe Datetime: " + datetime);

				App.Order.clientPayment = new PaymentModel {
					last4CardNum = last4,
					stripeToken = token.Id,
					datetime = datetime
				};

				return true;
			} catch (Exception e) {
				System.Diagnostics.Debug.WriteLine ("Stripe Error: " + e.Message);
				return false;
			}
				
		}
	}
}

