﻿using System;
using UIKit;
using Foundation;
using Yuvish;

[assembly: Xamarin.Forms.Dependency (typeof (IWeb))]

namespace Yuvish.iOS
{
	public class WebDependency :IWeb
	{
		#region IWeb implementation

		public void OpenUrl (string url)
		{
			UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
		}

		#endregion

	}
}

