﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Yuvish;
using Yuvish.iOS;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer(typeof(ContentViewCustom), typeof(ContentViewRender))]

namespace Yuvish.iOS
{
	public class ContentViewRender : ViewRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged (e);

			var obj = (ContentViewCustom) this.Element;
			if (Element == null)
				return;

			if (obj.Radius > 0) {
				this.BackgroundColor = obj.BackgroundColor.ToUIColor ();
				this.Layer.MasksToBounds = false;
				this.Layer.CornerRadius = obj.Radius;
				this.Layer.BorderWidth = 0.5f;
				this.Layer.BorderColor = obj.ColorBorder.ToCGColor();
			}	

			for (int i = 0; i < obj.BlurNumber; i++) {
				if (obj.IsDarkBlur) {
					var blurView = new UIVisualEffectView {
						Frame = new CGRect (0, 0, obj.WidthRequest, obj.HeightRequest),
						//AutosizesSubviews = true,
						Effect = UIBlurEffect.FromStyle (UIBlurEffectStyle.Dark),
					};
					var subviews = this.Subviews;
					this.Subviews.Initialize ();
					this.AddSubview (blurView);
					this.AddSubviews (subviews);
				}
				if (obj.IsLightBlur) {
					var blurView = new UIVisualEffectView {
						Frame = new CGRect (0, 0, obj.WidthRequest, obj.HeightRequest),
						//AutosizesSubviews = true,
						Effect = UIBlurEffect.FromStyle (UIBlurEffectStyle.Light),
					};
					var subviews = this.Subviews;
					this.Subviews.Initialize ();
					this.AddSubview (blurView);
					this.AddSubviews (subviews);
				}
			}


		}
	}
}

