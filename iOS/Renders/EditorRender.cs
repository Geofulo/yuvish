﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Yuvish;
using Yuvish.iOS;
using CoreAnimation;
using CoreGraphics;

[assembly: ExportRenderer (typeof (EditorCustom), typeof (EditorRender))]

namespace Yuvish.iOS
{
	public class EditorRender : EditorRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged (e);

			var obj = (EditorCustom) this.Element;

			if (Element == null)
				return;

			Control.Layer.BorderColor = Color.Transparent.ToCGColor ();
			Control.TextColor = ColorResource.Pantone446c.ToUIColor ();

		}
	}
}

