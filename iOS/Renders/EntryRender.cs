﻿using System;
using Xamarin.Forms;
using Yuvish;
using Yuvish.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using CoreAnimation;
using CoreGraphics;

[assembly: ExportRenderer (typeof (EntryCustom), typeof (EntryRender))]

namespace Yuvish.iOS
{
	public class EntryRender : EntryRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);

			var obj = (EntryCustom) this.Element;
			if (Element == null)
				return;			
			if (Control != null) {								

				Control.Layer.CornerRadius = obj.BorderRadius;

				if (obj.IsNoBorder)
					Control.BorderStyle = UIKit.UITextBorderStyle.None;

				if (obj.IsUnderline) {
					var layer = new CALayer ();
					layer.Frame = new CGRect (0, 38, obj.WidthUnderline, 1.5f);
					layer.BorderWidth = 1.5f;
					layer.BorderColor = ColorResource.Pantone445c.ToCGColor ();
					Control.Layer.AddSublayer (layer);

					Control.Layer.BorderColor = Color.Transparent.ToCGColor ();
					Control.TextColor = ColorResource.Pantone446c.ToUIColor ();
				}
			}
		}
	}
}

