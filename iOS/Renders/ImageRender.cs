﻿using System;
using Xamarin.Forms;
using Yuvish;
using Yuvish.iOS;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer (typeof (ImageCustom), typeof (ImageRender))]

namespace Yuvish.iOS
{
	public class ImageRender : ImageRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Image> e)
		{
			base.OnElementChanged (e);

			var obj = (ImageCustom) this.Element;

			if (obj == null || Control == null)
				return;

			if (obj.IsCircle) {
				var min = Math.Min(Element.Width, Element.Height);
				Control.Layer.CornerRadius = (float)(min / 2.0);
				Control.Layer.MasksToBounds = false;
				Control.ClipsToBounds = true;
			}

		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			var obj = (ImageCustom) this.Element;

			if (Control == null)
				return;

			if (e.PropertyName == VisualElement.HeightProperty.PropertyName ||
				e.PropertyName == VisualElement.WidthProperty.PropertyName) {
				DrawFill();
			}
			else
			{
				if (e.PropertyName == Image.IsLoadingProperty.PropertyName
					&& !Element.IsLoading && Control.Image != null)
				{
					DrawOther();
				}
			}
		}

		private void DrawOther()
		{
			int height;
			int width;

			switch (Element.Aspect)
			{
			case Aspect.AspectFill:
				height = (int)Control.Image.Size.Height;
				width = (int)Control.Image.Size.Width;
				height = MakeSquare(height, ref width);
				break;
			case Aspect.AspectFit:
				height = (int)Control.Image.Size.Height;
				width = (int)Control.Image.Size.Width;
				height = MakeSquare(height, ref width);
				break;
			default:
				throw new NotImplementedException();
			}

			UIImage image = Control.Image;
			var clipRect = new CGRect(0, 0, width, height);
			var scaled = image.Scale(new CGSize(width, height));
			UIGraphics.BeginImageContextWithOptions(new CGSize(width, height), false, 0f);
			UIBezierPath.FromRoundedRect(clipRect, Math.Max(width, height) / 2).AddClip();

			scaled.Draw(new CGRect(0, 0, scaled.Size.Width, scaled.Size.Height));
			UIImage final = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			Control.Image = final;
		}

		private void DrawFill()
		{
			double min = Math.Min(Element.Width, Element.Height);
			Control.Layer.CornerRadius = (float)(min / 2.0);
			Control.Layer.MasksToBounds = false;
			Control.ClipsToBounds = true;
		}

		private int MakeSquare(int height, ref int width)
		{
			if (height < width)
			{
				width = height;
			}
			else
			{
				height = width;
			}
			return height;
		}


	}
}

