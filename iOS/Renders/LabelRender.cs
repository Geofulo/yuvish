﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Yuvish;
using Yuvish.iOS;

[assembly: ExportRenderer (typeof (LabelCustom), typeof (LabelRender))]

namespace Yuvish.iOS
{
	public class LabelRender : LabelRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);

			var obj = (LabelCustom) this.Element;
			if (Element == null)
				return;
			if (Control != null)
			{
				Control.Layer.CornerRadius = obj.BorderRadius;

			}
		}
	}
}

