﻿using System;
using Yuvish;
using Yuvish.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Maps.iOS;
using MapKit;
using CoreLocation;

[assembly: ExportRenderer (typeof (MapCustom), typeof (MapRender))]

namespace Yuvish.iOS
{
	public class MapRender : MapRenderer
	{
		protected override void OnElementChanged (Xamarin.Forms.Platform.iOS.ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;

			CLLocationManager locationManager = new CLLocationManager();
			locationManager.RequestWhenInUseAuthorization();
			//locationManager.RequestAlwaysAuthorization();

			var nativeMap = Control as MKMapView;

			//nativeMap.ShowsUserLocation = true;

			if (locationManager.Location != null) {

				var coor = locationManager.Location.Coordinate;

				Console.WriteLine ("Lat: " + coor.Latitude);
				Console.WriteLine ("Lon: " + coor.Longitude);

				//nativeMap.CenterCoordinate = new CLLocationCoordinate2D (coor.Latitude, coor.Longitude);

				//nativeMap.SetCamera (MKMapCamera.CameraLookingAtCenterCoordinate (new CLLocationCoordinate2D (coor.Latitude, coor.Longitude), 1200, 0, 0), true);
			}

		}
			
		protected override void OnRegisterEffect (Xamarin.Forms.Platform.iOS.PlatformEffect effect)
		{
			base.OnRegisterEffect (effect);



		}

		public override void TouchesMoved (Foundation.NSSet touches, UIKit.UIEvent evt)
		{
			base.TouchesMoved (touches, evt);

			System.Diagnostics.Debug.WriteLine ("TouchesMoved");
		}			


	}
}

