﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Yuvish;
using Yuvish.iOS;
using CoreAnimation;
using CoreGraphics;
using UIKit;

[assembly: ExportRenderer (typeof (PickerCustom), typeof (PickerRender))]

namespace Yuvish.iOS
{
	public class PickerRender : PickerRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Picker> e)
		{
			base.OnElementChanged (e);

			var obj = (PickerCustom) this.Element;

			var layer = new CALayer ();
			layer.Frame = new CGRect (0, 38.5f, obj.WidthUnderline, 1.5f);
			layer.BorderWidth = 1.5f;
			layer.BorderColor = ColorResource.Pantone445c.ToCGColor ();
			Control.Layer.AddSublayer (layer);

			Control.Layer.BorderColor = Color.Transparent.ToCGColor ();
			Control.TextColor = ColorResource.Pantone446c.ToUIColor ();
			if (obj.TextColor != Color.Transparent)
				Control.TextColor = obj.TextColor.ToUIColor();

			if (obj.IsTextAligmentCenter)
				Control.TextAlignment = UIKit.UITextAlignment.Center;

			var picker = (UIPickerView)this.Control.InputView;
			picker.TintColor = Color.Transparent.ToUIColor();

			Control.Font = obj.Font.ToUIFont ();
		}

		protected override void OnElementPropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged (sender, e);

			var obj = (PickerCustom) this.Element;

			if (obj.TextColor != Color.Transparent)
				Control.TextColor = obj.TextColor.ToUIColor();			
		}
	}
}

