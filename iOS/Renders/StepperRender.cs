﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Yuvish;
using Yuvish.iOS;

[assembly: ExportRenderer (typeof (StepperCustom), typeof (StepperRender))]

namespace Yuvish.iOS
{
	public class StepperRender : StepperRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.Stepper> e)
		{
			base.OnElementChanged (e);

			var obj = (StepperCustom) this.Element;

			if (Element == null)
				return;			

			Control.TintColor = obj.TintColor.ToUIColor();
		}
	}
}

