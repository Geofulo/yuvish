﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using CoreGraphics;
using CoreAnimation;
using Yuvish;
using Yuvish.iOS;

[assembly: ExportRenderer (typeof (TimePickerCustom), typeof (TimePickerRender))]

namespace Yuvish.iOS
{
	public class TimePickerRender : TimePickerRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.TimePicker> e)
		{
			base.OnElementChanged (e);

			var obj = (TimePickerCustom) this.Element;

			var layer = new CALayer ();
			layer.Frame = new CGRect (0, 38, obj.WidthUnderline, 1.5f);
			layer.BorderWidth = 1.5f;
			layer.BorderColor = ColorResource.Pantone445c.ToCGColor ();
			Control.Layer.AddSublayer (layer);

			Control.Layer.BorderColor = Color.Transparent.ToCGColor ();
			Control.TextColor = ColorResource.Pantone446c.ToUIColor ();
			Control.TextAlignment = UIKit.UITextAlignment.Center;

		}


	}

}

