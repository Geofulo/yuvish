﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;
using Yuvish;
using Yuvish.iOS;

[assembly: ExportRenderer (typeof (ViewCellCustom), typeof (ViewCellRender))]

namespace Yuvish.iOS
{
	public class ViewCellRender : ViewCellRenderer
	{
		/*
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.TableView> e)
		{
			base.OnElementChanged (e);

			var tableView = Control as UITableView;

			tableView.SectionIndexColor = UIColor.White;


		}*/
		public override UITableViewCell GetCell (Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var cell = base.GetCell (item, reusableCell, tv);

			var obj = (ViewCellCustom) item;

			cell.SelectedBackgroundView = new UIView {
				BackgroundColor = obj.SelectedBackgroundColor.ToUIColor(),
			};

			cell.TextLabel.HighlightedTextColor = UIColor.Yellow;

			return cell;
		}
	}
}

